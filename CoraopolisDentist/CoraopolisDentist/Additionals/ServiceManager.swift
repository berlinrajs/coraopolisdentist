//
//  ServiceManager.swift
//  Advanced Aesthetic Dentistry
//
//  Created by Bose on 02/02/16.
//  Copyright © 2016 Advanced Aesthetic Dentistry. All rights reserved.
//

import UIKit

let hostUrl = "https://corapolis.srswebsolutions.com/corapolis/eaglesoft/"
//let hostUrl = "https://integrations.srswebsolutions.com/demoes_new/eaglesoft/" //demo

class ServiceManager: NSObject {
    
    class func fetchDataFromService(_ baseUrlString: String, serviceName: String, parameters : [String : String]?, success:@escaping (_ result : AnyObject) -> Void, failure :@escaping (_ error : Error) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: baseUrlString))
        
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.post(serviceName, parameters: parameters, progress: { (progress) in
            print(progress.fractionCompleted)
            }, success: { (task, result) in
                //                print(task)
                //                print(result)
                success(result as AnyObject)
        }) { (task, error) in
            //            print(task)
            //            print(error)
            failure(error)
        }
        
        
        
    }
    
    class func loginWithUsername(_ userName: String, password: String, completion: @escaping (_ success: Bool, _ error: NSError?) -> Void) {
        ServiceManager.fetchDataFromService("http://mncell.com/mclogin/", serviceName: "apploginapi.php?", parameters: ["appkey": "mccoraopolis", "username": userName, "password": password], success: { (result) in
            if (result["posts"]  as AnyObject)["status"] as! String == "success" {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"]  as AnyObject)["message"] as! String]))
            }
        }) { (error) in
            completion(false, nil)
        }
    }
    
    class func fetchDataFromService(_ baseUrlString: String, serviceName: String, parameters : [String : String]?, imageData: Data?, success:@escaping (_ result : AnyObject) -> Void, failure :@escaping (_ error : Error) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: baseUrlString))
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        
        manager.post(serviceName, parameters: parameters, constructingBodyWith: { (data) in
            if imageData != nil {
                data.appendPart(withFileData: imageData!, name: "task_data", fileName: "\(parameters!["patient_name"])_\(ServiceManager.date())_signature.jpeg", mimeType: "image/jpeg")
            }
            }, progress: { (progress) in
                
            }, success: { (task, result) in
                success(result as AnyObject)
        }) { (task, error) in
            failure(error)
        }
    }

    
//    class func ChekInFormStatus(patientName: String, patientPurpose: String, completion: (success: Bool, error: NSError?) -> Void) {
//        //"mcdistinctivelaser"
//        ServiceManager.fetchDataFromServiceCheckIn("savepatients_info.php?", parameters: ["patientkey": "mcCommunityDental", "patientname": patientName, "patientpurpose": patientPurpose], success: { (result) in
//            if (result["posts"] as! String == "success") {
//                completion(success: true, error: nil)
//            } else {
//                completion(success: false, error: NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"]!)!["message"] as! String]))
//            }
//        }) { (error) in
//            completion(success: false, error: nil)
//        }
//    }
    
    class func date() -> String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MMM_dd_yyyy_hh_mm"
        return dateFormat.string(from: Date())
    }
    
    class func ChekInFormStatus(_ patientName: String, patientPurpose: String, signatureImage: UIImage?, completion: @escaping (_ success: Bool, _ error: NSError?) -> Void) {
        //"mcdistinctivelaser"
        ServiceManager.fetchDataFromService("http://alpha.mncell.com/mconsent/", serviceName:"savepatients_info.php?", parameters: ["patientkey": "mcCommunityDental", "patientname": patientName, "patientpurpose": patientPurpose], imageData: signatureImage == nil ? nil : UIImageJPEGRepresentation(signatureImage!, 0.2), success: { (result) in
            if (result["posts"] as! String == "success") {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"]  as AnyObject)["message"] as! String]))
            }
        }) { (error) in
            completion(false, nil)
        }
    }
    
    
    class func postReview(_ name: String, comment: String, rating: CGFloat, phoneNumber: String, allowMessage: Bool, email: String, anonymous: Bool, completion: @escaping (_ success: Bool, _ error: NSError?) -> Void) {
        //http://distinctdental.mncell.com/appservice/review.php
        ServiceManager.fetchDataFromService("https://alpha.mncell.com/review/", serviceName: "app_review.php?", parameters: ["patient_appkey": "mcCoraopolis", "patient_name": name, "patient_review_comment": comment, "patient_rating": "\(rating)", "patient_phone_number": phoneNumber, "sms_allowed": allowMessage == true ? "1" : "0", "patient_email": email, "user_anonymous": anonymous == true ? "1" : "0"], success: { (result) in
            if result["posts"] != nil && (result["posts"] as AnyObject)["status"] as! String == "success" {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"] as AnyObject)["message"] as! String]))
            }
        }) { (error) in
            completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong, Please Check your internet connection and try again"]))
        }
    }
    
    
    #if AUTO
    class func sendChildDetails(patient: PDPatient, completion:@escaping (_ success : Bool, _ error: Error?) -> Void) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        let date = dateFormatter.date(from: patient.dateOfBirth)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        var params: [String: String] = [String: String]()
        params["Birthdate"] = dateFormatter.string(from: date!)
        params["FName"] = patient.firstName.stringVal
        params["LName"] = patient.lastName.stringVal
        params["City"] = patient.child.city.stringVal
        params["State"] = patient.child.state.stringVal
        params["Address"] = patient.child.address.stringVal
        params["Zip"] = patient.child.zipcode.stringVal
        params["HmPhone"] = patient.child.father.phoneNumber.stringVal.numbers
        params["Email"] = patient.child.father.email.stringVal
        params["sex"] = patient.child.radioGenderTag == 1 ? "M" : "F"
        
        if let patientDetails = patient.patientDetails {
            params["PatNum"] = patientDetails.patientNumber.isEmpty ? "0" : patientDetails.patientNumber
        } else {
            params["PatNum"] = "0"
        }
        let manager = AFHTTPSessionManager(baseURL: NSURL(string: hostUrl) as URL?)
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.post("consent_form_add_newpatient_op.php", parameters: params, progress: { (progress) in
            
            }, success: { (task, result) in
                if let patientId = (result as AnyObject)["patient_id"] as? String {
                    if let patientDetails = patient.patientDetails {
                        patientDetails.patientNumber = patientId
                    } else {
                        patient.patientDetails = PatientDetails()
                        patient.patientDetails?.patientNumber = patientId
                    }
                }
                completion(true, nil)
        }) { (task, error) in
            completion(false, error)
        }
    }
    
    class func sendAdultDetails(patient: PDPatient, completion:@escaping (_ success : Bool, _ error: Error?) -> Void) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        let date = dateFormatter.date(from: patient.dateOfBirth)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        var params: [String: String] = [String: String]()
        params["Birthdate"] = dateFormatter.string(from: date!)
        params["FName"] = patient.firstName.stringVal
        params["LName"] = patient.lastName.stringVal
        params["City"] = patient.adult.city.stringVal
        params["Email"] = patient.adult.email.stringVal
        params["Address"] = patient.adult.address.stringVal
        params["Zip"] = patient.adult.zipcode.stringVal
        params["HmPhone"] = patient.adult.homeNumber.stringVal.numbers
        params["work_phone"] = patient.adult.workNumber.stringVal.numbers
        params["cellular_phone"] = patient.adult.cellNumber.stringVal.numbers
        params["sex"] = patient.adult.radioGenderTag == 1 ? "M" : "F"
        params["State"] = patient.adult.state.stringVal
        params["SSN"] = patient.adult.socialSecurityNumber.stringVal.numbers

        if let patientDetails = patient.patientDetails {
            params["PatNum"] = patientDetails.patientNumber.isEmpty ? "0" : patientDetails.patientNumber
        } else {
            params["PatNum"] = "0"
        }
        let manager = AFHTTPSessionManager(baseURL: NSURL(string: hostUrl) as URL?)
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.post("consent_form_add_newpatient_op.php", parameters: params, progress: { (progress) in
            
            }, success: { (task, result) in
                if let patientId = (result as AnyObject)["patient_id"] as? String {
                    if let patientDetails = patient.patientDetails {
                        patientDetails.patientNumber = patientId
                    } else {
                        patient.patientDetails = PatientDetails()
                        patient.patientDetails?.patientNumber = patientId
                    }
                }
                completion(true, nil)
        }) { (task, error) in
            completion(false, error)
        }
    }
    #endif

}
