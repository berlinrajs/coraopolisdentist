//
//  DateInputView.swift
//  Secure Dental
//
//  Created by SRS Web Solutions on 27/04/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DateInputView: UIView {
    var textField: UITextField!
    var datePicker: UIDatePicker!
    var toolbar: UIToolbar!
    var isTimePicker : Bool = false
    
    var arrayStates: [String]!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        datePicker.addTarget(self, action: #selector(datePickerDateChanged(_:)), for: UIControlEvents.valueChanged)
        datePicker.datePickerMode = UIDatePickerMode.date
        
        self.toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 44))
        
        //        toolbar.translatesAutoresizingMaskIntoConstraints = false
        //        pickerView.translatesAutoresizingMaskIntoConstraints = false
        
//        let buttonDone = UIButton(frame: CGRectMake(0, 0, 80, 44))
//        buttonDone.setTitle("Done", forState: UIControlState.Normal)
//        buttonDone.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
//        buttonDone.addTarget(self, action: "donePressed", forControlEvents: UIControlEvents.TouchUpInside)
        
        //        buttonDone.translatesAutoresizingMaskIntoConstraints = false
        
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePressed))
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil), barbuttonDone]
        
//        let plist = NSBundle.mainBundle().pathForResource("USStateAbbreviations", ofType: "plist")
//        let states = NSDictionary(contentsOfFile: plist!)
//        arrayStates = states?.allKeys.sort({ (obj1, obj2) -> Bool in
//            let state1 = obj1 as! String
//            let state2 = obj2 as! String
//            return state1 < state2
//        }) as! [String]
        
        self.addSubview(datePicker)
        //        self.addSubview(toolbar)
        
        //        let top:NSLayoutConstraint = NSLayoutConstraint(item: toolbar, attribute: NSLayoutAttribute.TopMargin, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0)
        //
        //        let bottom:NSLayoutConstraint = NSLayoutConstraint(item: pickerView, attribute: NSLayoutAttribute.TopMargin, relatedBy: NSLayoutRelation.Equal, toItem: toolbar, attribute: NSLayoutAttribute.BottomMargin, multiplier: 1, constant: 0)
        //
        //        let pickerCenterX:NSLayoutConstraint = NSLayoutConstraint(item: pickerView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0);
        //
        //        let toolCenterX:NSLayoutConstraint = NSLayoutConstraint(item: pickerView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0);
        //
        //        NSLayoutConstraint.activateConstraints([top, bottom, pickerCenterX, toolCenterX])
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @IBAction func datePickerDateChanged(_ sender: AnyObject) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = self.isTimePicker == true ? "hh:mm a" : "MMM dd, yyyy"
        textField.text = dateFormatter.string(from: datePicker.date).uppercased()
    }
    func donePressed() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = self.isTimePicker == true ? "hh:mm a" : "MMM dd, yyyy"
        textField.text = dateFormatter.string(from: datePicker.date).uppercased()
        textField.resignFirstResponder()
    }
    class func addDatePickerForTextField(_ textField: UITextField) {
        self.addDatePickerForTextField(textField, minimumDate: nil, maximumDate: Date())
    }
    class func addDatePickerForDOBTextField(_ textField: UITextField) {
        self.addDatePickerForDOBTextField(textField, minimumDate: nil, maximumDate: Date())
    }
    
    
    class func addDatePickerForTextField(_ textField: UITextField, minimumDate: Date?, maximumDate: Date?) {
        let dateListView = DateInputView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = dateListView
        textField.inputAccessoryView = dateListView.toolbar
        dateListView.textField = textField
        dateListView.datePicker.minimumDate = minimumDate
        dateListView.datePicker.maximumDate = maximumDate

        let year = (Calendar.current as NSCalendar).component(NSCalendar.Unit.year, from: Date())
        let dateString = "1 Jan \(year)"
        let df = DateFormatter()
        df.dateFormat = "dd MM yyyy"
        let date = df.date(from: dateString)
        if let unwrappedDate = date {
            dateListView.datePicker.setDate(unwrappedDate, animated: false)
        }
    }
    
    class func addDatePickerForDOBTextField(_ textField: UITextField, minimumDate: Date?, maximumDate: Date?) {
        let dateListView = DateInputView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = dateListView
        textField.inputAccessoryView = dateListView.toolbar
        dateListView.textField = textField
        dateListView.datePicker.minimumDate = minimumDate
        dateListView.datePicker.maximumDate = maximumDate
        
//        let year = NSCalendar.currentCalendar().component(NSCalendarUnit.Year, fromDate: NSDate())
        let dateString = "1 Jan 1980"
        let df = DateFormatter()
        df.dateFormat = "dd MM yyyy"
        let date = df.date(from: dateString)
        if let unwrappedDate = date {
            dateListView.datePicker.setDate(unwrappedDate, animated: false)
        }
    }
    
    class func addDatePickerForDateOfBirthTextField(_ textField: UITextField) {
        let dateListView = DateInputView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = dateListView
        textField.inputAccessoryView = dateListView.toolbar
        dateListView.textField = textField
        dateListView.datePicker.datePickerMode = UIDatePickerMode.date
        let dateString = "1 Jan 1980"
        let df = DateFormatter()
        df.dateFormat = "dd MM yyyy"
        let date = df.date(from: dateString)
        if let unwrappedDate = date {
            dateListView.datePicker.setDate(unwrappedDate, animated: false)
        }
    }
    
    
    class func addTimePickerForTextField(_ textField: UITextField) {
        let dateListView = DateInputView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        dateListView.isTimePicker = true
        textField.inputView = dateListView
        textField.inputAccessoryView = dateListView.toolbar
        dateListView.textField = textField
        dateListView.datePicker.datePickerMode = UIDatePickerMode.time
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        
        dateListView.datePicker.setDate(dateFormatter.date(from: "12:00 am")!, animated: true)
    }
    
    class func addExpiryDatePickerForTextField(_ textField: UITextField) {
        let dateListView = DateInputView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = dateListView
        textField.inputAccessoryView = dateListView.toolbar
        dateListView.textField = textField
        dateListView.datePicker.minimumDate = nil
        dateListView.datePicker.maximumDate = nil
        dateListView.datePicker.datePickerMode = UIDatePickerMode.date
        
    }

}

class MonthInputView: UIView {
    var toolbar: UIToolbar!
    var pickerMonth : UIPickerView!
    var textField: UITextField!
    var arrayMonths = ["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        pickerMonth = UIPickerView(frame: frame)
        pickerMonth.dataSource = self
        pickerMonth.delegate = self
        self.toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 44))
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePressed))
        let barButtonClear = UIBarButtonItem(title: "Clear", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelPressed))
        toolbar.items = [barButtonClear, UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil), barbuttonDone]
        self.addSubview(pickerMonth)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func cancelPressed() {
        textField.resignFirstResponder()
        textField.text = ""
    }
    
    func donePressed() {
        let string1 = arrayMonths[pickerMonth.selectedRow(inComponent: 0)]
        textField.text = string1.substring(with: string1.startIndex ..< string1.characters.index(string1.startIndex, offsetBy: 3))
        textField.resignFirstResponder()
    }
    
    class func addMonthPickerForTextField(_ textField: UITextField) {
        let monthListView = MonthInputView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = monthListView
        textField.inputAccessoryView = monthListView.toolbar
        monthListView.textField = textField
    }
}

extension MonthInputView : UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayMonths.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayMonths[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let string1 = arrayMonths[row]
        
        //let index1 = string1.startIndex.advancedBy(3)
        
        textField.text = string1.substring(with: string1.startIndex ..< string1.characters.index(string1.startIndex, offsetBy: 3))
        //  textfieldMonth.text = arrayMonths[row]
    }
}
