//
//  PDTextView.swift
//  Advanced Aesthetic Dentistry
//
//  Created by Office on 2/24/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDTextView: UITextView {

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
        self.autocapitalizationType = UITextAutocapitalizationType.none

    }
    override func awakeFromNib() {
        super.awakeFromNib()
        if delegate == nil {
            delegate = self
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.black {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 3.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    var textValue: String? {
        get {
            self.delegate?.textViewDidBeginEditing?(self)
            let returnString = self.text
            self.delegate?.textViewDidEndEditing?(self)
            return returnString
        } set {
            self.delegate?.textViewDidBeginEditing?(self)
            self.text = newValue
            self.delegate?.textViewDidEndEditing?(self)
        }
    }
    
    @IBInspectable var characterCount: Int! = 0
    
    @IBInspectable var placeholder: String? = nil {
        didSet {
            if (placeholder?.characters.count)! > 0 {
                self.text = placeholder
                self.textColor = self.placeholderColor
            }
        }
    }
    @IBInspectable var defaultTextColor: UIColor = UIColor.black {
        didSet {
            if self.text != self.placeholder {
                self.textColor = defaultTextColor
            }
        }
    }
    
    @IBInspectable var placeholderColor: UIColor = UIColor.lightGray {
        didSet {
            if self.text == self.placeholder {
                self.textColor = placeholderColor
            }
        }
    }
    
    override var isEmpty : Bool {
        return text == placeholder ? true : super.isEmpty
    }

}


//class TextView: PDTextView {
//    
//    @IBInspectable var clearOnEndEditing: Bool = true
//    var placeHolder : String? = ""
//    var isEdited : Bool {
//        get {
//            return (self.text != placeHolder && !self.isEmpty)
//        }
//    }
//    override func awakeFromNib() {
//        if placeHolder?.isEmpty == true {
//            placeHolder = self.text
//        }
//        if self.delegate == nil {
//            self.delegate = self
//        }
//    }
//    
//    func checkEdited() {
//        if isEdited {
//            self.textColor = UIColor.black
//        } else {
//            self.text = placeHolder
//            self.textColor = UIColor.lightGray
//        }
//    }
//    
//}

extension PDTextView : UITextViewDelegate {
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = placeholder
            textView.textColor = placeholderColor
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: text)
        
        return characterCount == 0 || newString.characters.count <= characterCount
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == placeholder {
            textView.text = ""
            textView.textColor = self.defaultTextColor
        }
    }

}
