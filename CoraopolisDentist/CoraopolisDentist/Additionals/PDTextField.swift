//
//  PDTextField.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDTextField: UITextField {

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.borderColor = isEnabled ? borderColor.cgColor : borderColor.withAlphaComponent(0.2).cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: isEnabled ? placeHolderColor : placeHolderColor.withAlphaComponent(0.2)])
        tintColor = isEnabled ? placeHolderColor : placeHolderColor.withAlphaComponent(0.2)
        clipsToBounds = true
        self.autocapitalizationType = UITextAutocapitalizationType.none

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: frame.height))
        self.leftView = leftView
        leftViewMode = .always
    }
    
    var borderColor: UIColor = UIColor.white {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    var cornerRadius: CGFloat = 3.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }
    
    var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    var placeHolderColor: UIColor = UIColor.white.withAlphaComponent(0.5) {
        didSet {
            attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: placeHolderColor])
            tintColor = placeHolderColor
        }
    }
    
    override var placeholder: String? {
        didSet {
            attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: placeHolderColor])
            tintColor = placeHolderColor
        }
    }
}
