
//
//  Forms.swift
//   Coraopolisdentist
//
//  Created by samadsyed on 2/18/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


let kFormsCompletedNotification = "kAllFormsCompletedSuccessfully"

let kNewAdultSignIn = "NEW PATIENT SIGN IN FORM - ADULT"
let kNewChildSignIn = "NEW PATIENT SIGN IN FORM - CHILD"

let kAdultSignIn = "PATIENT SIGN IN FORM - ADULT"
let kChildSignIn = "PATIENT SIGN IN FORM - CHILD"

let kConsentForms = "CONSENT FORMS"
let kOfficePoliciesForms = "OFFICE POLICIES"
let kHygieneForms = "HYGIENE"
let kComposite = "COMPOSITE CONSENT FORM"
let kSocialMedia = "CONSENT TO SOCIAL MEDIA"
let kOfficePolicy = "OFFICE APPOINTMENT POLICY"
let kXrays = "INFORMED REFUSAL OF NECESSARY X-RAYS"
let kReservation = "RESERVATION FEE POLICY"
let kWaxConsent  = "WAX TRY IN CONSENT"
let kOralSurgery = "ORAL SURGERY CONSENT FORM"
let kFinancial = "FINANCIAL OPTIONS"
let kPhotography = "CONSENT TO USE DENTAL PHOTOGRAPHY"
let kDentalRecords = "DENTAL RECORDS RELEASE FROM OUR OFFICE"
let kPreviousRecords = "DENTAL RECORDS RELEASE FROM PREVIOUS DMD"
let kFeedbackForm = "FEEDBACK FORM"
let kInsuranceCard = "SCAN INSURANCE CARD"
let kDrivingLicense = "SCAN DRIVING LICENSE"

let kPeriodontalCharting = "INFORMED REFUSAL OF PERIODONTAL CHARTING"
let kRootPlanningRefusal = "INFORMED REFUSAL OF SCALING AND ROOT PLANING"
let kTeethScaling = "INFORMED REFUSAL OF PROPHYLAXIS TEETH SCALING"
let kRootPlanningAuthorization = "INFORMED CONSENT AND AUTHORIZATION FOR PERIODONTAL TREATMENT UTILIZING SCALING & ROOT PLANNING (SRP)"
let kPeriodontalSpecialist = "INFORMED CONSENT OF PERIODONTAL SPECIALIST RECOMMENDATION"
let kInsuranceCoverage = "INSURANCE COVERAGE"
let kDeliveredAppliance = "ACKNOWLEDGEMENT OF DELIVERED APPLIANCE"
let kPatientUpdate = "PATIENT UPDATE"
let kNoticeOfPrivacyPractices = "NOTICE OF PRIVACY PRACTICES"
let kPaymentPlan = "PAYMENT PLAN"
let kDiscountPlan = "IN-HOUSE DISCOUNT PLAN"
let kPeriodontalDisease = "NOTIFICATION OF PERIODONTAL DISEASE"
let kPeriodontalManagement = "PERIODONTAL MANAGEMENT"
let kHippa = "NOTICE OF PRIVACY PRACTICES FOR PROTECTED HEALTH INFORMATION"


let kNewPatient = ["NEW PATIENT" : [kNewAdultSignIn,kNewChildSignIn,kInsuranceCard, kDrivingLicense]]
let kExistingPatient = ["EXISTING PATIENT": [kAdultSignIn,kChildSignIn,kInsuranceCard, kDrivingLicense]]
let kOfficePolicies = ["OFFICE POLICIES": [kComposite,kOfficePolicy,kReservation,kFinancial,kNoticeOfPrivacyPractices,kPatientUpdate, kPaymentPlan,kDiscountPlan,kHippa]]
let kHygiene = ["HYGIENE": [kPeriodontalCharting,kRootPlanningRefusal,kTeethScaling,kRootPlanningAuthorization,kPeriodontalSpecialist,kPeriodontalDisease,kPeriodontalManagement]]
let kConsentFormsAuto = ["CONSENT FORMS": [kSocialMedia,kXrays,kWaxConsent,kOralSurgery,kPhotography,kDentalRecords,kPreviousRecords,kInsuranceCoverage, kDeliveredAppliance,kFeedbackForm]]


let toothNumberRequired : [String] = []

class Forms: NSObject {
    
    var formTitle : String!
    var subForms : [Forms]!
    var isSelected : Bool!
    var toothNumbers : String!
    var isToothNumberRequired : Bool!
    var index : Int!

    init(formDetails : NSDictionary) {
        super.init()
        self.isSelected = false
    }
    
    override init() {
        super.init()
    }
    
    class func getAllForms (_ completion :(_ isConnectionfailed: Bool, _ forms : [Forms]?) -> Void) {
        let isConnected = Reachability.isConnectedToNetwork()
        #if AUTO
            var arrayResult : [Forms] = [Forms]()
            let forms = [kNewPatient, kExistingPatient,kOfficePolicies,kHygiene, kConsentFormsAuto]
            for form in forms {
                let formObj = getFormObjects(forms: form)
                arrayResult.append(formObj)
            }
            completion(isConnected ? false : true, arrayResult)
        #else
            var arrayResult : [Forms] = [Forms]()
            let forms = [kNewPatient, kOfficePolicies,kHygiene, kConsentFormsAuto]
            for form in forms {
                let formObj = getFormObjects(forms: form)
                arrayResult.append(formObj)
            }
            completion(isConnected ? false : true, arrayResult)
        #endif
    }

    #if AUTO
    private class func getFormObjects (forms : [String : [String]]) -> Forms {
        let formObject = Forms()
        formObject.formTitle = forms.keys.first!
        formObject.isSelected = false
        
        var formList : [Forms] = [Forms]()
        for (_, form) in forms.values.first!.enumerated() {
            let formObj = Forms()
            formObj.isSelected = false
            formObj.formTitle = form
            formObj.isToothNumberRequired = toothNumberRequired.contains(form)
            formList.append(formObj)
        }
        formObject.subForms = formList
        return formObject
    }
    #else
   private class func getFormObjects (forms : [String : [String]]) -> Forms {
    let formObject = Forms()
    formObject.formTitle = forms.keys.first!
    formObject.isSelected = false
    
    var formList : [Forms] = [Forms]()
    for (_, form) in forms.values.first!.enumerated() {
    let formObj = Forms()
    formObj.isSelected = false
    formObj.formTitle = form
    formObj.isToothNumberRequired = toothNumberRequired.contains(form)
    formList.append(formObj)
    }
    formObject.subForms = formList
    return formObject
    }
    #endif
    
}
