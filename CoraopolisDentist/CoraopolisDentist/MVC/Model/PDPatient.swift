//
//  PDPatient.swift
//   Coraopolisdentist
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDPatient: NSObject {
    var selectedForms : [Forms]!
    var firstName : String!
    var lastName : String!
    var middleInitial : String = ""
   // var phoneNumber : String!
    var dateToday : String!
    var doctorName: String!
    
    #if AUTO
    var patientDetails : PatientDetails?
    #endif
    
    var fullName: String! {
        get {
            return middleInitial == "" ? firstName + " " + lastName : firstName + " " + middleInitial + " " + lastName
        }
    }

    var is18YearsOld : Bool {
        return patientAge >= 18
    }

    var patientAge: Int! {
        get {
            if dateOfBirth == nil || dateOfBirth.characters.count == 0 {
                return 0
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            
            let numberOfDays = (Date().timeIntervalSince(dateFormatter.date(from: self.dateOfBirth)!))/(3600 * 24)
            return Int(numberOfDays/365.2425)
        }
    }
    
    //NEW PATIENT
    var dateOfBirth : String!
    var parentFirstName : String?
    var parentLastName : String?
    var parentDateOfBirth : String?
    
    required override init() {
        super.init()
    }
    
    init(forms : [Forms]) {
        super.init()
        self.selectedForms = forms
    }
    
    //visitorcheckin
    var VisitorFullName : String!
    var VisitingPurpose : String!
    var visitorFirstName : String!
    var visitorLastName : String!
    
    var visitorFullName : String {
        return visitorFirstName + " " + visitorLastName
    }
    
    var oral : OralSurgery = OralSurgery()
    
    //PHOTOGRAPHY CONSENT
    var photoTypeTag : Int = 0
    var photoUseTags : [Int] = [Int]()
    
    var dentalRecords : DentalRecords = DentalRecords()
    var previousRecords : PreviousRecords = PreviousRecords()
    var adult : AdultRegistration = AdultRegistration()
    var patientInformation : PatientInformation = PatientInformation()
    var child : ChildRegistration = ChildRegistration()

    
    ///SMILE ANALYSIS
    var radioSmileTag : Int = 0
    var radioTeethTag : Int = 0
    var arraySmileTags : NSMutableArray = NSMutableArray()
    var smileOther : String = "N/A"
    var arrayTeethTags : NSMutableArray = NSMutableArray()
    var teethOther : String = "N/A"
    var arrayLifestyleTag : NSMutableArray = NSMutableArray()
    var lifestyleOther : String = "N/A"
    var arrayMakeOverTag : NSMutableArray = NSMutableArray()
    var makeOverOther : String = "N/A"
    var arrayFamilyTag : NSMutableArray = NSMutableArray()
    var familyOther : String = "N/A"
    var arrayAppointmentTags : NSMutableArray = NSMutableArray()
    var appointmentOther = "N/A"
    var arrayMusicTags : NSMutableArray = NSMutableArray()
    var musicOther : String = "N/A"
    var upcomingEvents : String = "N/A"
    var upcomingEventsTag : Int = 0
    var childrensTag : Int = 0
    var childrens : String = "N/A"
    var hobbies : String = "N/A"
    var knowMore : String = "N/A"
    var smileAnalysisSignature : UIImage!
    var patientNumber : String = "N/A"

    //patientUpdate
    @IBOutlet weak var radioUseDentalAppliance : RadioButton!
    @IBOutlet weak var viewAppliance : UIView!
    @IBOutlet var buttonApplianceList: [UIButton]!
    @IBOutlet weak var radioCPAPWear : RadioButton!
    
    var applianceList : [Int] = []
    var useDentalAppliance : Int = 2
    var CPAPWear : Int = 0
    var medicalHistoryChange : String = ""
    var medicationsChange : String = ""
    
    
    var signRefused: Bool!
    var signRefusalTag: Int!
    var signRefusalOther: String!
    
    var cardType : Int = 0
    var cardNumber : String!
    var expiryDate : String!
    var creditCardNumber : String!
    var paymentAmt : String!
    var dueDate : String!
    var procedure : String = ""
    
    
    
    var periodontalType: Int = 0
    var pocketsSelected1: Bool = false
    var pocketsSelected2: Bool = false
    var pocketsSelected3: Bool = false
    var pocketsSelected4: Bool = false
    var pocketsSelected5: Bool = false
    var treatmentRecommended : String = ""
    var recomendedCareInterval : String = ""
    
    var agreementSelected: Int = 0
    
    var quadrant1 : String = ""
    var quadrant2 : String = ""
    var quadrant3 : String = ""
    var quadrant4 : String = ""
    
    var name1 : String = ""
    var relationship1 : String = ""
    var name2 : String = ""
    var relationship2 : String = ""
    var name3 : String = ""
    var relationship3 : String = ""
    

}

class OralSurgery: NSObject {
    
    var authorization : String = "N/A"
    var procedures : String = "N/A"
    var proceduresTag : [Int] = [Int]()
    var signPatient : UIImage!
    var signDoctor : UIImage!
    var signWitness : UIImage!
    
    
}

class DentalRecords: NSObject {
    
    var practiceName : String = "N/A"
    var practiceAddress : String = "N/A"
    var practicePhoneNumber : String = "N/A"
    var email : String = "N/A"
    var patientAddress : String = "N/A"
    var patientPhoneNumber : String = "N/A"
    
    var signedTag : Int = 1
    var relationship : String = "N/A"
    var signaturePatient : UIImage!
    
}


class PreviousRecords: NSObject {
    
    var patientPhoneNumber : String = "N/A"
    var familyMembers : String = "N/A"
    var dentistName : String = "N/A"
    var dentistAddress : String = "N/A"
    var dentistCity : String = "N/A"
    var dentistState : String = "PA"
    var dentistZipcode : String = "N/A"
    var dentistPhoneNumber : String = "N/A"
    
    var signPatient : UIImage!
    
}

class AdultRegistration: NSObject {
    var nickName : String = "N/A"
    var radioGenderTag : Int = 0
    var address : String = "N/A"
    var city : String = "N/A"
    var state : String = "PA"
    var zipcode : String = "N/A"
    var socialSecurityNumber : String = "N/A"
    var email : String = "N/A"
    
    var radioContactTag : Int = 0
    var timeToCall : String = "N/A"
    var homeNumber : String = "N/A"
    var workNumber : String = "N/A"
    var cellNumber : String = "N/A"
    var faxNumber : String = "N/A"
    var driverLicense : String = "N/A"
    
    var employerName : String = "N/A"
    var occupation : String = "N/A"
    var spouseName : String = "N/A"
    var spouseEmployer : String = "N/A"
    
    var reference : String = "N/A"
    var schoolName : String = "N/A"
    var schoolPhone : String = "N/A"
    var grade : String = "N/A"
    
    var reasonTodayVisit : String = "N/A"
    var lastDentalExam : String = "N/A"
    var lastDentalCleaning : String = "N/A"
    var lastDentalXray : String = "N/A"
    var radioAnxietyTag : Int = 0
    
    var dentalProcedure : String = "N/A"
    var dentistName : String = "N/A"
    var dentistState : String = "PA"
    var dentistCity : String = "N/A"
    var dentistPhone : String = "N/A"
    var dentistReasonToChange : String = "N/A"
    
    var dentalExamination : String = "N/A"
    var brush : String = "N/A"
    var brushTag : Int = 0
    var floss : String = "N/A"
    var flossTag : Int = 0
    var bristleTag : Int = 0
    var dentalAids : String = "N/A"
    
    var dentalQuestions : [[PDQuestion]]!
    var dentalHistoryDetails : String = "N/A"
    var medicalQuestions : [[PDQuestion]]!
    var allergyQuestions : [PDQuestion]!
    
    required override init() {
        super.init()
        let quest1: [String] = ["Are you currently in pain?",
                                "Do you have any dental problems now?",
                                "Have you ever had trouble with a previous dental treatment?",
                                "Do you require antibiotics before dental treatment?",
                                "Do your gums ever bleed?",
                                "Have you noticed any mouth odors or bad tastes?",
                                "Do you bite your lips or cheeks frequently?",
                                "Do you have frequent headaches?",
                                "Do you clench or grind your teeth?",
                                "Are your teeth sensitive to heat/cold?",
                                "Do you still have your wisdom teeth?",
                                "Have you taken any medications or drugs in the past two years?",
                                "Are you currently taking any medications or drugs?",
                                "Have you ever taken Fen - Phen?"]
        
        let quest2: [String] = ["Have you been to the doctor to check for heart problems?",
                                "Periodontal disease/gum treatment",
                                "Orthodontics treatment",
                                "Oral surgery",
                                "A bite plate or mouth guard",
                                "Discomfort in your jaw joint (TMJ/TMD)",
                                "Your teeth ground or bite adjusted",
                                "Serious injury to the mouth or head",
                                "Is there anything else about your past dental treatment(s) that you would like us to know?"]
        
        self.dentalQuestions = [PDQuestion.arrayOfQuestions(quest1),PDQuestion.arrayOfQuestions(quest2)]
        self.dentalQuestions[0][0].isAnswerRequired = true
        self.dentalQuestions[0][1].isAnswerRequired = true
        self.dentalQuestions[0][2].isAnswerRequired = true
        self.dentalQuestions[0][12].isAnswerRequired = true
        self.dentalQuestions[0][13].isAnswerRequired = true
        self.dentalQuestions[1][8].isAnswerRequired = true
        self.dentalQuestions[1][0].isAnswerRequired = true

        
        let mQuest1: [String] = ["AIDS/HIV",
                                "Alcohol/Drug Abuse",
                                "Allergies or Hives",
                                "Anemia",
                                "Arthritis/Rheumatism",
                                "Artificial Heart Valve",
                                "Artificial Bones/Joints",
                                "Asthma",
                                "Blood Disease",
                                "Blood Transfusion",
                                "Bruise Easily",
                                "Cancer/Chemotherapy",
                                "Chest Pain",
                                "Cold Sores/Herpes"]
        
        let mQuest2: [String] = ["Colitis",
                                 "Contact Lenses",
                                 "Cortisone Medicine",
                                 "Diabetes",
                                 "Diet (Special/Restricted)",
                                 "Difficulty Breathing",
                                 "Emphysema",
                                 "Epilepsy or Seizures",
                                 "Fainting or Dizzy Spells",
                                 "Frequent Headaches",
                                 "Glaucoma",
                                 "Hay Fever",
                                 "Heart (Surgery, Disease, Attack)",
                                 "Heart Pacemaker"]

        let mQuest3: [String] = ["Heart Murmur",
                                 "Hemophilia/Abnormal Bleeding",
                                 "Hepatitis",
                                 "High or Low Blood Pressure",
                                 "Hospitalized for Any Reason",
                                 "Jaundice",
                                 "Kidney Trouble",
                                 "Liver Disease",
                                 "Lupus",
                                 "Mitral Valve Prolapse",
                                 "Nervousness/Anxiety",
                                 "Neurological Disorders",
                                 "Psychiatric/ Psychological Care"]
        
        let mQuest4: [String] = ["Radiation Therapy",
                                 "Rheumatic/Scarlet Fever",
                                 "Shingles/Chicken Pox",
                                 "Sickle Cell Disease/Traits",
                                 "Sinus Trouble",
                                 "Snoring/Sleep Apnea",
                                 "Stomach Problems/ Ulcers",
                                 "Stroke",
                                 "Swollen Ankles",
                                 "Thyroid Problems",
                                 "Tuberculosis (TB)",
                                 "Tumors",
                                 "Venereal Disease/STD",
                                 "Other"]

        self.medicalQuestions = [PDQuestion.arrayOfQuestions(mQuest1),PDQuestion.arrayOfQuestions(mQuest2),PDQuestion.arrayOfQuestions(mQuest3),PDQuestion.arrayOfQuestions(mQuest4)]
        self.medicalQuestions[3][13].isAnswerRequired = true
        self.medicalQuestions[2][2].isAnswerRequired = true

        let aQuest1: [String] = ["Aspirin",
                                 "Codeine",
                                 "Anesthetics (i.e. Novocaine)",
                                 "Erythromycin",
                                 "Iodine",
                                 "Jewelry/Metals",
                                 "Latex",
                                 "Penicillin or Other Antibiotics",
                                 "Sedatives",
                                 "Sulfa Drugs",
                                 "Tetracycline",
                                 "Other"]
        self.allergyQuestions = PDQuestion.arrayOfQuestions(aQuest1)
        self.allergyQuestions[11].isAnswerRequired = true


    }
    
    var radioHospitalizedTag : Int = 2
    var hospitalizedReason : String = "N/A"
    var hospitalName : String = "N/A"
    var hospitalState : String = "PA"
    var hospitalCity : String = "N/A"
    var hospitalPhone : String = "N/A"
    var radioTobaccoTag : Int = 0
    var radioAlcoholTag : Int = 0
    var radioPregnantTag : Int = 0
    var radioBirthControlTag : Int = 0
    var radioNursingTag : Int = 0
    
    

    
    
}

class PatientInformation : NSObject {
    
    var radioPrimaryInsuranceTag : Int = 1
    var radioPrimarySelfTag : Int = 2
    var primaryCompanyName : String = "N/A"
    var primaryCompanyNumber : String = "N/A"
    var primaryGroupNumber : String = "N/A"
    var primaryInsuredId : String = "N/A"
    var primaryAddress : String = "N/A"
    var primaryCity : String = "N/A"
    var primaryState : String = "PA"
    var primaryZipcode : String = "N/A"
    var primaryName : String = "N/A"
    var primaryRelationship : String = "N/A"
    var primaryDateOfBirth : String = "N/A"
    var primarySocialSecurityNumber : String = "N/A"
    var primaryEmployerName : String = "N/A"
    var primaryPracticeTag : Int = 0
    
    var radioSecondarySelfTag : Int = 2
    var secondaryCompanyName : String = "N/A"
    var secondaryCompanyNumber : String = "N/A"
    var secondaryGroupNumber : String = "N/A"
    var secondaryInsuredId : String = "N/A"
    var secondaryAddress : String = "N/A"
    var secondaryCity : String = "N/A"
    var secondaryState : String = "PA"
    var secondaryZipcode : String = "N/A"
    var secondaryName : String = "N/A"
    var secondaryRelationship : String = "N/A"
    var secondaryDateOfBirth : String = "N/A"
    var secondarySocialSecurityNumber : String = "N/A"
    var secondaryEmployerName : String = "N/A"
    var secondaryPracticeTag : Int = 0
    
    var responsibleSelfTag : Int = 2
    var responsibleName : String = "N/A"
    var responsibleRelationship : String = "N/A"
    var responsiblePhoneNUmber : String = "N/A"
    var responsibleDateOfBirth : String = "N/A"
    var responsibleAddress : String = "N/A"
    var responsibleCity : String = "N/A"
    var responsibleState : String = "PA"
    var responsibleZipcode : String = "N/A"
    var responsibleEmployerName : String = "N/A"
    var responsibleWOrkPhone : String = "N/A"
    var responsibleDriverLicense : String = "N/A"
    var responsiblePaymentType : Int = 0
    var responsibleCardType : Int = 0

    var responsibleCardNumber : String = "N/A"
    var responsibleExpDate : String = "N/A"
    var responsibleSocialSecurityNumber : String = "N/A"
    var legalGuardianName : String = "N/A"
    var legalGuardianRelationship : String = "N/A"
    var legalGuardianPractice : Int = 0
    var signature : UIImage!
    var signatureOfficial : UIImage!
    
    var emergencyName : String = "N/A"
    var emergencyRelationship : String = "N/A"
    var emergencyCity : String = "N/A"
    var emergencyState : String = "PA"
    var emergencyCellNumber : String = "N/A"
    var emergencyWorkNumber : String = "N/A"
    var emergencyHomeNumber : String = "N/A"
    
    required override init() {
        super.init()
    }
    
}



class ChildRegistration: NSObject {
    var nickName : String = "N/A"
    var radioGenderTag : Int = 0
    var address : String = "N/A"
    var city : String = "N/A"
    var state : String = "PA"
    var zipcode : String = "N/A"
    var hobbies : String = "N/A"
    var tvShows : String = "N/A"
    
    var schoolName : String = "N/A"
    var grade : String = "N/A"
    var physicianName : String = "N/A"
    var physicianAddress : String = "N/A"
    var radioFirstVisitTag : Int = 0
    var lastDentalVisit : String = "N/A"
    
    var responsibleName : String = "N/A"
    var responsiblePhone : String = "N/A"
    var brotherDetail : String = "N/A"
    
    var father : Guardian = Guardian()
    var mother : Guardian = Guardian()
    
    var primaryInsurance : String = "N/A"
    var primaryName : String = "N/A"
    var secondaryInsurance : String = "N/A"
    var secondaryName : String = "N/A"
    
    var ageFirstTeeth : String = "N/A"
    var dentalProblems : String = "N/A"
    var birthPlace : String = "N/A"
    var radioBrushTag : Int = 0
    var brushDetails : String = "N/A"
    var brushType : String = "N/A"
    var brushTypeTag : Int = 0
    var brushingTime : String = "N/A"
    var radioNowTag : Int = 0
    
    var dentalQuestions : [PDQuestion]!
    var allergies : [PDQuestion]!
    var medicalQuestions : [[PDQuestion]]!

    
    var bottleTag : Int = 0
    var bottleContents : String = "N/A"
    var bottleStoppedAge : String = "N/A"
    var arrayHabitTags : [Int] = [Int]()
    var arrayFluorideTag : [Int] = [Int]()
    var habitOther : String = "N/A"
    var dietSummary : String = "N/A"
    var familyHistory : String = "N/A"
    
    
    var illness : String = "N/A"
    var comments : String = "N/A"
    var printName : String = "N/A"
    var signaturePatient : UIImage!
    
    required override init() {
        super.init()
        let quest1: [String] = ["Was water fluoridated?",
                                "Has the patient had any dental injuries?",
                                "Does the patient snore?",
                                "Any problems or medications during pregnancy?",
                                "Is the patient in good health now?",
                                "Is the patient taking medication?"]
        
        
        self.dentalQuestions = PDQuestion.arrayOfQuestions(quest1)
        self.dentalQuestions[0].isAnswerRequired = true
        self.dentalQuestions[1].isAnswerRequired = true
        self.dentalQuestions[3].isAnswerRequired = true
        self.dentalQuestions[5].isAnswerRequired = true
        
        
        let mQuest1: [String] = ["Abnormal bleeding",
                                 "Allergies",
                                 "Asthma",
                                 "Behavior/ learning deficit",
                                 "Blood transfusions",
                                 "Breathing/ lung problems",
                                 "Cancer/ Tumors",
                                 "Cerebral palsy",
                                 "Congenital birth defect",
                                 "Developmental disability",
                                 "Diabetes",
                                 "Frequent infections",
                                 "Hearing difficulty",
                                 "Heart trouble"]
        
        let mQuest2: [String] = ["Hospitalization/ Surgery",
                                 "Infectious diseases",
                                 "Kidney trouble",
                                 "Liver/ GI problems",
                                 "Rheumatic fever",
                                 "Seizures",
                                 "Sight difficulty",
                                 "Sleep apnea",
                                 "Speech difficulty",
                                 "Thyroid Problems"]
        
        self.medicalQuestions = [PDQuestion.arrayOfQuestions(mQuest1),PDQuestion.arrayOfQuestions(mQuest2)]
        
        
        let allergyquest1: [String] = ["Aspirin",
                                "Anesthetics/Novocaine",
                                "Codeine",
                                "Erythromycin",
                                "Iodine",
                                "Jewelry",
                                "Latex",
                                "Penicillin/Antibiotics",
                                "Sedatives",
                                "Sulfa Drugs",
                                "Tetracycline",
                                "Other"]
        
        
        self.allergies = PDQuestion.arrayOfQuestions(allergyquest1)
        self.allergies[11].isAnswerRequired = true


    }

    
}

class Guardian: NSObject {
    var firstName : String = "N/A"
    var lastName : String = "N/A"
    var address : String = "N/A"
    var city : String = "N/A"
    var state : String = "PA"
    var zipcode : String = "N/A"
    var phoneNumber : String = "N/A"
    var dateOfBirth : String = "N/A"
    var socialSecurity : String = "N/A"
    var employer : String = "N/A"
    var workPhone : String = "N/A"
    var email : String = "N/A"
    
}

enum MaritalStatus: String {
    case SINGLE = "S"
    case MARRIED = "M"
    case WIDOWED = "W"
    case DIVORCED = "D"
    case SEPARATE = "X"
    case UNKNOWN = "U"
    
    init(value: Int) {
        switch value {
        case 1: self = .SINGLE
        case 2: self = .MARRIED
        case 3: self = .WIDOWED
        case 4: self = .DIVORCED
        case 5: self = .SEPARATE
        default: self = .UNKNOWN
        }
    }
    var index: Int {
        switch self {
        case .SINGLE: return 1
        case .MARRIED: return 2
        case .WIDOWED: return 3
        case .DIVORCED: return 4
        case .SEPARATE: return 5
        default: return 6
        }
    }
    var description: String {
        switch self {
        case .SINGLE: return "SINGLE"
        case .MARRIED: return "MARRIED"
        case .WIDOWED: return "WIDOWED"
        case .DIVORCED: return "DIVORCED"
        case .SEPARATE: return "SEPARATED"
        default: return "UNKNOWN"
        }
    }
}

enum Gender: String {
    case MALE = "M"
    case FEMALE = "F"
    
    init(value: Int) {
        switch value {
        case 1: self = .MALE
        default: self = .FEMALE
        }
    }
    var index: Int {
        switch self {
        case .MALE: return 1
        default: return 2
        }
    }
    
    var description: String {
        switch self {
        case .MALE: return "MALE"
        default: return "FEMALE"
        }
    }
}


#if AUTO
    class PatientDetails : NSObject {
        var dateOfBirth : String!
        var firstName : String!
        var lastName : String!
        var preferredName : String!
        var address : String!
        var city : String!
        var state : String!
        var zipCode : String!
        var country : String!
        var gender : Gender?
        var socialSecurityNumber : String!
        var email : String!
        var homePhone : String!
        var workPhone : String!
        var cellPhone : String!
        
        
        var patientNumber : String!
        var maritalStatus: MaritalStatus?
        var middleInitial : String!
        
        override init() {
            super.init()
        }
        
        init(details : [String: AnyObject]) {
            super.init()
            self.city = getValue(value: details["City"])
            self.dateOfBirth = getValue(value: details["Birthdate"])
            self.email = getValue(value: details["Email"])
            self.socialSecurityNumber = getValue(value: details["SSN"])
            self.address = getValue(value: details["Address"])
            self.zipCode = getValue(value: details["Zip"])
            self.gender = getValue(value: details["sex"]) == "" ? nil : Gender(rawValue: getValue(value: details["sex"]))
            self.firstName = getValue(value: details["FName"])
            self.patientNumber = getValue(value: details["PatNum"])
            self.state = getValue(value: details["State"])
            self.homePhone = getValue(value: details["HmPhone"]).toPhoneNumber()
            self.workPhone = getValue(value: details["work_phone"]).toPhoneNumber()
            self.cellPhone = getValue(value: details["cellular_phone"]).toPhoneNumber()
            self.country = getValue(value: details["Country"])
            self.lastName = getValue(value: details["LName"])
            self.preferredName = getValue(value: details["Preferred"])
            self.maritalStatus = getValue(value: details["marital_status"]) == "" ? nil : MaritalStatus(rawValue: getValue(value: details["marital_status"]))
            self.middleInitial = getValue(value: details["MiddleI"])
        }
        
        func getValue(value: AnyObject?) -> String {
            if value is String {
                return (value as! String).uppercased()
            }
            return ""
        }
    }
#endif
