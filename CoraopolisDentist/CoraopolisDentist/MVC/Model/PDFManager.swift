
//
//  PDFManager.swift
//  FutureDentistry
//
//  Created by Office on 2/21/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

let kKeychainItemLoginName = "Coraopolisdentist: Google Login"
let kKeychainItemName = "Coraopolisdentist: Google Drive"
#if AUTO
let kClientId = "798498427737-bd6in786ba3leaphae89nq3bojntv108.apps.googleusercontent.com"
let kClientSecret = "2FFvkqiceK6OHWE12EqboxW1"
private let kFolderName = "CoraopolisDentistAuto"
#else
let kClientId = "176224296740-cnnj6k9ovinu4mg6ple931gceshsetp5.apps.googleusercontent.com"
let kClientSecret = "Iz3Z6ICpEy2hqvxS1KG5EdxZ"
private let kFolderName = "CoraopolisDentist"
#endif
    
var sharedPDFManager: PDFManager!

class PDFManager: NSObject, GIDSignInUIDelegate, GIDSignInDelegate  {
    
    var service : GTLServiceDrive!
    var credentials : GTMOAuth2Authentication!
    var authViewController: UIViewController!
    var completion: ((_ success : Bool) -> Void)!
    
    class func sharedInstance() -> PDFManager {
        if sharedPDFManager == nil {
            sharedPDFManager = PDFManager()
        }
        return sharedPDFManager
    }

    
    fileprivate func driveService() -> GTLServiceDrive {
        if (service == nil)
        {
            service = GTLServiceDrive()
            
            // Have the service object set tickets to fetch consecutive pages
            // of the feed so we do not need to manually fetch them.
            service.shouldFetchNextPages = true
            
            // Have the service object set tickets to retry temporary error conditions
            // automatically.
            service.isRetryEnabled = true
        }
        return service
    }
    
    
    func createPDFForView(_ view : UIView, fileName : String!, patient : PDPatient, completionBlock:(_ finished : Bool) -> Void) {
        let pdfData = NSMutableData()
        let pageSize = screenSize
        UIGraphicsBeginPDFContextToData(pdfData, CGRect.zero, nil)
        let pdfContext : CGContext = UIGraphicsGetCurrentContext()!
        UIGraphicsBeginPDFPageWithInfo(CGRect(x: 0, y: 0, width: pageSize.width, height: pageSize.height), nil)
        view.layer.render(in: pdfContext)
        UIGraphicsEndPDFContext()
        self.savePDF(pdfData as Data, fileName: fileName, patient: patient, completionBlock: completionBlock)
    }
    
    func createPDFForScrollView(_ scrollView : UIScrollView, fileName : String, patient : PDPatient, completionBlock:(_ finished : Bool) -> Void) {
        let pdfData = NSMutableData()
        let scrollHeight = scrollView.contentSize.height
        let rawNumberOfPages = scrollHeight / screenSize.height
        let numberOfPages = Int(ceil(rawNumberOfPages))
        var pageNumber = Int()
        let pageSize = screenSize
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
        UIGraphicsBeginPDFContextToData(pdfData, CGRect.zero, nil)
        let pdfContext : CGContext = UIGraphicsGetCurrentContext()!
        repeat {
            UIGraphicsBeginPDFPageWithInfo(CGRect(x: 0, y: 0, width: pageSize.width, height: pageSize.height), nil)
            if pageNumber < 1 {
                scrollView.layer.render(in: pdfContext)
            } else if pageNumber >= 1 {
                let offsetForScroll = CGFloat(pageNumber) * screenSize.height
                scrollView.setContentOffset(CGPoint(x: 0, y: offsetForScroll), animated: false)
                UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -offsetForScroll)
                scrollView.layer.render(in: pdfContext)
            }
            pageNumber = pageNumber + 1
        }
            while pageNumber < numberOfPages
        UIGraphicsEndPDFContext()
        self.savePDF(pdfData as Data, fileName: fileName, patient: patient, completionBlock: completionBlock)
    }
    
    func savePDF(_ pdfData : Data, fileName : String, patient : PDPatient, completionBlock:(_ finished : Bool) -> Void) {
        do {
            // save as a local file
            #if AUTO
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM dd, yyyy"
                let birthDate = dateFormatter.date(from: patient.dateOfBirth)
                dateFormatter.dateFormat = "yyyy'_'MM'_'dd"
                let dateString = dateFormatter.string(from: NSDate() as Date).uppercased()
                let birthDateString = dateFormatter.string(from: birthDate!)
                let patientId = patient.patientDetails != nil ? patient.patientDetails!.patientNumber! : "0"
                let name = "\(patient.firstName!.lowercased().capitalizingFirstLetter())_\(patient.lastName!.lowercased().capitalizingFirstLetter())-\(dateString)-\(fileName.lowercased().capitalizingFirstLetter())-\(birthDateString)-\(patientId).pdf"
                
            #else
                let documentsPath = NSTemporaryDirectory()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM'_'dd'_'yyyy"
                let dateString = dateFormatter.string(from: Date()).uppercased()
                let name = "\(patient.fullName.fileName.lowercased().capitalizingFirstLetter())_\(dateString)_\(fileName.lowercased().capitalizingFirstLetter()).pdf"
            #endif
            let path = "\(documentsPath)/\(name)"
            try pdfData.write(to: URL(fileURLWithPath: path), options: .atomic)
            self.uploadFileToDrive(path, fileName: name)
            completionBlock(true)
        } catch _ as NSError {
            completionBlock(false)
        }
    }
    
    func uploadFileToDrive(_ path : String, fileName : String) {
        
        
        func uploadFile(_ identitifer : String) {
            let driveFile = GTLDriveFile()
            driveFile.mimeType = fileName.hasSuffix(".pdf") ? "application/pdf" : "image/jpeg"
            driveFile.originalFilename = "\(fileName)"
            driveFile.name = "\(fileName)"
            driveFile.parents = [identitifer]
            
            let formData = try! Data(contentsOf: URL(fileURLWithPath: path))
            
            let uploadParameters = GTLUploadParameters(data: formData, mimeType: fileName.hasSuffix(".pdf") ? "application/pdf" : "image/jpeg")
            let query = GTLQueryDrive.queryForFilesCreate(withObject: driveFile, uploadParameters: uploadParameters)
            query?.addParents = identitifer
            
            self.driveService().executeQuery(query!, completionHandler: { (ticket, uploadedFile, error) -> Void in
                if (error == nil) {
                    
                    #if AUTO
                        let qos = DispatchQoS(qosClass: .background, relativePriority: 0)
                        let backgroundQueue = DispatchQueue.global(qos: qos.qosClass)
                        backgroundQueue.async {
                            let manager = AFHTTPSessionManager(baseURL: NSURL(string: hostUrl) as URL?)
                            manager.responseSerializer.acceptableContentTypes = ["text/html"]
                            manager.post("consent_form_upload_to_server.php", parameters: nil, constructingBodyWith: { (formData) in
                                do {
                                    try formData.appendPart(withFileURL: NSURL(fileURLWithPath: path) as URL, name: "consent_file", fileName: fileName, mimeType: "application/pdf")
                                } catch {
                                    
                                }
                            }, progress: { (progress) in
                                
                            }, success: { (task, result) in
                                
                            }) { (task, error) in
                                
                            }
                        }
                    #endif
                } else {
                    
                }
            })
        }

        
       /* func uploadFile(_ identitifer : String) {
            let driveFile = GTLDriveFile()
            driveFile.mimeType = "application/pdf"
            driveFile.originalFilename = fileName
            driveFile.name = fileName
            driveFile.parents = [identitifer]
            
            let uploadParameters = GTLUploadParameters(data: try! Data(contentsOf: URL(fileURLWithPath: path)), mimeType: "application/pdf")
            let query = GTLQueryDrive.queryForFilesCreate(withObject: driveFile, uploadParameters: uploadParameters)
            query?.addParents = identitifer
            
            self.driveService().executeQuery(query!, completionHandler: { (ticket, uploadedFile, error) -> Void in
                if (error == nil) {
                    let fileManager = FileManager.default
                    if fileManager.fileExists(atPath: path) {
                        do {
                            try fileManager.removeItem(atPath: path)
                        } catch  {
                            
                        }
                    }
                } else {
                }
            })
        }
        
        func createFolder(_ folderName : String, parent : [String]?) {
            let folderObj = GTLDriveFile()
            folderObj.name = folderName
            if parent != nil {
                folderObj.parents = parent
            }
            folderObj.mimeType = "application/vnd.google-apps.folder"
            
            let queryFolder = GTLQueryDrive.queryForFilesCreate(withObject: folderObj, uploadParameters: nil)
            
            self.driveService().executeQuery(queryFolder!, completionHandler: { (ticket, result, error) -> Void in
                if (error == nil) {
                    let folder = result as! GTLDriveFile
                    if parent == nil {
                        checkAndCreateSubFolder(folder)
                    } else {
                        uploadFile(folder.identifier)
                    }
                } else {
                    
                }
            })
        }
        
        func checkAndCreateSubFolder(_ folder : GTLDriveFile) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM'-'dd'-'yyyy"
            let folderName = dateFormatter.string(from: NSDate() as Date).uppercased()
            
            let folderDateQuery = GTLQueryDrive.queryForFilesList()
            folderDateQuery?.q = "mimeType = 'application/vnd.google-apps.folder' and name = '\(folderName)' and trashed = false and '\(folder.identifier)' in parents"
            
            self.driveService().executeQuery(folderDateQuery!, completionHandler: { (ticket, obj, error) -> Void in
                if error == nil {
                    let childFolder = (obj as! GTLDriveFileList).files
                    if childFolder != nil && (childFolder?.count)! > 0 {
                        let dateFolder = childFolder?[0] as! GTLDriveFile
                        uploadFile(dateFolder.identifier)
                    } else {
                        createFolder(folderName, parent: [folder.identifier])
                    }
                } else {
                }
            })
        }*/
        
        func checkAndCreateFolder(_ folderName: String, parent: GTLDriveFile?, completion: @escaping ((_ success: Bool, _ folder: GTLDriveFile) -> Void)) {
            let folderDateQuery = GTLQueryDrive.queryForFilesList()
            folderDateQuery?.q = parent == nil ? "mimeType = 'application/vnd.google-apps.folder' and name = '\(folderName)' and trashed = false" : "mimeType = 'application/vnd.google-apps.folder' and name = '\(folderName)' and trashed = false and '\(parent!.identifier!)' in parents"
            
            self.driveService().executeQuery(folderDateQuery!, completionHandler: { (ticket, obj, error) -> Void in
                if error == nil {
                    let childFolder = (obj as! GTLDriveFileList).files
                    if childFolder != nil && (childFolder?.count)! > 0 {
                        let dateFolder = childFolder?[0] as! GTLDriveFile
                        completion(true, dateFolder)
                    } else {
                        createNewFolder(folderName, parent: parent == nil ? nil : [parent!.identifier!], createCompletion: { (success, returnFolder) in
                            if success {
                                completion(true, returnFolder!)
                            }
                        })
                    }
                } else {
                }
            })
        }
        func createNewFolder(_ folderName : String, parent : [String]?, createCompletion: @escaping ((_ success: Bool, _ returnFolder: GTLDriveFile?) -> Void)) {
            let folderObj = GTLDriveFile()
            folderObj.name = folderName
            if parent != nil {
                folderObj.parents = parent
            }
            folderObj.mimeType = "application/vnd.google-apps.folder"
            
            let queryFolder = GTLQueryDrive.queryForFilesCreate(withObject: folderObj, uploadParameters: nil)
            
            self.driveService().executeQuery(queryFolder!, completionHandler: { (ticket, result, error) -> Void in
                if (error == nil) {
                    let folder = result as! GTLDriveFile
                    createCompletion(true, folder)
                } else {
                    createCompletion(false, nil)
                }
            })
        }
        
        let name = "\(UIDevice.current.name)_" + kFolderName
        checkAndCreateFolder(name, parent: nil) { (success, folder) in
            if success {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM'-'dd'-'yyyy"
                let folderName = dateFormatter.string(from: NSDate() as Date).uppercased()
                
                checkAndCreateFolder(folderName.fileName, parent: folder, completion: { (success, folder) in
                    uploadFile(folder.identifier!)
                })
            }
        }
        
       
    }
    func authorizeDrive(_ viewController : UIViewController, completion: @escaping (_ success : Bool) -> Void) {
        credentials = GTMOAuth2ViewControllerTouch.authForGoogleFromKeychain(forName: kKeychainItemName, clientID: kClientId, clientSecret: kClientSecret)
            if credentials.canAuthorize {
            self.driveService().authorizer = credentials
            completion(true)
        } else {
                self.completion = completion
                self.authViewController = viewController
                var configureError: NSError?
                GGLContext.sharedInstance().configureWithError(&configureError)
                assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
                
                GIDSignIn.sharedInstance().uiDelegate = self
                GIDSignIn.sharedInstance().delegate = self
                GIDSignIn.sharedInstance().scopes = ["https://www.googleapis.com/auth/drive"]
                GIDSignIn.sharedInstance().signIn()
        }
    }
    
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        authViewController.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        authViewController.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if error == nil {
            // ...
            self.driveService().authorizer = user.authentication.fetcherAuthorizer()
            self.completion(true)
        } else {
            self.completion(false)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }

}
