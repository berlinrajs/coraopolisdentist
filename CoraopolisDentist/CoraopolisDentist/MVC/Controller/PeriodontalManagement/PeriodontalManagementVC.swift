//
//  PeriodontalManagementVC.swift
//  CoraopolisDentist
//
//  Created by Manjusha Chembra on 4/19/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import Foundation

class PeriodontalManagementVC: PDViewController {
    
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var signatureProvider : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var radioAgreement: RadioButton!
    @IBOutlet weak var textFieldQuadrant1: PDTextField!
    @IBOutlet weak var textFieldQuadrant2: PDTextField!
    @IBOutlet weak var textFieldQuadrant3: PDTextField!
    @IBOutlet weak var textFieldQuadrant4: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.endEditing(true)
        labelDate.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
        loadValue()
        // Do any additional setup after loading the view.
    }
    
    
    func loadValue()  {
        
        labelPatientName.text = patient.fullName
        labelDate.todayDate = patient.dateToday
        radioAgreement.setSelectedWithTag(patient.agreementSelected)
        
         textFieldQuadrant1.text = patient.quadrant1
         textFieldQuadrant2.text = patient.quadrant2
         textFieldQuadrant3.text = patient.quadrant3
         textFieldQuadrant4.text = patient.quadrant4
        
    }
    
    func saveValue()  {

        patient.agreementSelected = radioAgreement.selected == nil ? 0 : radioAgreement.selected.tag
        patient.quadrant1 = textFieldQuadrant1.text!
        patient.quadrant2 = textFieldQuadrant2.text!
        patient.quadrant3 = textFieldQuadrant3.text!
        patient.quadrant4 = textFieldQuadrant4.text!
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        self.view.endEditing(true)
        if radioAgreement.selected == nil{
            self.showAlert("PLEASE ACCEPT OR DECLINE TREATMENT")
        }else if !signaturePatient.isSigned() || !signatureProvider.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValue()
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kPeriodontalManagementFormVC") as! PeriodontalManagementFormVC
            formVC.patient = self.patient
            formVC.patientSign = signaturePatient.signatureImage()
            formVC.providerSign = signatureProvider.signatureImage()
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
    
}
