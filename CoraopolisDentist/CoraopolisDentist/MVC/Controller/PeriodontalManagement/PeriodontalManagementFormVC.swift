//
//  PeriodontalManagementFormVC.swift
//  CoraopolisDentist
//
//  Created by Manjusha Chembra on 4/19/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import Foundation

class PeriodontalManagementFormVC: PDViewController {
    
    var patientSign : UIImage!
    var providerSign : UIImage!
    
    @IBOutlet var imageviewPatientSign1 : UIImageView!
    @IBOutlet var imageviewPatientSign2 : UIImageView!
    @IBOutlet var imageviewProviderSign : UIImageView!
    @IBOutlet var labelDate1 : FormLabel!
    @IBOutlet var labelDate2 : FormLabel!
    @IBOutlet var labelDate3 : FormLabel!
    @IBOutlet var labelQuadrant1 : FormLabel!
    @IBOutlet var labelQuadrant2 : FormLabel!
    @IBOutlet var labelQuadrant3 : FormLabel!
    @IBOutlet var labelQuadrant4 : FormLabel!
    
    @IBOutlet var labelPatientName : FormLabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate3.text = patient.dateToday
        labelPatientName.text = patient.fullName
        imageviewProviderSign.image = providerSign
        labelQuadrant1.text = patient.quadrant1
        labelQuadrant2.text = patient.quadrant2
        labelQuadrant3.text = patient.quadrant3
        labelQuadrant4.text = patient.quadrant4
        if patient.agreementSelected == 1
        {
            imageviewPatientSign1.image = patientSign
            labelDate1.text = patient.dateToday
        }
        else if patient.agreementSelected == 2
        {
            imageviewPatientSign2.image = patientSign
            labelDate2.text = patient.dateToday
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
