//
//  PatientUpdateVC.swift
//  CoraopolisDentist
//
//  Created by Manjusha Chembra on 4/14/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import Foundation

class PatientUpdateVC: PDViewController {
    
    @IBOutlet weak var patientSignature: SignatureView!
    @IBOutlet weak var labelDate1: DateLabel!
    @IBOutlet weak var labelPAtientNAme: UILabel!
    @IBOutlet weak var textViewMedicalChange: MCTextView!
    @IBOutlet weak var textViewMedicationChange: MCTextView!
    @IBOutlet weak var radioUseDentalAppliance : RadioButton!
    @IBOutlet weak var viewAppliance : UIView!
    @IBOutlet var buttonApplianceList: [UIButton]!
    @IBOutlet weak var radioCPAPWear : RadioButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.endEditing(true)
        labelDate1.todayDate = patient.dateToday
        labelPAtientNAme.text = patient.fullName
        loadValues()
        // Do any additional setup after loading the view.
    }

    func loadValues()
    {
        if patient.useDentalAppliance == 1
        {
            viewAppliance.isUserInteractionEnabled = true
            viewAppliance.alpha = 1.0
        }
        else{
            viewAppliance.isUserInteractionEnabled = false
            viewAppliance.alpha = 0.5
        }
        textViewMedicalChange.delegate?.textViewDidBeginEditing?(textViewMedicalChange)
        textViewMedicalChange.textValue = patient.medicalHistoryChange
        textViewMedicalChange.delegate?.textViewDidEndEditing?(textViewMedicalChange)
        
        textViewMedicationChange.delegate?.textViewDidBeginEditing?(textViewMedicationChange)
        textViewMedicationChange.textValue = patient.medicationsChange
        textViewMedicationChange.delegate?.textViewDidEndEditing?(textViewMedicationChange)
        
        radioUseDentalAppliance.setSelectedWithTag(patient.useDentalAppliance)
        for button in buttonApplianceList {
            button.isSelected = patient.applianceList.contains(button.tag)
            
        }
        radioCPAPWear.setSelectedWithTag(patient.CPAPWear)
        
    }
    
    
    func saveValues()
    {
        patient.applianceList = []
        for button in buttonApplianceList {
            if button.isSelected == true
            {
                patient.applianceList.append(button.tag)
            }
        }
        patient.CPAPWear = radioCPAPWear.selected == nil ? 0 :radioCPAPWear.selected.tag
        patient.useDentalAppliance = radioUseDentalAppliance.selected.tag
        
        textViewMedicalChange.delegate?.textViewDidBeginEditing?(textViewMedicalChange)
        patient.medicalHistoryChange = textViewMedicalChange.textValue!
        textViewMedicalChange.delegate?.textViewDidEndEditing?(textViewMedicalChange)
        
        textViewMedicationChange.delegate?.textViewDidBeginEditing?(textViewMedicationChange)
          patient.medicationsChange = textViewMedicationChange.textValue!
        textViewMedicationChange.delegate?.textViewDidEndEditing?(textViewMedicationChange)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func applianceButtonAction(sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func onUseDentalAppliancetButtonPressed (withSender sender : RadioButton){
        if sender.tag == 2{
            viewAppliance.isUserInteractionEnabled = false
            viewAppliance.alpha = 0.5
            for button in buttonApplianceList {
                button.isSelected = false
            }
            patient.applianceList = []
            patient.CPAPWear = 0
             radioCPAPWear.deselectAllButtons()
            
        }
        else{
            viewAppliance.isUserInteractionEnabled = true
            viewAppliance.alpha = 1.0
        }
    }
    
    @IBAction func nextBtnAction(sender: AnyObject) {
        self.view.endEditing(true)
        if !patientSignature.isSigned()   {
            self.showAlert("PLEASE SIGN THE FORM")
        }  else if !labelDate1.dateTapped    {
            self.showAlert("PLEASE SELECT DATE")
        }else{
            saveValues()
            let form = self.storyboard?.instantiateViewController(withIdentifier: "kPatientUpdateFormVC") as! PatientUpdateFormVC
            form.patientSign = patientSignature.signatureImage()
            form.patient = patient
            self.navigationController?.pushViewController(form, animated: true)
            
        }
        
    }
    
}


