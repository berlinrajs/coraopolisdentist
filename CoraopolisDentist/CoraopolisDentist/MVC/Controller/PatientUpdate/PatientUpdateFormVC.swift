//
//  PatientUpdateFormVC.swift
//  CoraopolisDentist
//
//  Created by Manjusha Chembra on 4/14/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import Foundation

class PatientUpdateFormVC: PDViewController {
    
    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var labelDate1: FormLabel!
    @IBOutlet weak var signatureView: UIImageView!

    @IBOutlet weak var radioUseDentalAppliance : RadioButton!
    @IBOutlet var buttonApplianceList: [UIButton]!
    @IBOutlet weak var radioCPAPWear : RadioButton!
    @IBOutlet var medicationChangeLabel: [FormLabel]!
    @IBOutlet var medicalHistoryChangeLabel: [FormLabel]!
    var patientSign : UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        labelPatientName.text = patient.fullName
        labelDate1.text = patient.dateToday
        signatureView.image = patientSign
        patient.medicationsChange.setTextForArrayOfLabels(medicationChangeLabel)
        patient.medicalHistoryChange.setTextForArrayOfLabels(medicalHistoryChangeLabel)
        radioUseDentalAppliance.setSelectedWithTag(patient.useDentalAppliance)
        for button in buttonApplianceList {
            button.isSelected = patient.applianceList.contains(button.tag)
            
        }
        radioCPAPWear.setSelectedWithTag(patient.CPAPWear)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
