//
//  DentalRecordsFormViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/20/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class DentalRecordsFormViewController: PDViewController {
    
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelPracticeName : UILabel!
    @IBOutlet weak var labelPracticeAddress : UILabel!
    @IBOutlet weak var labelPracticePhoneNumber : UILabel!
    @IBOutlet weak var labelEmail : UILabel!
    @IBOutlet weak var labelPatientName1 : UILabel!
    @IBOutlet weak var imageviewSignature : UIImageView!
    @IBOutlet weak var labelRelationship : UILabel!
    @IBOutlet weak var labelDateOfBirth : UILabel!
    @IBOutlet      var labelPatientAddress : [UILabel]!
    @IBOutlet weak var labelPatientPhoneNumber : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.text = patient.dateToday
        labelPatientName.text = patient.fullName
        labelPracticeName.text = patient.dentalRecords.practiceName
        labelPracticeAddress.text = patient.dentalRecords.practiceAddress
        labelPracticePhoneNumber.text = patient.dentalRecords.practicePhoneNumber
        labelEmail.text = patient.dentalRecords.email
        labelPatientName1.text = patient.fullName
        imageviewSignature.image = patient.dentalRecords.signaturePatient
        labelRelationship.text = patient.dentalRecords.relationship
        labelDateOfBirth.text = patient.dateOfBirth
        patient.dentalRecords.patientAddress.setTextForArrayOfLabels(labelPatientAddress)
        labelPatientPhoneNumber.text = patient.dentalRecords.patientPhoneNumber
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
