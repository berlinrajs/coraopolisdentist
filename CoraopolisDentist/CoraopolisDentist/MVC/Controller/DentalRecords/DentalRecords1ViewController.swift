//
//  DentalRecords1ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/20/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class DentalRecords1ViewController: PDViewController {

    @IBOutlet weak var labelAuthorization : UILabel!
    @IBOutlet weak var textfieldPracticeName : UITextField!
    @IBOutlet weak var textfieldPracticeAddress : UITextField!
    @IBOutlet weak var textfieldPracticePhone : UITextField!
    @IBOutlet weak var textfieldEmail : UITextField!
    @IBOutlet weak var textfieldPatientAddress : UITextField!
    @IBOutlet weak var textfieldPatientPhone : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        labelAuthorization.text = labelAuthorization.text!.replacingOccurrences(of: "KPATIENTNAME", with: patient.fullName)
        let str : NSString = labelAuthorization.text! as NSString
        let range = str.range(of: patient.fullName)
        let attrStrng = NSMutableAttributedString(string: labelAuthorization.text!)
        attrStrng.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        labelAuthorization.attributedText = attrStrng
        
        textfieldPracticeName.setSavedText(patient.dentalRecords.practiceName)
        textfieldPracticeAddress.setSavedText(patient.dentalRecords.practiceAddress)
        textfieldPracticePhone.setSavedText(patient.dentalRecords.practicePhoneNumber)
        textfieldEmail.setSavedText(patient.dentalRecords.email)
        textfieldPatientAddress.setSavedText(patient.dentalRecords.patientAddress)
        textfieldPatientPhone.setSavedText(patient.dentalRecords.patientPhoneNumber)

    }
    
    func saveValue()  {
        patient.dentalRecords.practiceName = textfieldPracticeName.isEmpty ? "N/A" : textfieldPracticeName.text!
        patient.dentalRecords.practiceAddress = textfieldPracticeAddress.isEmpty ? "N/A" : textfieldPracticeAddress.text!
        patient.dentalRecords.practicePhoneNumber = textfieldPracticePhone.isEmpty ? "N/A" : textfieldPracticePhone.text!
        patient.dentalRecords.email = textfieldEmail.isEmpty ? "N/A" : textfieldEmail.text!
        patient.dentalRecords.patientAddress = textfieldPatientAddress.isEmpty ? "N/A" : textfieldPatientAddress.text!
        patient.dentalRecords.patientPhoneNumber = textfieldPatientPhone.isEmpty ? "N/A" : textfieldPatientPhone.text!
        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
//        if textfieldPatientAddress.isEmpty{
//            self.showAlert("PLEASE ENTER THE PATIENT ADDRESS")
//        }else
        if (!textfieldPatientPhone.isEmpty && !textfieldPatientPhone.text!.isPhoneNumber){
            self.showAlert("PLEASE ENTER THE VALID PATIENT PHONE NUMBER")
        }else if (!textfieldPracticePhone.isEmpty && !textfieldPracticePhone.text!.isPhoneNumber){
            self.showAlert("PLEASE ENTER THE VALID PRACTICE PHONE NUMBER")
        }else if !textfieldEmail.isEmpty && !textfieldEmail.text!.isValidEmail{
            self.showAlert("PLEASE ENTER THE VALID EMAIL")
        }
        else{
            saveValue()
            let dental = consentStoryboard.instantiateViewController(withIdentifier: "DentalRecords2VC") as! DentalRecords2ViewController
            dental.patient = self.patient
            self.navigationController?.pushViewController(dental, animated: true)
        }
        
    }


}

extension DentalRecords1ViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldPracticePhone || textField == textfieldPatientPhone{
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

