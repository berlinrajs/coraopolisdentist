//
//  DentalRecords2ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/20/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class DentalRecords2ViewController: PDViewController {
    @IBOutlet weak var radioSign : RadioButton!
    @IBOutlet weak var textfieldRelationship : UITextField!
    @IBOutlet weak var viewContainer : UIView!
    @IBOutlet weak var signatureViewPatient : SignatureView!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelNameTitle : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        labelDate1.todayDate = patient.dateToday
        radioSign.setSelectedWithTag(patient.dentalRecords.signedTag)
        if patient.dentalRecords.signedTag == 1{
            viewContainer.isUserInteractionEnabled = false
            viewContainer.alpha = 0.5
            textfieldRelationship.text = ""
            labelPatientName.text = patient.fullName
            labelNameTitle.text = "Patient Signature"
        }else{
            viewContainer.isUserInteractionEnabled = true
            viewContainer.alpha = 1.0
            textfieldRelationship.setSavedText(patient.dentalRecords.relationship)
            labelPatientName.text = ""
            labelNameTitle.text = "Guardian Signature"

        }
        
    }
    
    func saveValue()  {
        patient.dentalRecords.signedTag = radioSign.selected.tag
        patient.dentalRecords.relationship = radioSign.selected.tag == 1 ? "N/A" : textfieldRelationship.isEmpty ? "N/A" : textfieldRelationship.text!
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if radioSign.selected.tag == 2 && textfieldRelationship.isEmpty{
            self.showAlert("PLEASE ENTER THE RELATIONSHIP TO PATIENT")
        }else if !signatureViewPatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
        saveValue()
        patient.dentalRecords.signaturePatient = signatureViewPatient.signatureImage()
            let dental = consentStoryboard.instantiateViewController(withIdentifier: "DentalRecordsFormVC") as! DentalRecordsFormViewController
            dental.patient = self.patient
            self.navigationController?.pushViewController(dental, animated: true)

        }
        
    }
    
    @IBAction func onSignedButtonPressed (withSender sender : RadioButton){
        if sender.tag == 1{
            viewContainer.isUserInteractionEnabled = false
            viewContainer.alpha = 0.5
            textfieldRelationship.text = ""
            labelPatientName.text = patient.fullName
            labelNameTitle.text = "Patient Signature"

        }else{
            viewContainer.isUserInteractionEnabled = true
            viewContainer.alpha = 1.0
            labelPatientName.text = ""
            labelNameTitle.text = "Guardian Signature"

        }

    }

}

extension DentalRecords2ViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

