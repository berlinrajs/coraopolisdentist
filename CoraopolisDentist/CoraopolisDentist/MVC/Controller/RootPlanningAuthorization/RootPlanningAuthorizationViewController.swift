//
//  RootPlanningAuthorizationViewController.swift
//  CoraopolisDentist
//
//  Created by Office on 12/17/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class RootPlanningAuthorizationViewController: PDViewController {

    @IBOutlet weak var initials: SignatureView!
    
    @IBOutlet weak var signatureDoctor: SignatureView!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelDate1 : DateLabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.todayDate = patient.dateToday
        labelDate1.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !initials.isSigned() {
            self.showAlert("PLEASE FILL INITIALS")
        } else if !signaturePatient.isSigned() || !signatureDoctor.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped || !labelDate1.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "RootPlanningAuthorizationFormVC") as! RootPlanningAuthorizationFormViewController
            formVC.patient = self.patient
            formVC.patientSign = signaturePatient.signatureImage()
            formVC.doctorSign = signatureDoctor.signatureImage()
            formVC.initials = initials.signatureImage()
            self.navigationController?.pushViewController(formVC, animated: true)
        }
        
    }

}
