//
//  RootPlanningAuthorizationFormViewController.swift
//  CoraopolisDentist
//
//  Created by Office on 12/17/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class RootPlanningAuthorizationFormViewController: PDViewController {

    var patientSign : UIImage!
    var doctorSign : UIImage!
    var initials : UIImage!

    @IBOutlet var imageviewInitials : UIImageView!
    @IBOutlet var imageviewPatientSign : UIImageView!
    @IBOutlet var imageviewDoctorSign : UIImageView!
    @IBOutlet var labelDate : UILabel!
    @IBOutlet var labelDate1 : UILabel!
    @IBOutlet var labelPatientName : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageviewInitials.image = initials
        imageviewPatientSign.image = patientSign
        imageviewDoctorSign.image = doctorSign
        labelDate.text = patient.dateToday
        labelDate1.text = patient.dateToday

        labelPatientName.text = patient.fullName
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
