//
//  Photography1ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/20/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class Photography1ViewController: PDViewController {

    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var radioPhotoType : RadioButton!
    @IBOutlet var arrayPhotoUseButtons : [UIButton]!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue(){
        labelDate.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
        radioPhotoType.setSelectedWithTag(patient.photoTypeTag)
        for btn in arrayPhotoUseButtons{
            btn.isSelected = patient.photoUseTags.contains(btn.tag)
        }

    }
    
    func saveValue (){
        patient.photoTypeTag = radioPhotoType.selected == nil ? 0 : radioPhotoType.selected.tag
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }

    @IBAction func onPhotoUseButtonPressed (withSender sender : UIButton){
        sender.isSelected = !sender.isSelected
        if patient.photoUseTags.contains(sender.tag){
            patient.photoUseTags.remove(at: patient.photoUseTags.index(of: sender.tag)!)
        }else{
            patient.photoUseTags.append(sender.tag)
        }
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValue()
            let photo = consentStoryboard.instantiateViewController(withIdentifier: "PhotographyFormVC") as! PhotographyFormViewController
            photo.patientSign = signaturePatient.signatureImage()
            photo.patient = self.patient
            self.navigationController?.pushViewController(photo, animated: true)
        }
        
    }


}
