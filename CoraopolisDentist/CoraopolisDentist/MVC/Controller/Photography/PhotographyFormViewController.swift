//
//  PhotographyFormViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/20/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PhotographyFormViewController: PDViewController {

    var patientSign : UIImage!
    @IBOutlet var imageviewPatientSign : UIImageView!
    @IBOutlet var labelDate : UILabel!
    @IBOutlet var labelPatientName : UILabel!
    @IBOutlet weak var radioPhotoType : RadioButton!
    @IBOutlet var arrayPhotoUseButtons : [UIButton]!

    override func viewDidLoad() {
        super.viewDidLoad()

        imageviewPatientSign.image = patientSign
        labelDate.text = patient.dateToday
        labelPatientName.text = patient.fullName
        radioPhotoType.setSelectedWithTag(patient.photoTypeTag)
        for btn in arrayPhotoUseButtons{
            btn.isSelected = patient.photoUseTags.contains(btn.tag)
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
