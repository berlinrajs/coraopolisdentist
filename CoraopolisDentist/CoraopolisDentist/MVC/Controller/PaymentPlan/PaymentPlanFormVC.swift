//
//  PaymentPlanFormVC.swift
//  CoraopolisDentist
//
//  Created by Manjusha Chembra on 4/19/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import Foundation

class PaymentPlanFormVC: PDViewController {
    
    var patientSignature : UIImage!
    var staffSignature : UIImage!
    
    @IBOutlet var imageviewPatientSign : UIImageView!
    @IBOutlet var labelDate : FormLabel!
    @IBOutlet var labelDateOfService : FormLabel!
    @IBOutlet var labelPatientName : FormLabel!
    @IBOutlet var imageviewStaffSign : UIImageView!
    @IBOutlet var labelPaymentAmt : FormLabel!
    @IBOutlet var labelCardNumber : FormLabel!
    @IBOutlet var labelDueDate : FormLabel!
    @IBOutlet var labelExpDate : FormLabel!
    @IBOutlet weak var radioCardType: RadioButton!
    @IBOutlet var procedureLabel: [FormLabel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageviewPatientSign.image = patientSignature
        labelDate.text = patient.dateToday
        labelDateOfService.text = patient.dateToday
        labelPatientName.text = patient.fullName
        labelPaymentAmt.text = patient.paymentAmt
        labelCardNumber.text = patient.creditCardNumber
        labelDueDate.text = patient.dueDate
        labelExpDate.text = patient.expiryDate
        
        imageviewStaffSign.image = staffSignature
        radioCardType.setSelectedWithTag(patient.cardType)
        patient.procedure.setTextForArrayOfLabels(procedureLabel)
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
