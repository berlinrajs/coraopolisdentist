//
//  PaymentPlanVC.swift
//  CoraopolisDentist
//
//  Created by Manjusha Chembra on 4/19/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import Foundation

class PaymentPlanVC: PDViewController {
    
    @IBOutlet weak var radioButtonCardType: RadioButton!
    @IBOutlet weak var textViewProcedure: MCTextView!
    @IBOutlet weak var textFieldExpDate: PDTextField!
    @IBOutlet weak var textFieldCardNumber: PDTextField!
    @IBOutlet weak var textFieldPaymentAmt: PDTextField!
    @IBOutlet weak var textFieldDueDate: PDTextField!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var signatureView1: SignatureView!
    @IBOutlet var labelPatientName : UILabel!
    @IBOutlet weak var paymentView: UIView!
    
    var datePicker : DatePickerView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.view.endEditing(true)
        paymentView.backgroundColor = UIColor.white.withAlphaComponent(0.25)
        labelDate.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName

        DateInputView.addDatePickerForTextField(textFieldDueDate)
        datePicker = DatePickerView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        let gregorian = Calendar(identifier: Calendar.Identifier.gregorian)
        
        let year = gregorian.component(.year, from: Date())
        datePicker.minYear = year
        datePicker.maxYear = year + 5
        datePicker.selectToday()
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 44))
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePressed))
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil), barbuttonDone]
        
        textFieldExpDate.inputView = datePicker
        textFieldExpDate.inputAccessoryView = toolbar

        
        // Do any additional setup after loading the view.
    }
    func donePressed() {
        textFieldExpDate.resignFirstResponder()
        textFieldExpDate.text = datePicker.date
    }
    
   
    
    func saveValues() {
        
        patient.cardType = radioButtonCardType.selected == nil ? 0 : radioButtonCardType.selected.tag
        patient.creditCardNumber = textFieldCardNumber.text!
        patient.expiryDate = textFieldExpDate.text
        patient.dueDate = textFieldDueDate.text
        patient.paymentAmt = textFieldPaymentAmt.text
        self.textViewProcedure.delegate?.textViewDidBeginEditing?(textViewProcedure)
        patient.procedure = textViewProcedure.textValue!
        self.textViewProcedure.delegate?.textViewDidEndEditing?(textViewProcedure)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(sender : AnyObject) {
        self.view.endEditing(true)
        if radioButtonCardType.selected == nil {
            self.showAlert("PLEASE SELECT CARD TYPE")
        } else if let _ = findEmptyTextField() {
            self.showAlert("PLEASE ENTER ALL REQUIRED FIELDS")
        } else if !textFieldCardNumber.text!.isCreditCard {
            self.showAlert("PLEASE ENTER VALID CARD NUMBER")
        } else if !signatureView.isSigned() || !signatureView1.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT DATE")
        } else {
            saveValues()
            
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kPaymentPlanFormVC") as! PaymentPlanFormVC
            formVC.patient = self.patient
            formVC.patientSignature = signatureView.signatureImage()
            formVC.staffSignature = signatureView1.signatureImage()
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
    
    func findEmptyTextField() -> UITextField? {
        let textFields = [textFieldDueDate, textFieldPaymentAmt, textFieldExpDate, textFieldCardNumber] as [UITextField]
        for textField in textFields {
            if (textField.isEmpty) {
                return textField 
            }
        }
        return nil
    }
    
}
extension PaymentPlanVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldCardNumber {
            return textField.formatCreditCardNumber(range, string: string)
        }else {
            return textField.formatNumbers(range, string: string, count: 10)
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

