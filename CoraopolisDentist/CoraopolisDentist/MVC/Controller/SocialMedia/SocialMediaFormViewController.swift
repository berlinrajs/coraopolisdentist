//
//  SocialMediaFormViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/19/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SocialMediaFormViewController: PDViewController {

    var patientSign : UIImage!
    @IBOutlet var imageviewPatientSign : UIImageView!
    @IBOutlet var labelDate : UILabel!
    @IBOutlet var labelPatientName : UILabel!
    @IBOutlet var labelPatientName1 : UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()

        imageviewPatientSign.image = patientSign
        labelDate.text = patient.dateToday
        labelPatientName.text = patient.fullName
        labelPatientName1.text = patient.fullName

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
