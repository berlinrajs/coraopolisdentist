//
//  PreviousRecords1ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/21/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PreviousRecords1ViewController: PDViewController {
    
    @IBOutlet weak var textfieldPatientPhoneNumber : UITextField!
    @IBOutlet weak var textfieldOfficeMembers : UITextField!
    @IBOutlet weak var textfieldDentistName : UITextField!
    @IBOutlet weak var textfieldDentistAddress : UITextField!
    @IBOutlet weak var textfieldDentistCity : UITextField!
    @IBOutlet weak var textfieldDentistState : UITextField!
    @IBOutlet weak var textfieldDentistZipcode : UITextField!
    @IBOutlet weak var textfieldDentistPhoneNumber : UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        StateListView.addStateListForTextField(textfieldDentistState)
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        textfieldPatientPhoneNumber.setSavedText(patient.previousRecords.patientPhoneNumber)
        textfieldOfficeMembers.setSavedText(patient.previousRecords.familyMembers)
        textfieldDentistName.setSavedText(patient.previousRecords.dentistName)
        textfieldDentistAddress.setSavedText(patient.previousRecords.dentistAddress)
        textfieldDentistCity.setSavedText(patient.previousRecords.dentistCity)
        textfieldDentistState.setSavedText(patient.previousRecords.dentistState)
        textfieldDentistZipcode.setSavedText(patient.previousRecords.dentistZipcode)
        textfieldDentistPhoneNumber.setSavedText(patient.previousRecords.dentistPhoneNumber)
        
    }
    
    func saveValue()  {
        patient.previousRecords.patientPhoneNumber = textfieldPatientPhoneNumber.isEmpty ? "N/A" : textfieldPatientPhoneNumber.text!
        patient.previousRecords.familyMembers = textfieldOfficeMembers.isEmpty ? "N/A" : textfieldOfficeMembers.text!
        patient.previousRecords.dentistName = textfieldDentistName.isEmpty ? "N/A" : textfieldDentistName.text!
        patient.previousRecords.dentistAddress = textfieldDentistAddress.isEmpty ? "N/A" : textfieldDentistAddress.text!
        patient.previousRecords.dentistCity = textfieldDentistCity.isEmpty ? "N/A" : textfieldDentistCity.text!
        patient.previousRecords.dentistState = textfieldDentistState.isEmpty ? "PA" : textfieldDentistState.text!
        patient.previousRecords.dentistZipcode = textfieldDentistZipcode.isEmpty ? "N/A" : textfieldDentistZipcode.text!
        patient.previousRecords.dentistPhoneNumber = textfieldDentistPhoneNumber.isEmpty ? "N/A" : textfieldDentistPhoneNumber.text!
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if (!textfieldPatientPhoneNumber.isEmpty && !textfieldPatientPhoneNumber.text!.isPhoneNumber) {
            self.showAlert("PLEASE ENTER THE VALID PHONE NUMBER")
        }else if (!textfieldDentistPhoneNumber.isEmpty && !textfieldDentistPhoneNumber.text!.isPhoneNumber){
            self.showAlert("PLEASE ENTER THE VALID DENTIST PHONE NUMBER")
        }else if !textfieldDentistZipcode.isEmpty && !textfieldDentistZipcode.text!.isZipCode{
            self.showAlert("PLEASE ENTER THE VALID ZIPCODE")
        }else{
            saveValue()
            let dental = consentStoryboard.instantiateViewController(withIdentifier: "PreviousRecords2VC") as! PreviousRecords2ViewController
            dental.patient = self.patient
            self.navigationController?.pushViewController(dental, animated: true)
        }

    }


}

extension PreviousRecords1ViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldPatientPhoneNumber || textField == textfieldDentistPhoneNumber{
            return textField.formatPhoneNumber(range, string: string)
        }else if textField == textfieldDentistZipcode{
            return textField.formatZipCode(range, string: string)
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
