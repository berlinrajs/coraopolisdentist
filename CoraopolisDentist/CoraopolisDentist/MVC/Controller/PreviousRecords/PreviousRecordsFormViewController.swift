//
//  PreviousRecordsFormViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/21/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PreviousRecordsFormViewController: PDViewController {
    
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDateOfBirth : UILabel!
    @IBOutlet weak var labelPhoneNumber : UILabel!
    @IBOutlet weak var labelFamilyMembers : UILabel!
    @IBOutlet weak var labelPreviousDentistName : UILabel!
    @IBOutlet      var labelDentistAddress : [UILabel]!
    @IBOutlet weak var labelDentistCity : UILabel!
    @IBOutlet weak var labelDentistPhoneNumber : UILabel!
    @IBOutlet weak var imageviewSign : UIImageView!
    @IBOutlet weak var labelDate : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelPatientName.text = patient.fullName
        labelDateOfBirth.text = patient.dateOfBirth
        labelPhoneNumber.text = patient.previousRecords.patientPhoneNumber
        labelFamilyMembers.text = patient.previousRecords.familyMembers
        labelPreviousDentistName.text = patient.previousRecords.dentistName
        patient.previousRecords.dentistAddress.setTextForArrayOfLabels(labelDentistAddress)
        labelDentistCity.text = getCity()
        labelDentistPhoneNumber.text = patient.previousRecords.dentistPhoneNumber
        imageviewSign.image = patient.previousRecords.signPatient
        labelDate.text = patient.dateToday
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getCity() -> String {
        var strFinal : String = ""
        var arrayString : [String] = [String]()
        if patient.previousRecords.dentistCity != "N/A"{
            arrayString.append(" \(patient.previousRecords.dentistCity)")
        }
        arrayString.append(" \(patient.previousRecords.dentistState)")
        if patient.previousRecords.dentistZipcode != "N/A"{
            arrayString.append(" \(patient.previousRecords.dentistZipcode)")
        }
        strFinal = (arrayString as NSArray).componentsJoined(by: ",")

        
        return strFinal.characters.count == 3 ? "N/A" : strFinal
    }

}
