//
//  PreviousRecords2ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/21/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PreviousRecords2ViewController: PDViewController {
    @IBOutlet weak var labelNameTitle : UILabel!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        if patient.is18YearsOld == true{
            labelNameTitle.text = "Patient Signature"
            labelPatientName.text = patient.fullName
        }else{
            labelNameTitle.text = "Parent Signature"
            labelPatientName.text = ""

        }
        labelDate.todayDate = patient.dateToday

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            patient.previousRecords.signPatient = signaturePatient.signatureImage()
            let dental = consentStoryboard.instantiateViewController(withIdentifier: "PreviousRecordsFormVC") as! PreviousRecordsFormViewController
            dental.patient = self.patient
            self.navigationController?.pushViewController(dental, animated: true)

        }
        
    }
    


}
