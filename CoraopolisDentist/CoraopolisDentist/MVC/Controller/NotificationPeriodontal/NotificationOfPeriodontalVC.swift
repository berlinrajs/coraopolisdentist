//
//  NotificationOfPeriodontalVC.swift
//  AnthonyParella
//
//  Created by Manjusha Chembra on 2/9/17.
//  Copyright © 2017 Manjusha Chembra. All rights reserved.
//

import Foundation
class NotificationOfPeriodontalVC: PDViewController {
    
    
     @IBOutlet weak var radioPeriodontics: RadioButton!
    @IBOutlet weak var buttonPockets1: UIButton!
    @IBOutlet weak var buttonPockets2: UIButton!
    @IBOutlet weak var buttonPockets3: UIButton!
    @IBOutlet weak var buttonPockets4: UIButton!
    @IBOutlet weak var buttonPockets5: UIButton!

    
    @IBOutlet weak var textFieldTreatment: PDTextField!
    @IBOutlet weak var textFieldCareInterval: PDTextField!
    
    @IBOutlet weak var signaturePatient: SignatureView!
    @IBOutlet weak var signatureProvider: SignatureView!
    @IBOutlet weak var labelDate1: DateLabel!
    @IBOutlet weak var pateintNameLabel: UILabel!

    @IBOutlet weak var periodontalView: UIView!
    @IBOutlet weak var pocketView: UIView!
    
    var form : Forms!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.endEditing(true)
        periodontalView.backgroundColor = UIColor.white.withAlphaComponent(0.25)
        pocketView.backgroundColor = UIColor.white.withAlphaComponent(0.25)
    
        loadValue()
       
        // Do any additional setup after loading the view.
    }

    func loadValue()  {
        
        pateintNameLabel.text = patient.fullName
        labelDate1.todayDate = patient.dateToday
        
        radioPeriodontics.setSelectedWithTag(patient.periodontalType)
        buttonPockets1.isSelected = patient.pocketsSelected1
        buttonPockets2.isSelected = patient.pocketsSelected2
        buttonPockets3.isSelected = patient.pocketsSelected3
        buttonPockets4.isSelected = patient.pocketsSelected4
        buttonPockets5.isSelected = patient.pocketsSelected5
        textFieldTreatment.text = patient.treatmentRecommended
       textFieldCareInterval.text = patient.recomendedCareInterval
        
        
    }
    
    func saveValue()  {

        
        patient.periodontalType = radioPeriodontics.selected == nil ? 0 : radioPeriodontics.selected.tag
        patient.pocketsSelected1 = buttonPockets1.isSelected
        patient.pocketsSelected2 = buttonPockets2.isSelected
        patient.pocketsSelected3 = buttonPockets3.isSelected
        patient.pocketsSelected4 = buttonPockets4.isSelected
        patient.pocketsSelected5 = buttonPockets5.isSelected
        patient.treatmentRecommended = textFieldTreatment.text!
        patient.recomendedCareInterval = textFieldCareInterval.text!
        
    }

   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonAction( _ sender: UIButton) {
        sender.isSelected = !sender.isSelected

    }
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    @IBAction func buttonActionNext(sender: AnyObject) {
        self.view.endEditing(true)
        if radioPeriodontics.selected == nil || (buttonPockets1.isSelected == false && buttonPockets2.isSelected == false && buttonPockets3.isSelected == false && buttonPockets4.isSelected == false && buttonPockets5.isSelected == false) {
            self.showAlert("PLEASE ENTER ALL THE DETAILS NEEDED")
        }
        if !signaturePatient.isSigned() || !signatureProvider.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
        
            saveValue()
            let notificatioPeriodontalForm = self.storyboard?
                .instantiateViewController(withIdentifier: "kNotificationOfPeriodontalForm") as! NotificationOfPeriodontalForm
            notificatioPeriodontalForm.patient = self.patient
            notificatioPeriodontalForm.signPatient = signaturePatient.signatureImage()
            notificatioPeriodontalForm.signProvider = signatureProvider.signatureImage()
            self.navigationController?.pushViewController(notificatioPeriodontalForm, animated: true)
        }
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
