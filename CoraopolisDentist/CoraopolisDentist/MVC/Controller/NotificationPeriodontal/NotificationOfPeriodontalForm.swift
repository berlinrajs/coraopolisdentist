//
//  NotificationOfPeriodontalForm.swift
//  AnthonyParella
//
//  Created by Manjusha Chembra on 2/9/17.
//  Copyright © 2017 Manjusha Chembra. All rights reserved.
//

import Foundation
class NotificationOfPeriodontalForm: PDViewController {
    
    var signPatient : UIImage!
    var signProvider : UIImage!

    @IBOutlet weak var periodontitisLabel: FormLabel!
    @IBOutlet weak var buttonPockets1: UIButton!
    @IBOutlet weak var buttonPockets2: UIButton!
    @IBOutlet weak var buttonPockets3: UIButton!
    @IBOutlet weak var buttonPockets4: UIButton!
    @IBOutlet weak var buttonPockets5: UIButton!
    @IBOutlet weak var signaturePatient: UIImageView!
    @IBOutlet weak var signatureProvider: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelPateintName: UILabel!
    @IBOutlet weak var labelTreatment: FormLabel!
    @IBOutlet weak var labelCareInterval: FormLabel!
    @IBOutlet weak var labelPatientName1: FormLabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        
        
        periodontitisLabel.text = patient.periodontalType == 1 ? "Early Periodontitis" : patient.periodontalType == 2 ? "Moderate Periodontitis" : "Advanced Periodontitis"
        labelPateintName.text = patient.fullName
        labelPatientName1.text = patient.fullName
        signaturePatient.image = signPatient
        signatureProvider.image = signProvider
        labelDate.text = patient.dateToday
  
        
        buttonPockets1.isSelected = patient.pocketsSelected1
        buttonPockets2.isSelected = patient.pocketsSelected2
        buttonPockets3.isSelected = patient.pocketsSelected3
        buttonPockets4.isSelected = patient.pocketsSelected4
        buttonPockets5.isSelected = patient.pocketsSelected5
        labelTreatment.text = patient.treatmentRecommended
        labelCareInterval.text = patient.recomendedCareInterval

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
