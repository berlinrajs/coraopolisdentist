//
//  ChildRegistration4ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildRegistration4ViewController: PDViewController {
    @IBOutlet weak var labelHeading : UILabel!
    @IBOutlet weak var textfieldFirstName : UITextField!
    @IBOutlet weak var textfieldLastName : UITextField!
    @IBOutlet weak var textfieldAddress : UITextField!
    @IBOutlet weak var textfieldCity : UITextField!
    @IBOutlet weak var textfieldState : UITextField!
    @IBOutlet weak var textfieldZipcode : UITextField!
    @IBOutlet weak var textfieldPhoneNumber : UITextField!
    var isFather : Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        labelHeading.text = isFather == true ? "FATHER/GUARDIAN INFORMATION" : "MOTHER/GUARDIAN INFORMATION"
        StateListView.addStateListForTextField(textfieldState)
        let guardDetails : Guardian = isFather == true ? patient.child.father : patient.child.mother
        textfieldFirstName.setSavedText(guardDetails.firstName)
        textfieldLastName.setSavedText(guardDetails.lastName)
        textfieldAddress.setSavedText(guardDetails.address)
        textfieldCity.setSavedText(guardDetails.city)
        textfieldState.setSavedText(guardDetails.state)
        textfieldZipcode.setSavedText(guardDetails.zipcode)
        
        func load() {
            textfieldPhoneNumber.setSavedText(guardDetails.phoneNumber)
        }
        
        if isFather == true {
            #if AUTO
                if let patientDetails = patient.patientDetails {
                    textfieldPhoneNumber.text = guardDetails.phoneNumber == "N/A" ? patientDetails.homePhone : guardDetails.phoneNumber
                } else {
                    load()
                }
            #else
                load()
            #endif
        } else {
            load()
        }
        
    }
    
    func saveValue()  {
        let guardDetails : Guardian = isFather == true ? patient.child.father : patient.child.mother
        guardDetails.firstName = textfieldFirstName.getText()
        guardDetails.lastName = textfieldLastName.getText()
        guardDetails.address = textfieldAddress.getText()
        guardDetails.city = textfieldCity.getText()
        guardDetails.state = textfieldState.getText()
        guardDetails.zipcode = textfieldZipcode.getText()
        guardDetails.phoneNumber = textfieldPhoneNumber.getText()
        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldPhoneNumber.isEmpty && !textfieldPhoneNumber.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID PHONE NUMBER")
        }else if !textfieldZipcode.isEmpty && !textfieldZipcode.text!.isZipCode{
            self.showAlert("PLEASE ENTER THE VALID ZIPCODE")
        }else{
            saveValue()
            let child = childStoryBoard.instantiateViewController(withIdentifier: "Child5VC") as! ChildRegistration5ViewController
            child.patient = self.patient
            child.isFather = isFather
            self.navigationController?.pushViewController(child, animated: true)
        }
    }


}

extension ChildRegistration4ViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldZipcode {
            return textField.formatZipCode(range, string: string)
        }else if textField == textfieldPhoneNumber{
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
