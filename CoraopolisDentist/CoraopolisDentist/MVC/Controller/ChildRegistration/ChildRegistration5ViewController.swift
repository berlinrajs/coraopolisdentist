//
//  ChildRegistration5ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildRegistration5ViewController: PDViewController {
    @IBOutlet weak var labelHeading : UILabel!
    var isFather : Bool = true
    @IBOutlet weak var textfieldDateOfBirth : UITextField!
    @IBOutlet weak var textfieldSocialSecurity : UITextField!
    @IBOutlet weak var textfieldEmployer : UITextField!
    @IBOutlet weak var textfieldWorkPhone : UITextField!
    @IBOutlet weak var textfieldEmail : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        loadValue()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        labelHeading.text = isFather == true ? "FATHER/GUARDIAN INFORMATION" : "MOTHER/GUARDIAN INFORMATION"
        DateInputView.addDatePickerForDOBTextField(textfieldDateOfBirth)
        let guardDetails : Guardian = isFather == true ? patient.child.father : patient.child.mother
        textfieldDateOfBirth.setSavedText(guardDetails.dateOfBirth)
        textfieldSocialSecurity.setSavedText(guardDetails.socialSecurity)
        textfieldEmployer.setSavedText(guardDetails.employer)
        textfieldWorkPhone.setSavedText(guardDetails.workPhone)
        
        func load() {
            textfieldEmail.setSavedText(guardDetails.email)
        }
        
        if isFather == true {
            #if AUTO
                if let patientDetails = patient.patientDetails {
                    textfieldEmail.text = guardDetails.email == "N/A" ? patientDetails.email : guardDetails.email
                } else {
                    load()
                }
            #else
                load()
            #endif
        } else {
            load()
        }
        
    }
    
    func saveValue()  {
        let guardDetails : Guardian = isFather == true ? patient.child.father : patient.child.mother
        guardDetails.dateOfBirth = textfieldDateOfBirth.getText()
        guardDetails.socialSecurity = textfieldSocialSecurity.getText()
        guardDetails.employer = textfieldEmployer.getText()
        guardDetails.workPhone = textfieldWorkPhone.getText()
        guardDetails.email = textfieldEmail.getText()
        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldWorkPhone.isEmpty && !textfieldWorkPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID WORK PHONE NUMBER")
        }else if !textfieldEmail.isEmpty && !textfieldEmail.text!.isValidEmail{
            self.showAlert("PLEASE ENTER THE VALID EMAIL")
        }else if !textfieldSocialSecurity.isEmpty && textfieldSocialSecurity.text!.characters.count < 9{
            self.showAlert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
        }
        else{
            saveValue()
            if isFather == true{
            let child = childStoryBoard.instantiateViewController(withIdentifier: "Child4VC") as! ChildRegistration4ViewController
            child.patient = self.patient
            child.isFather = false
            self.navigationController?.pushViewController(child, animated: true)
            }else{
                
                YesOrNoAlert.sharedInstance.showWithTitle("DO YOU HAVE INSURANCE?", button1Title: "YES", button2Title: "NO", completion: { (buttonIndex) in
                    if buttonIndex == 1{
                        let child = self.childStoryBoard.instantiateViewController(withIdentifier: "Child6VC") as! ChildRegistration6ViewController
                        child.patient = self.patient
                        self.navigationController?.pushViewController(child, animated: true)
                    }else{
                        let child = self.childStoryBoard.instantiateViewController(withIdentifier: "Child7VC") as! ChildRegistration7ViewController
                        child.patient = self.patient
                        self.navigationController?.pushViewController(child, animated: true)

                    }
                })
            }
            
        }
        
    }


}
extension ChildRegistration5ViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldSocialSecurity {
            return textField.formatNumbers(range, string: string, count: 9)
        }else if textField == textfieldWorkPhone{
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
