//
//  ChildRegisration12ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildRegisration12ViewController: PDViewController {
    
    @IBOutlet weak var textviewIllness : UITextView!
    @IBOutlet weak var textviewComments : UITextView!
    @IBOutlet weak var textfieldPrintName : UITextField!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadValue()  {
        labelDate.todayDate = patient.dateToday
        textviewIllness.text = patient.child.illness == "N/A" ? "PLEASE TYPE HERE" : patient.child.illness
        textviewIllness.textColor = patient.child.illness == "N/A" ? UIColor.lightGray : UIColor.black
        textviewComments.text = patient.child.comments == "N/A" ? "PLEASE TYPE HERE" : patient.child.comments
        textviewComments.textColor = patient.child.comments == "N/A" ? UIColor.lightGray : UIColor.black
        textfieldPrintName.setSavedText(patient.child.printName)

        
    }
    
    func saveValue()  {
        patient.child.illness = textviewIllness.text == "PLEASE TYPE HERE" ? "N/A" : textviewIllness.text
        patient.child.comments = textviewComments.text == "PLEASE TYPE HERE" ? "N/A" : textviewComments.text
        patient.child.printName = textfieldPrintName.getText()
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textfieldPrintName.isEmpty {
            self.showAlert("PLEASE ENTER THE PRINT NAME")
        } else if !signaturePatient.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        } else {
            saveValue()
            patient.child.signaturePatient = signaturePatient.signatureImage()
            let child = childStoryBoard.instantiateViewController(withIdentifier: "ChildFormVC") as! ChildRegistrationFormViewController
            child.patient = self.patient
            self.navigationController?.pushViewController(child, animated: true)
        }
    }

}

extension ChildRegisration12ViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "PLEASE TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if  textView.text.isEmpty  {
            textView.text = "PLEASE TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

extension ChildRegisration12ViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
