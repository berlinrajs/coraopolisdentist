//
//  ChildRegistration3ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildRegistration3ViewController: PDViewController {
    
    @IBOutlet weak var textfieldName : UITextField!
    @IBOutlet weak var textfieldPhoneNumber : UITextField!
    @IBOutlet weak var textviewBrothers : UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loadValue()  {
        textfieldName.setSavedText(patient.child.responsibleName)
        textfieldPhoneNumber.setSavedText(patient.child.responsiblePhone)
        textviewBrothers.text = patient.child.brotherDetail == "N/A" ? "PLEASE TYPE HERE" : patient.child.brotherDetail
        textviewBrothers.textColor = patient.child.brotherDetail == "N/A" ? UIColor.lightGray : UIColor.black
        
    }
    
    func saveValue()  {
        patient.child.responsibleName = textfieldName.getText()
        patient.child.responsiblePhone = textfieldPhoneNumber.getText()
        patient.child.brotherDetail = textviewBrothers.text! == "PLEASE TYPE HERE" ? "N/A" : textviewBrothers.text!
        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textfieldName.isEmpty{
            self.showAlert("PLEASE ENTER THE RESPONSIBLE PERSON NAME")

        }else if !textfieldPhoneNumber.isEmpty && !textfieldPhoneNumber.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID PHONE NUMBER")
        }else{
            saveValue()
            let child = childStoryBoard.instantiateViewController(withIdentifier: "Child4VC") as! ChildRegistration4ViewController
            child.patient = self.patient
            self.navigationController?.pushViewController(child, animated: true)
        }
    }
}

extension ChildRegistration3ViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldPhoneNumber {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension ChildRegistration3ViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "PLEASE TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if  textView.text.isEmpty  {
            textView.text = "PLEASE TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
