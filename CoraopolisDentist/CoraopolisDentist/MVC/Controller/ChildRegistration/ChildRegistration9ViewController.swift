//
//  ChildRegistration9ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildRegistration9ViewController: PDViewController {
    
    @IBOutlet weak var radioBottle : RadioButton!
    @IBOutlet var arrayHabitsButtons : [UIButton]!
    @IBOutlet var arrayFluorideButtons : [UIButton]!
    @IBOutlet var textviewFamilyHistory : UITextView!
    @IBOutlet weak var textFieldDiet : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loadValue()  {
        radioBottle.setSelectedWithTag(patient.child.bottleTag)
        for btn in arrayHabitsButtons{
            btn.isSelected = self.patient.child.arrayHabitTags.contains(btn.tag)
        }
        for btn in arrayFluorideButtons{
            btn.isSelected = self.patient.child.arrayFluorideTag.contains(btn.tag)
        }
        textFieldDiet.setSavedText(patient.child.dietSummary)
        textviewFamilyHistory.text = patient.child.familyHistory == "N/A" ? "PLEASE TYPE HERE" : patient.child.familyHistory
        textviewFamilyHistory.textColor = patient.child.familyHistory == "N/A" ? UIColor.lightGray : UIColor.black
        
    }
    
    func saveValue()  {
        patient.child.bottleTag = radioBottle.selected == nil ? 0 : radioBottle.selected.tag
        patient.child.familyHistory = textviewFamilyHistory.text == "PLEASE TYPE HERE" ? "N/A" : textviewFamilyHistory.text!
        patient.child.dietSummary = textFieldDiet.getText()
        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if radioBottle.selected == nil {
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else{
            saveValue()
            let child = childStoryBoard.instantiateViewController(withIdentifier: "Child10VC") as! ChildRegistration10ViewController
            child.patient = self.patient
            self.navigationController?.pushViewController(child, animated: true)
        }
    }
    

    @IBAction func onBottleButtonPressed (withSender sender : RadioButton){
        if sender.tag == 1{
            BottleAlert.sharedInstance.showPopUp(self.view, completion: { (contents, age) in
                    self.patient.child.bottleStoppedAge = age
                    self.patient.child.bottleContents = contents
                }, showAlert: { (alertMessage) in
                    
            })
        }else{
            self.patient.child.bottleStoppedAge = "N/A"
            self.patient.child.bottleContents = "N/A"
        }
//        if sender.tag == 1{
//            PopupTextField.sharedInstance.showWithPlaceHolder("BOTTLE CONTENTS?", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, completion: { (textField, isEdited) in
//                if isEdited{
//                    self.patient.child.bottleContents = textField.text!
//                    self.patient.child.bottleStoppedAge = "N/A"
//                }else{
//                    self.patient.child.bottleContents = "N/A"
//                    sender.deselectAllButtons()
//                }
//            })
//        }else{
//            PopupTextField.sharedInstance.showWithPlaceHolder("AGE THIS STOPPED", keyboardType: UIKeyboardType.NumbersAndPunctuation, textFormat: TextFormat.Number, completion: { (textField, isEdited) in
//                if isEdited{
//                    self.patient.child.bottleStoppedAge = textField.text!
//                    self.patient.child.bottleContents = "N/A"
//                }else{
//                    self.patient.child.bottleStoppedAge = "N/A"
//                    sender.deselectAllButtons()
//                }
//            })
//
//        }
    }

    @IBAction func onHabitButtonsPressed (withSender sender : UIButton){
        sender.isSelected = !sender.isSelected
        if patient.child.arrayHabitTags.contains(sender.tag){
            patient.child.arrayHabitTags.remove(at: patient.child.arrayHabitTags.index(of: sender.tag)!)
        }else{
            patient.child.arrayHabitTags.append(sender.tag)
        }
        
        if sender.isSelected == true && sender.tag == 4{
            PopupTextField.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", keyboardType: UIKeyboardType.default, textFormat: TextFormat.default, completion: { (textField, isEdited) in
                if isEdited{
                    self.patient.child.habitOther = textField.text!
                }else{
                    self.patient.child.habitOther = "N/A"
                sender.isSelected = false
                self.patient.child.arrayHabitTags.remove(at: self.patient.child.arrayHabitTags.index(of: sender.tag)!)
                }
            })

        }else if sender.isSelected == false && sender.tag == 4{
            self.patient.child.habitOther = "N/A"

        }
    }

    
    @IBAction func onFluorideButtonPressed (withSender sender : UIButton){
        sender.isSelected = !sender.isSelected
        if patient.child.arrayFluorideTag.contains(sender.tag){
        patient.child.arrayFluorideTag.remove(at: patient.child.arrayFluorideTag.index(of: sender.tag)!)
        }else{
            patient.child.arrayFluorideTag.append(sender.tag)
        }
        
        if sender.tag == 4{
            patient.child.arrayFluorideTag.removeAll()
            patient.child.arrayFluorideTag.append(4)
            for btn in arrayFluorideButtons{
                btn.isSelected = self.patient.child.arrayFluorideTag.contains(btn.tag)
            }
        }else{
            if patient.child.arrayFluorideTag.contains(4){
                patient.child.arrayFluorideTag.remove(at: patient.child.arrayFluorideTag.index(of: 4)!)
                for btn in arrayFluorideButtons{
                    btn.isSelected = self.patient.child.arrayFluorideTag.contains(btn.tag)
                }

            }

        }
 
    }
}

extension ChildRegistration9ViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension ChildRegistration9ViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "PLEASE TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if  textView.text.isEmpty  {
            textView.text = "PLEASE TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
