//
//  ChildRegistration1ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildRegistration1ViewController: PDViewController {

    @IBOutlet weak var textfieldNickname : UITextField!
    @IBOutlet weak var textfieldAddress : UITextField!
    @IBOutlet weak var textfieldCity : UITextField!
    @IBOutlet weak var textfieldState : UITextField!
    @IBOutlet weak var textfieldZipcode : UITextField!
    @IBOutlet weak var textfieldHobbies : UITextField!
    @IBOutlet weak var textfieldTv : UITextField!
    @IBOutlet weak var dropdownGender : BRDropDown!

    override func viewDidLoad() {
        super.viewDidLoad()

        StateListView.addStateListForTextField(textfieldState)
        dropdownGender.items = ["MALE","FEMALE"]
        dropdownGender.placeholder = "-- SELECT GENDER * --"
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadValue()  {
        
        textfieldNickname.setSavedText(patient.child.nickName)
        textfieldHobbies.setSavedText(patient.child.hobbies)
        textfieldTv.setSavedText(patient.child.tvShows)
        
        func load() {
            textfieldAddress.setSavedText(patient.child.address)
            textfieldCity.setSavedText(patient.child.city)
            textfieldState.setSavedText(patient.child.state)
            textfieldZipcode.setSavedText(patient.child.zipcode)
            dropdownGender.selectedIndex = patient.child.radioGenderTag
        }
        
        #if AUTO
            if let patientDetails = patient.patientDetails {
                textfieldAddress.text = patient.child.address == "N/A" ? patientDetails.address : patient.child.address
                textfieldCity.text = patient.child.city == "N/A" ? patientDetails.city : patient.child.city
                textfieldState.text = patient.child.state == "N/A" ? patientDetails.state : patient.child.state
                textfieldZipcode.text = patient.child.zipcode == "N/A" ? patientDetails.zipCode : patient.child.zipcode
                if let gender = patientDetails.gender, patient.child.radioGenderTag == 0 {
                    dropdownGender.selectedIndex = gender.index
                } else {
                    dropdownGender.selectedIndex = patient.child.radioGenderTag
                }
            } else {
                load()
            }
        #else
                load()
        #endif
        
        
    }
    
    func saveValue()  {
        patient.child.nickName = textfieldNickname.getText()
        patient.child.address = textfieldAddress.getText()
        patient.child.city = textfieldCity.getText()
        patient.child.state = textfieldState.getText()
        patient.child.zipcode = textfieldZipcode.getText()
        patient.child.hobbies = textfieldHobbies.getText()
        patient.child.tvShows = textfieldTv.getText()
        patient.child.radioGenderTag = dropdownGender.selectedIndex
        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textfieldAddress.isEmpty || textfieldCity.isEmpty || textfieldState.isEmpty || textfieldZipcode.isEmpty || dropdownGender.selectedOption == nil{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if !textfieldZipcode.text!.isZipCode{
            self.showAlert("PLEASE ENTER THE VALID ZIPCODE")
        }else{
            saveValue()
            let child = childStoryBoard.instantiateViewController(withIdentifier: "Child2VC") as! ChildRegistration2ViewController
            child.patient = self.patient
            self.navigationController?.pushViewController(child, animated: true)
            
        }
        
    }

}
extension ChildRegistration1ViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldZipcode {
            return textField.formatZipCode(range, string: string)
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

