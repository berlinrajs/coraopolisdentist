//
//  ChildRegistration6ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildRegistration6ViewController: PDViewController {
    
    @IBOutlet weak var textfieldPrimaryInsurance : UITextField!
    @IBOutlet weak var textfieldSecondaryInsurance : UITextField!
    @IBOutlet weak var textfieldPrimaryName : UITextField!
    @IBOutlet weak var textfieldSecondaryName : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        textfieldPrimaryInsurance.setSavedText(patient.child.primaryInsurance)
        textfieldSecondaryInsurance.setSavedText(patient.child.secondaryInsurance)
        textfieldPrimaryName.setSavedText(patient.child.primaryName)
        textfieldSecondaryName.setSavedText(patient.child.secondaryName)
        
    }
    
    func saveValue()  {
        patient.child.primaryInsurance = textfieldPrimaryInsurance.getText()
        patient.child.secondaryInsurance = textfieldSecondaryInsurance.getText()
        patient.child.primaryName = textfieldPrimaryName.getText()
        patient.child.secondaryName = textfieldSecondaryName.getText()
        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
            saveValue()
            let child = childStoryBoard.instantiateViewController(withIdentifier: "Child7VC") as! ChildRegistration7ViewController
            child.patient = self.patient
            self.navigationController?.pushViewController(child, animated: true)
    }
    


}

extension ChildRegistration6ViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

