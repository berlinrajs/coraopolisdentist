//
//  ChildRegistrationFormViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/27/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildRegistrationFormViewController: PDViewController {
    
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelFirstName : UILabel!
    @IBOutlet weak var labelInitial : UILabel!
    @IBOutlet weak var labelLastName : UILabel!
    @IBOutlet weak var labelNickName : UILabel!
    @IBOutlet weak var labelGender : UILabel!
    @IBOutlet weak var labelDateOfBirth : UILabel!
    @IBOutlet weak var labelSchool : UILabel!
    @IBOutlet weak var labelGrade : UILabel!
    @IBOutlet weak var labelPhysicianAddress : UILabel!
    @IBOutlet weak var labelAddress : UILabel!
    @IBOutlet weak var labelCity : UILabel!
    @IBOutlet weak var labelState : UILabel!
    @IBOutlet weak var labelZipcode : UILabel!
    @IBOutlet weak var labelResponsibleName : UILabel!
    @IBOutlet weak var labelResponsiblePhone : UILabel!
    @IBOutlet weak var labelBrotherDetails : UILabel!
    @IBOutlet weak var radioFirstVisit : RadioButton!
    @IBOutlet weak var labelDentalVisit : UILabel!
    @IBOutlet weak var labelHobbies : UILabel!
    @IBOutlet weak var labelTV : UILabel!
    
    @IBOutlet weak var labelFatherFirstName : UILabel!
    @IBOutlet weak var labelFatherLastName : UILabel!
    @IBOutlet weak var labelFatherStreet : UILabel!
    @IBOutlet weak var labelFatherCity : UILabel!
    @IBOutlet weak var labelFatherState : UILabel!
    @IBOutlet weak var labelFatherZipcode : UILabel!
    @IBOutlet weak var labelFatherPhone : UILabel!
    @IBOutlet weak var labelFatherDateOfBirth : UILabel!
    @IBOutlet weak var labelFatherSSN : UILabel!
    @IBOutlet weak var labelFatherEmployer : UILabel!
    @IBOutlet weak var labelFatherWorkPhone : UILabel!
    @IBOutlet weak var labelFatherEmail : UILabel!
    
    @IBOutlet weak var labelMotherFirstName : UILabel!
    @IBOutlet weak var labelMotherLastName : UILabel!
    @IBOutlet weak var labelMotherStreet : UILabel!
    @IBOutlet weak var labelMotherCity : UILabel!
    @IBOutlet weak var labelMotherState : UILabel!
    @IBOutlet weak var labelMotherZipcode : UILabel!
    @IBOutlet weak var labelMotherPhone : UILabel!
    @IBOutlet weak var labelMotherDateOfBirth : UILabel!
    @IBOutlet weak var labelMotherSSN : UILabel!
    @IBOutlet weak var labelMotherEmployer : UILabel!
    @IBOutlet weak var labelMotherWorkPhone : UILabel!
    @IBOutlet weak var labelMotherEmail : UILabel!
    
    @IBOutlet weak var labelPrimaryInsurance : UILabel!
    @IBOutlet weak var labelPrimaryName : UILabel!
    @IBOutlet weak var labelSecondaryInsurance : UILabel!
    @IBOutlet weak var labelSecondaryName : UILabel!

    
    @IBOutlet weak var labelFirstTeeth : UILabel!
    @IBOutlet weak var labelDentalProblems : UILabel!
    @IBOutlet weak var labelBirthPlace : UILabel!
    @IBOutlet weak var radioFluoridated : RadioButton!
    @IBOutlet weak var radioNow : RadioButton!
    @IBOutlet weak var radioBottle : RadioButton!
    @IBOutlet weak var labelBottleContents : UILabel!
    @IBOutlet weak var labelBottleStop : UILabel!
    @IBOutlet weak var radioBrushTeeth : RadioButton!
    @IBOutlet weak var labelBrushTeeth : UILabel!
    @IBOutlet weak var labelBrushType : UILabel!
    @IBOutlet weak var labelBrushingTime : UILabel!
    @IBOutlet      var arrayButtonHabits : [RadioButton]!
    @IBOutlet weak var labelDentalInjuries : UILabel!
    @IBOutlet weak var radioSnore : RadioButton!
    @IBOutlet      var arrayButtonFluoride : [UIButton]!
    @IBOutlet weak var labelDiet : UILabel!
    @IBOutlet weak var labelFamilyHistory : UILabel!
    @IBOutlet weak var labelPregnancy : UILabel!
    @IBOutlet weak var radioHealth : RadioButton!
    @IBOutlet weak var labelMedication : UILabel!
    @IBOutlet      var arrayButtonMedicalHistory1 : [UIButton]!
    @IBOutlet      var arrayButtonMedicalHistory2 : [UIButton]!
    @IBOutlet weak var radioMedicationButton : RadioButton!
    @IBOutlet weak var labelHabitOther : UILabel!


    @IBOutlet      var arrayButtonAllergies : [RadioButton]!
    @IBOutlet weak var labelAllergyOther : UILabel!
    @IBOutlet      var labelIllness : [UILabel]!
    @IBOutlet      var labelComments : [UILabel]!
    @IBOutlet weak var labelPrintName : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        loadvalues()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadvalues (){
        labelDate.text = patient.dateToday
        labelFirstName.text = patient.firstName
        labelInitial.text = patient.middleInitial
        labelLastName.text = patient.lastName
        labelNickName.text = patient.child.nickName
        labelGender.text = patient.child.radioGenderTag == 1 ? "MALE" : "FEMALE"
        labelDateOfBirth .text = patient.dateOfBirth
        labelSchool.text = patient.child.schoolName
        labelGrade.text = patient.child.grade
        labelPhysicianAddress.text = getPhysicianAddress()
        labelAddress.text = patient.child.address
        labelCity.text = patient.child.city
        labelState.text = patient.child.state
        labelZipcode.text = patient.child.zipcode
        labelResponsibleName.text = patient.child.responsibleName
        labelResponsiblePhone.text = patient.child.responsiblePhone
        labelBrotherDetails.text = patient.child.brotherDetail
        radioFirstVisit.setSelectedWithTag(patient.child.radioFirstVisitTag)
        labelDentalVisit.text = patient.child.lastDentalVisit
        labelHobbies.text = patient.child.hobbies
        labelTV.text = patient.child.tvShows
        labelFatherFirstName.text = patient.child.father.firstName
        labelFatherLastName.text = patient.child.father.lastName
        labelFatherStreet.text = patient.child.father.address
        labelFatherCity.text = patient.child.father.city
        labelFatherState.text = patient.child.father.state
        labelFatherZipcode.text = patient.child.father.zipcode
        labelFatherPhone.text = patient.child.father.phoneNumber
        labelFatherDateOfBirth.text = patient.child.father.dateOfBirth
        labelFatherSSN.text = patient.child.father.socialSecurity.socialSecurityNumber
        labelFatherEmployer.text = patient.child.father.employer
        labelFatherWorkPhone.text = patient.child.father.workPhone
        labelFatherEmail.text = patient.child.father.email
        labelMotherFirstName.text = patient.child.mother.firstName
        labelMotherLastName.text = patient.child.mother.lastName
        labelMotherStreet.text = patient.child.mother.address
        labelMotherCity.text = patient.child.mother.city
        labelMotherState.text = patient.child.mother.state
        labelMotherZipcode.text = patient.child.mother.zipcode
        labelMotherPhone.text = patient.child.mother.phoneNumber
        labelMotherDateOfBirth.text = patient.child.mother.dateOfBirth
        labelMotherSSN.text = patient.child.mother.socialSecurity.socialSecurityNumber
        labelMotherEmployer.text = patient.child.mother.employer
        labelMotherWorkPhone.text = patient.child.mother.workPhone
        labelMotherEmail.text = patient.child.mother.email
        labelPrimaryInsurance.text = patient.child.primaryInsurance
        labelPrimaryName.text = patient.child.primaryName
        labelSecondaryInsurance.text = patient.child.secondaryInsurance
        labelSecondaryName.text = patient.child.secondaryName
        
        
        labelFirstTeeth.text = patient.child.ageFirstTeeth
        labelDentalProblems.text = patient.child.dentalProblems
        labelBirthPlace.text = patient.child.birthPlace
        radioFluoridated.setSelected(patient.child.dentalQuestions[0].selectedOption!)
        radioNow.setSelectedWithTag(patient.child.radioNowTag)
        radioBottle.setSelectedWithTag(patient.child.bottleTag)
        labelBottleContents.text = patient.child.bottleContents
        labelBottleStop.text = patient.child.bottleStoppedAge
        radioBrushTeeth.setSelectedWithTag(patient.child.radioBrushTag)
        labelBrushTeeth.text = patient.child.brushDetails
        labelBrushType.text = patient.child.brushType
        labelBrushingTime.text = patient.child.brushingTime
        for btn in arrayButtonHabits{
            btn.isSelected = patient.child.arrayHabitTags.contains(btn.tag)
        }
        labelHabitOther.text = patient.child.habitOther
        labelDentalInjuries.text = patient.child.dentalQuestions[1].answer
        radioSnore.setSelected(patient.child.dentalQuestions[2].selectedOption!)
        for btn in arrayButtonFluoride{
            btn.isSelected = patient.child.arrayFluorideTag.contains(btn.tag)
        }

        labelDiet.text = patient.child.dietSummary
        labelFamilyHistory.text = patient.child.familyHistory
        labelPregnancy.text = patient.child.dentalQuestions[3].answer
        radioHealth.setSelected(patient.child.dentalQuestions[4].selectedOption!)
        labelMedication.text = patient.child.dentalQuestions[5].answer
        for btn in arrayButtonMedicalHistory1{
            btn.isSelected = patient.child.medicalQuestions[0][btn.tag].selectedOption!
        }
        for btn in arrayButtonMedicalHistory2{
            btn.isSelected = patient.child.medicalQuestions[1][btn.tag].selectedOption!
        }
        radioMedicationButton.setSelected(patient.child.dentalQuestions[5].selectedOption!)
        
        
        for btn in arrayButtonAllergies{
            btn.isSelected = patient.child.allergies[btn.tag].selectedOption!

        }
        
        labelAllergyOther.text = patient.child.allergies[11].answer
        patient.child.illness.setTextForArrayOfLabels(labelIllness)
        patient.child.comments.setTextForArrayOfLabels(labelComments)
        labelPrintName.text = patient.child.printName
        signaturePatient.image = patient.child.signaturePatient
        labelDate1.text = patient.dateToday
    }
    
    
    func getPhysicianAddress () -> String {
        var array : [String] = [String]()
        if patient.child.physicianName != "N/A"{
            array.append(" \(patient.child.physicianName)")
        }
        if patient.child.physicianAddress != "N/A"{
            array.append(" \(patient.child.physicianAddress)")
        }
        return array.count == 0 ? "N/A" : (array as NSArray).componentsJoined(by: ",")
        
    }
    
    
    override func buttonActionSubmit(withSender sender: UIButton) {
        #if AUTO
            if !Reachability.isConnectedToNetwork() {
                let alertController = UIAlertController(title: "Coraopolis Dentist", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
                let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url as URL)
                    }
                }
                let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    
                }
                alertController.addAction(alertOkAction)
                alertController.addAction(alertCancelAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            BRProgressHUD.show()
            ServiceManager.sendChildDetails(patient: patient, completion: { (success, error) in
                BRProgressHUD.hide()
                if success {
                    super.buttonActionSubmit(withSender: sender)
                } else {
                    let alert = Extention.alert(error!.localizedDescription.uppercased())
                    self.present(alert, animated: true, completion: nil)
                }
            })
        #else
            super.buttonActionSubmit(withSender: sender)
        #endif
    }

}
