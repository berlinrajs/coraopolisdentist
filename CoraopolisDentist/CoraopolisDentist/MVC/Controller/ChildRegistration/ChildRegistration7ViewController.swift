//
//  ChildRegistration7ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildRegistration7ViewController: PDViewController {
    
    @IBOutlet weak var textfieldAge : UITextField!
    @IBOutlet weak var textfieldDentalProblems : UITextField!
    @IBOutlet weak var textfieldBirthPlace : UITextField!
    @IBOutlet weak var radioBrush : RadioButton!
    @IBOutlet weak var dropdownBrushType : BRDropDown!
    @IBOutlet weak var textfieldBrushingTime : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        dropdownBrushType.items = ["SOFT","MEDIUM","HARD"]
        dropdownBrushType.placeholder = "-- SELECT --"
        textfieldAge.setSavedText(patient.child.ageFirstTeeth)
        textfieldDentalProblems.setSavedText(patient.child.dentalProblems)
        textfieldBirthPlace.setSavedText(patient.child.birthPlace)
        textfieldBrushingTime.setSavedText(patient.child.brushingTime)
        radioBrush.setSelectedWithTag(patient.child.radioBrushTag)
        dropdownBrushType.selectedIndex = patient.child.brushTypeTag
        
    }
    
    func saveValue()  {
        patient.child.ageFirstTeeth = textfieldAge.getText()
        patient.child.dentalProblems = textfieldDentalProblems.getText()
        patient.child.birthPlace = textfieldBirthPlace.getText()
        patient.child.brushingTime = textfieldBrushingTime.getText()
        patient.child.radioBrushTag = radioBrush.selected == nil ? 0 : radioBrush.selected.tag
        patient.child.brushTypeTag = dropdownBrushType.selectedIndex
        patient.child.brushType = dropdownBrushType.selectedOption == nil ? "N/A" : dropdownBrushType.selectedOption!
        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if radioBrush.selected == nil{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else{
        saveValue()
        let child = childStoryBoard.instantiateViewController(withIdentifier: "Child8VC") as! ChildRegistration8ViewController
        child.patient = self.patient
        self.navigationController?.pushViewController(child, animated: true)
        }
    }
    
    @IBAction func onBrushButtonPressed (withSender sender : RadioButton){
        if sender.tag == 1{
            self.patient.child.brushDetails = "N/A"
        }else{
            PopupTextField.sharedInstance.showWithPlaceHolder("WHO DOES?", keyboardType: UIKeyboardType.default, textFormat: TextFormat.default, completion: { (textField, isEdited) in
                if isEdited{
                    self.patient.child.brushDetails = textField.text!
                    
                }else{
                    self.patient.child.brushDetails = "N/A"
                    sender.isSelected = false

                }
            })
        }
    }


}

extension ChildRegistration7ViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldAge {
            return textField.formatNumbers(range, string: string, count: 2)
        }
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

