//
//  ChildRegistration8ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildRegistration8ViewController: PDViewController {

    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var buttonVerified: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if buttonVerified.isSelected == false {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.present(alert, animated: true, completion: nil)
        } else {
            let child = childStoryBoard.instantiateViewController(withIdentifier: "Child9VC") as! ChildRegistration9ViewController
            child.patient = self.patient
            self.navigationController?.pushViewController(child, animated: true)
            
        }
    }

}
extension ChildRegistration8ViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.child.dentalQuestions.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PatientInfoCell
        cell.delegate = self
        cell.tag = indexPath.row
        cell.configCell(self.patient.child.dentalQuestions[indexPath.row])
        
        return cell
    }
}

extension ChildRegistration8ViewController : PatientInfoCellDelegate{
    
    func radioButtonTappedForCell(_ cell: PatientInfoCell){
        if cell.tag == 0{
            YesOrNoAlert.sharedInstance.showWithTitle("IS IT NOW?", button1Title: "YES", button2Title: "NO", completion: { (buttonIndex) in
                self.patient.child.radioNowTag = buttonIndex
            })
        }else{
        let placeholder = cell.tag == 1 ? "PLEASE EXPLAIN" : "PLEASE LIST"
        PopupTextField.sharedInstance.showWithPlaceHolder(placeholder, keyboardType: UIKeyboardType.default, textFormat: TextFormat.default) { (textField, isEdited) in
            if isEdited{
                self.patient.child.dentalQuestions[cell.tag].answer = textField.text
            }else{
                cell.radioButtonYes.isSelected = false
                self.patient.child.dentalQuestions[cell.tag].selectedOption = false
            }
            }
        }
    }
}

