//
//  ChildRegistration2ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildRegistration2ViewController: PDViewController {
    
    @IBOutlet weak var textfieldSchool : UITextField!
    @IBOutlet weak var textfieldGrade : UITextField!
    @IBOutlet weak var textfieldPhysicianName : UITextField!
    @IBOutlet weak var textfieldPhysicianAddress : UITextField!
    @IBOutlet weak var radioFirstVisit : RadioButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loadValue()  {
        textfieldSchool.setSavedText(patient.child.schoolName)
        textfieldGrade.setSavedText(patient.child.grade)
        textfieldPhysicianName.setSavedText(patient.child.physicianName)
        textfieldPhysicianAddress.setSavedText(patient.child.physicianAddress)
        radioFirstVisit.setSelectedWithTag(patient.child.radioFirstVisitTag)
        
    }
    
    func saveValue()  {
        patient.child.schoolName = textfieldSchool.getText()
        patient.child.grade = textfieldGrade.getText()
        patient.child.physicianName = textfieldPhysicianName.getText()
        patient.child.physicianAddress = textfieldPhysicianAddress.getText()
        patient.child.radioFirstVisitTag = radioFirstVisit.selected == nil ? 0 : radioFirstVisit.selected.tag
        
        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if radioFirstVisit.selected == nil{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else{
            saveValue()
            let child = childStoryBoard.instantiateViewController(withIdentifier: "Child3VC") as! ChildRegistration3ViewController
            child.patient = self.patient
            self.navigationController?.pushViewController(child, animated: true)
            
        }
        
    }

    @IBAction func onFirstVisitButtonPressed (withSender sender : RadioButton){
        if sender.tag == 2{
            PopupTextField.sharedInstance.showWithPlaceHolder("DATE OF LAST VISIT", keyboardType: UIKeyboardType.default, textFormat: TextFormat.dateInCurrentYear, completion: { (textField, isEdited) in
                if isEdited{
                    self.patient.child.lastDentalVisit = textField.text!
                    
                }else{
                    self.patient.child.lastDentalVisit = "N/A"
                    sender.isSelected = false
                }
            })
        }else{
            self.patient.child.lastDentalVisit = "N/A"

        }
    }

}

extension ChildRegistration2ViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
