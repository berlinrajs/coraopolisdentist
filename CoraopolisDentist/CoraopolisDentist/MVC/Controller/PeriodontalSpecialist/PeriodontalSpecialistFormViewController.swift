//
//  PeriodontalSpecialistFormViewController.swift
//  CoraopolisDentist
//
//  Created by Office on 12/17/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PeriodontalSpecialistFormViewController: PDViewController {

    var patientSign : UIImage!
    var providerSign : UIImage!
    
    @IBOutlet var imageviewPatientSign : UIImageView!
    @IBOutlet var imageviewProviderSign : UIImageView!
    @IBOutlet var labelDate : UILabel!
    @IBOutlet var labelPatientName : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageviewPatientSign.image = patientSign
        imageviewProviderSign.image = providerSign
        labelDate.text = patient.dateToday
        labelPatientName.text = patient.fullName
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
