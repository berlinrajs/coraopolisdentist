//
//  Wax1ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/19/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class Wax1ViewController: PDViewController {

    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate : DateLabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let wax = consentStoryboard.instantiateViewController(withIdentifier: "WaxFormVC") as! WaxFormViewController
            wax.patient = self.patient
            wax.patientSign = signaturePatient.signatureImage()
            self.navigationController?.pushViewController(wax, animated: true)

        }
        
    }
    


}
