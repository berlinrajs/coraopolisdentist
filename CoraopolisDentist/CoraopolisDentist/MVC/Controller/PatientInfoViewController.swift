//
//  PatientInfoViewController.swift
//   Coraopolisdentist
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientInfoViewController: PDViewController {
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var textFieldFirstName: PDTextField!
    @IBOutlet weak var textFieldLastName: PDTextField!
    @IBOutlet weak var textFieldMiddleInitial : UITextField!
    @IBOutlet weak var textfieldMonth : UITextField!
    @IBOutlet weak var textfieldDate : UITextField!
    @IBOutlet weak var textfieldYear : UITextField!
    
//    @IBOutlet weak var constraintTableViewHeight: NSLayoutConstraint!
//    @IBOutlet weak var tableViewDoctorList: PDTableView!
//    @IBOutlet weak var labelDoctorName: UILabel!
    
    var isNewPatient: Bool!
    var manager: AFHTTPSessionManager?
    @IBOutlet weak var buttonNext: PDButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        MonthInputView.addMonthPickerForTextField(textfieldMonth)

        labelDate.text = patient.dateToday
//        tableViewDoctorList.layer.cornerRadius = 4.0
//        tableViewDoctorList.delegatePDTableView = self
//        self.tableViewDoctorList.arrayValues = ["Dr. Judy L. DiSanti", "Dr. Jodon"]        
//        self.tableViewDoctorList.reloadData()

        // Do any additional setup after loading the view.
    }
    
//    @IBAction func buttonActionSelectDoctor(sender: AnyObject) {
//        
//        if constraintTableViewHeight.constant == 0 {
//            self.view.layoutIfNeeded()
//            constraintTableViewHeight.constant = 3 * (70.0 * screenSize.height/1024.0)
//            UIView.animateWithDuration(0.25, animations: { () -> Void in
//                self.view.layoutIfNeeded()
//            })
//        } else {
//            self.view.layoutIfNeeded()
//            constraintTableViewHeight.constant = 0
//            UIView.animateWithDuration(0.25, animations: { () -> Void in
//                self.view.layoutIfNeeded()
//            })
//        }
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        if textFieldFirstName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER PATIENT FIRST NAME")
            self.present(alert, animated: true, completion: nil)
        } else if textFieldLastName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER PATIENT LAST NAME")
            self.present(alert, animated: true, completion: nil)
        } else if invalidDateofBirth {
            let alert = Extention.alert("PLEASE ENTER THE VALID DATE OF BIRTH")
            self.present(alert, animated: true, completion: nil)
        } else {
            
            patient.doctorName = "" //labelDoctorName.text == "SELECT A DOCTOR" ? "" : labelDoctorName.text
            patient.firstName = textFieldFirstName.text
            patient.lastName = textFieldLastName.text
            patient.middleInitial = textFieldMiddleInitial.isEmpty ? "" : textFieldMiddleInitial.text!
            patient.dateOfBirth = textfieldMonth.text! + " " + textfieldDate.text! + ", " + textfieldYear.text!
            #if AUTO
                func showMoreThanOneUserAlert()  {
                    let alertController = UIAlertController(title: "Coraopolis Dentist", message: "More than one user found. Please handover the device to front desk to enter your patient id", preferredStyle: UIAlertControllerStyle.alert)
                    let alertYesAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
                        let newPatientStep1VC = self.storyboard?.instantiateViewController(withIdentifier: "VerificationVC") as! VerificationViewController
                        newPatientStep1VC.patient = self.patient
                        self.navigationController?.pushViewController(newPatientStep1VC, animated: true)
                    }
                    alertController.addAction(alertYesAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                }
                
                func showUnableToFindAlert (){
                    let alertController = UIAlertController(title: "Coraopolis Dentist", message: "Unable to find the patient \(textFieldFirstName.text!) \(textFieldLastName.text!) - \(patient.dateOfBirth) mismatch", preferredStyle: UIAlertControllerStyle.alert)
                    let alertYesAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
                        showCreateNewPatientAlert()
                    }
                    alertController.addAction(alertYesAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                
                func showCreateNewPatientAlert (){
                    let alertController = UIAlertController(title: "Coraopolis Dentist", message: "Do you wish to create a new patient", preferredStyle: UIAlertControllerStyle.alert)
                    let alertYesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) -> Void in
                        self.patient.patientDetails = nil
                        gotoPatientSignInForm()
                        
                    }
                    alertController.addAction(alertYesAction)
                    let alertNoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (action) -> Void in
                        self.navigationController?.popToRootViewController(animated: true)
                        
                    }
                    alertController.addAction(alertNoAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                }
                
                func gotoPatientSignInForm() {
                    let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
                    if formNames.contains(kChildSignIn) || formNames.contains(kNewChildSignIn) {
                        let child = childStoryBoard.instantiateViewController(withIdentifier: "Child1VC") as! ChildRegistration1ViewController
                        child.patient = self.patient
                        self.navigationController?.pushViewController(child, animated: true)
                    } else {
                        let adult = adultStoryboard.instantiateViewController(withIdentifier: "Adult1VC") as! AdultRegistration1ViewController
                        adult.patient = self.patient
                        self.navigationController?.pushViewController(adult, animated: true)
                    }
                }
                
                func APICall() {
                    BRProgressHUD.show()
                    self.buttonNext.isUserInteractionEnabled = false
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MMM dd, yyyy"
                    let date = dateFormatter.date(from: patient.dateOfBirth)
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    manager = AFHTTPSessionManager(baseURL: NSURL(string: hostUrl) as? URL)
                    manager?.responseSerializer.acceptableContentTypes = ["text/html"]
                    manager?.post("consent_fetch_patient_info.php", parameters: ["first_name" : self.textFieldFirstName.text!, "last_name": self.textFieldLastName.text!, "dob": dateFormatter.string(from: date!)], progress: { (progress) in
                        }, success: { (task, result) in
                            self.buttonNext.isUserInteractionEnabled = true
                            BRProgressHUD.hide()
                            if self.navigationController?.topViewController == self {
                                let response = result as! [String : AnyObject]
                                if response["status"] as! String == "success"  {
                                    let patientDetails = response["patientData"] as! [String: AnyObject]
                                    self.patient.patientDetails = PatientDetails(details: patientDetails)
                                    self.gotoNextForm(true)
                                } else {
                                    if response["status"] as! String == "failed" && (response["message"] as! String).contains("Not connected") {
                                        let alert = Extention.alert((response["message"] as! String).uppercased())
                                        self.present(alert, animated: true, completion: nil)
                                    } else if response["status"] as! String == "matching_name"  {
                                        let patientDetails = response["matchingData"] as! [String: String]
                                        let alertController = UIAlertController(title: "Coraopolis Dentist", message: "Did you mean \(patientDetails["Fname"]!) \(patientDetails["Lname"]!)? If so select YES and choose the correct date of birth", preferredStyle: UIAlertControllerStyle.alert)
                                        let alertOkAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) -> Void in
                                            self.textFieldFirstName.text = patientDetails["Fname"]
                                            self.textFieldLastName.text = patientDetails["Lname"]
                                            self.textfieldDate.text = ""
                                            self.textfieldMonth.text = ""
                                            self.textfieldYear.text = ""
                                        }
                                        alertController.addAction(alertOkAction)
                                        let alertNoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (action) -> Void in
                                            showUnableToFindAlert()
                                        }
                                        alertController.addAction(alertNoAction)
                                        self.present(alertController, animated: true, completion: nil)
                                    } else if response["status"] as! String == "multiple_patient_found" {
                                        showMoreThanOneUserAlert()
                                    } else {
                                        self.patient.patientDetails = nil
                                        let alertController = UIAlertController(title: "Coraopolis Dentist", message: "Patient not found. Are you sure the provided details are correct?", preferredStyle: UIAlertControllerStyle.alert)
                                        let alertYesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) -> Void in
                                            showUnableToFindAlert()
                                        }
                                        alertController.addAction(alertYesAction)
                                        let alertNoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (action) -> Void in
                                            
                                        }
                                        alertController.addAction(alertNoAction)
                                        self.present(alertController, animated: true, completion: nil)
                                    }
                                }
                            }
                        }, failure: { (task, error) in
                            self.buttonNext.isUserInteractionEnabled = true
                            BRProgressHUD.hide()
                            self.patient.patientDetails = nil
                            if self.navigationController?.topViewController == self {
                                let alert = Extention.alert(error.localizedDescription)
                                self.present(alert, animated: true, completion: nil)
                            }
                    })
                }
                
                if self.isNewPatient == true {
                    self.gotoNextForm(true)
                } else {
                    APICall()
                }
            #else
                self.gotoNextForm(true)
            #endif
        }
    }
    
    @IBAction func buttonActionBack(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    var invalidDateofBirth: Bool {
        get {
            if textfieldMonth.isEmpty || textfieldDate.isEmpty || textfieldYear.isEmpty {
                return true
            } else if Int(textfieldDate.text!)! == 0{
                return true
            } else if !textfieldYear.text!.isValidYear {
                return true
            } else {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MMM-yyyy"
                
                let todayDate = dateFormatter.date(from: dateFormatter.string(from: Date()))
                let currentDate = dateFormatter.date(from: "\(textfieldDate.text!)-\(textfieldMonth.text!)-\(textfieldYear.text!)")
                
                if todayDate == nil || currentDate == nil {
                    return true
                }
                if todayDate!.timeIntervalSince(currentDate!) < 0 {
                    return true
                }
                return false
            }
        }
    }
    


}
extension PatientInfoViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldDate{
            return textField.formatDate(range, string: string)
        } else if textField == textfieldYear{
            return textField.formatNumbers(range, string: string, count: 4)
        }else if textField == textFieldMiddleInitial{
            return textField.formatMiddleName(range, string: string)
        }else if textField == textFieldLastName || textField == textFieldFirstName{
            return textField.formatFirstLastName(range, string: string)
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


//extension PatientInfoViewController : PDTableViewDelegate {
//    func selectedValue(value: String, id: Int) {
//        labelDoctorName.text = value.uppercaseString
//        self.view.layoutIfNeeded()
//        constraintTableViewHeight.constant = 0
//        UIView.animateWithDuration(0.25, animations: { () -> Void in
//            self.view.layoutIfNeeded()
//        })
//    }
//}
