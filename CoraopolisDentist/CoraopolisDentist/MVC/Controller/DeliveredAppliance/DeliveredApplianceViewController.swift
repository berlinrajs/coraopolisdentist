//
//  DeliveredApplianceViewController.swift
//  CoraopolisDentist
//
//  Created by Office on 12/17/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class DeliveredApplianceViewController: PDViewController {

    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var textFieldRelationship: PDTextField!
    @IBOutlet weak var textFieldPrintName: PDTextField!
    @IBOutlet weak var textFieldAppliance: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.todayDate = patient.dateToday
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textFieldAppliance.isEmpty {
            self.showAlert("PLEASE ENTER APPLIANCE")
        } else if !signaturePatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "DeliveredApplianceFormVC") as! DeliveredApplianceFormViewController
            formVC.patient = self.patient
            formVC.patientSign = signaturePatient.signatureImage()
            formVC.appliance = textFieldAppliance.text
            formVC.relationship = textFieldRelationship.text
            formVC.printName = textFieldPrintName.text
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }

}
