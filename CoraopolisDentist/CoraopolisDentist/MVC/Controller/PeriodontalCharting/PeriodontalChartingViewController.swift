//
//  PeriodontalChartingViewController.swift
//  CoraopolisDentist
//
//  Created by Leojin Bose on 12/16/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PeriodontalChartingViewController: PDViewController {

    @IBOutlet weak var signatureProvider: SignatureView!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate : DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned() || !signatureProvider.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "PeriodontalChartingFormVC") as! PeriodontalChartingFormViewController
            formVC.patient = self.patient
            formVC.patientSign = signaturePatient.signatureImage()
            formVC.providerSign = signatureProvider.signatureImage()
            self.navigationController?.pushViewController(formVC, animated: true)
        }
        
    }

}
