//
//  OralSurgeryFormViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/19/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class OralSurgeryFormViewController: PDViewController {

    @IBOutlet weak var labelAuthorization : UILabel!
    @IBOutlet weak var labelProcedures : UILabel!
    @IBOutlet      var arrayProceduresButton : [UIButton]!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelDate3 : UILabel!
    @IBOutlet weak var labelDate4 : UILabel!
    @IBOutlet weak var imageviewPatient : UIImageView!
    @IBOutlet weak var imageviewParent : UIImageView!
    @IBOutlet weak var imageviewWitness : UIImageView!
    @IBOutlet weak var imageviewDoctor : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData (){
        labelAuthorization.text = patient.oral.authorization
        labelProcedures.text = patient.oral.procedures
        for btn in arrayProceduresButton{
            btn.isSelected = patient.oral.proceduresTag.contains(btn.tag)
        }
        labelPatientName.text = patient.fullName
        labelDate3.text = patient.dateToday
        labelDate4.text = patient.dateToday
        imageviewWitness.image = patient.oral.signWitness
        imageviewDoctor.image = patient.oral.signDoctor
        
        if patient.is18YearsOld == true{
            labelDate1.text = patient.dateToday
            labelDate2.text = ""
            imageviewPatient.image = patient.oral.signPatient
            imageviewParent.image = nil

        }else{
            labelDate2.text = patient.dateToday
            labelDate1.text = ""
            imageviewParent.image = patient.oral.signPatient
            imageviewPatient.image = nil

        }
    }


}
