//
//  OralSurgery1ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/19/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class OralSurgery1ViewController: PDViewController {

    @IBOutlet weak var textviewAuthorization : UITextView!
    @IBOutlet weak var textviewProcedures : UITextView!
    @IBOutlet  var arrayButtonProcedures : [UIButton]!
    @IBOutlet weak var signatureViewPatient : SignatureView!
    @IBOutlet weak var signatureViewWitness : SignatureView!
    @IBOutlet weak var signatureViewDoctor : SignatureView!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelNameTitle : UILabel!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var labelDate2 : DateLabel!
    @IBOutlet weak var labelDate3 : DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        labelDate3.todayDate = patient.dateToday
        textviewAuthorization.text = patient.oral.authorization == "N/A" ? "PLEASE TYPE HERE" : patient.oral.authorization
        textviewProcedures.text = patient.oral.procedures == "N/A" ? "PLEASE TYPE HERE" : patient.oral.procedures
        textviewProcedures.textColor = patient.oral.procedures == "N/A" ? UIColor.lightGray : UIColor.black
        textviewAuthorization.textColor = patient.oral.authorization == "N/A" ? UIColor.lightGray : UIColor.black
        for btn in arrayButtonProcedures{
            btn.isSelected = patient.oral.proceduresTag.contains(btn.tag)
        }

        if patient.is18YearsOld == true{
            labelNameTitle.text = "PATIENT SIGNATURE"
            labelPatientName.text = patient.fullName
        }else{
            labelNameTitle.text = "PARENT SIGNATURE"
            labelPatientName.text = ""
        }


    }
    
    func saveValue()  {
        patient.oral.authorization = textviewAuthorization.text == "PLEASE TYPE HERE" ? "N/A" : textviewAuthorization.text
        patient.oral.procedures = textviewProcedures.text == "PLEASE TYPE HERE" ? "N/A" : textviewProcedures.text

    }

    @IBAction override func buttonActionBack(withSender sender : UIButton) {
         saveValue()
        super.buttonActionBack(withSender: sender)
    }

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signatureViewPatient.isSigned() || !signatureViewWitness.isSigned() || !signatureViewDoctor.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped || !labelDate2.dateTapped || !labelDate3.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValue()
            patient.oral.signPatient = signatureViewPatient.signatureImage()
            patient.oral.signWitness = signatureViewWitness.signatureImage()
            patient.oral.signDoctor = signatureViewDoctor.signatureImage()
            let oral = consentStoryboard.instantiateViewController(withIdentifier: "OralFormVC") as! OralSurgeryFormViewController
            oral.patient = self.patient
            self.navigationController?.pushViewController(oral, animated: true)

        }
        
    }

    @IBAction func onProceduresButtonPressed (withSender sender : UIButton){
        sender.isSelected = !sender.isSelected
        if patient.oral.proceduresTag.contains(sender.tag){
            patient.oral.proceduresTag.remove(at: patient.oral.proceduresTag.index(of: sender.tag)!)
        }else{
            patient.oral.proceduresTag.append(sender.tag)
        }
        
    }
}

extension OralSurgery1ViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "PLEASE TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "PLEASE TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
