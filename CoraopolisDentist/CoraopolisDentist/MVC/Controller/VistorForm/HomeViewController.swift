 //
//  HomeViewController.swift
//  Advanced Aesthetic Dentistry
//
//  Created by Leojin Bose on 17/02/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}





class HomeViewController: PDViewController {

    
    @IBOutlet weak var textFieldToothNumbers: PDTextField!
    @IBOutlet weak var labelAlertHeader: UILabel!
    @IBOutlet weak var labelAlertFooter: UILabel!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet var viewToothNumbers: PDView!
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var tableViewForms: UITableView!
    @IBOutlet weak var viewAlert: PDView!
    @IBOutlet weak var buttonLogout: UIButton!
    
    @IBOutlet weak var labelVersion: UILabel!
    
    var selectedForms : [Forms]! = [Forms]()
    var formList : [Forms]! = [Forms]()
    var consentIndex : Int = 4
    var patNumber = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.showAlert as (HomeViewController) -> () -> ()), name: NSNotification.Name(rawValue: kFormsCompletedNotification), object: nil)

        if let text = Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String {
            labelVersion.text = text
        }
        // Do any additional setup after loading the view, typically from a nib.
        NotificationCenter.default.addObserver(self, selector: #selector(dateChangedNotification), name: NSNotification.Name(rawValue: kDateChangedNotification), object: nil)
        self.dateChangedNotification()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    func dateChangedNotification() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        labelDate.text = dateFormatter.string(from: Date()).uppercased()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.background).async {
            let defaults = UserDefaults.standard
            ServiceManager.loginWithUsername(defaults.value(forKey: kAppLoginUsernameKey) as! String, password: defaults.value(forKey: kAppLoginPasswordKey) as! String) { (success, error) -> (Void) in
                if success {
                    
                } else {
                    if error == nil {
                        
                    } else {
                        DispatchQueue.main.async(execute: {
                            UserDefaults.standard.set(false, forKey: "kApploggedIn")
                            UserDefaults.standard.synchronize()
                            (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
                        })
                    }
                }
            }
        }
        Forms.getAllForms { (isConnectionFailed, forms) -> Void in
            self.formList = forms
            self.tableViewForms.reloadData()
            if isConnectionFailed == true {
                let alertController = UIAlertController(title: "Coraopolis Dentist", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
                let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url)
                    }
                }
                let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    
                }
                alertController.addAction(alertOkAction)
                alertController.addAction(alertCancelAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }


    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        selectedForms.removeAll()
        for form in formList {
            if form.isSelected == true {
                for subForm in form.subForms {
                    if subForm.isSelected == true {
                        selectedForms.append(subForm)
                    }
                }
                
            }
        }
        self.view.endEditing(true)

        let patient = PDPatient(forms: selectedForms)
        patient.dateToday = labelDate.text
        patient.patientNumber = patNumber


        
       // let formNames = (patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
        
//        if formNames.contains(kVisitorCheckInForm) && selectedForms.count > 0{
//            
//            let patientInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("kVisitorVC") as! VisitorCheckinVC
//            patientInfoVC.patient = patient
//            self.navigationController?.pushViewController(patientInfoVC, animated: true)
//            
//        }else
        if selectedForms.count == 1 && selectedForms.first!.formTitle == kFeedbackForm {
        
            let patientInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kFeedBackInfoViewController") as! FeedBackInfoViewController
            patientInfoVC.patient = patient
            self.navigationController?.pushViewController(patientInfoVC, animated: true)
        }else if selectedForms.count > 0 {
            
            let patientInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientInfoVC") as! PatientInfoViewController
            patientInfoVC.patient = patient
            self.navigationController?.pushViewController(patientInfoVC, animated: true)
        } else {
            let alert = Extention.alert("PLEASE SELECT ANY FORM")
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func buttonActionLogout(_ sender: AnyObject) {
//        GTMOAuth2ViewControllerTouch.removeAuthFromKeychainForName(kKeychainItemName)
//        buttonLogout.hidden = true
    }
    
    

    
    @IBAction func buttonActionDone(_ sender: AnyObject) {
        textFieldToothNumbers.resignFirstResponder()
        self.viewToothNumbers.removeFromSuperview()
        self.viewShadow.isHidden = true
        let form =  textFieldToothNumbers.tag <= consentIndex ? formList[textFieldToothNumbers.tag] : formList[consentIndex].subForms[textFieldToothNumbers.tag - (consentIndex + 1)]
        if !textFieldToothNumbers.isEmpty || textFieldToothNumbers.tag == 12 {
            form.isSelected = true
            form.toothNumbers = textFieldToothNumbers.text
        } else {
            form.isSelected = false
        }
        self.tableViewForms.reloadData()

    }
    
    func showAlert() {
        viewAlert.isHidden = false
        viewShadow.isHidden = false
    }
    
    @IBAction func buttonActionOk(_ sender: AnyObject) {
        
        viewAlert.isHidden = true
        viewShadow.isHidden = true
    }
    
    
    func showPopup() {
        textFieldToothNumbers.text = ""
        self.viewToothNumbers.frame = CGRect(x: 0, y: 0, width: 512.0, height: 250.0)
        self.viewToothNumbers.center = self.view.center
        self.viewShadow.addSubview(self.viewToothNumbers)
        self.viewToothNumbers.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        self.viewShadow.isHidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.viewToothNumbers.transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    


}

extension HomeViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
     
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

                if string.lengthOfBytes(using: String.Encoding.utf8) == 0 {
            return true
        }
        if string.rangeOfCharacter(from: CharacterSet(charactersIn: "01234567890,").inverted) != nil {
            return false
        }
        
        let newRange = textField.text!.characters.index(textField.text!.startIndex, offsetBy: range.location)..<textField.text!.characters.index(textField.text!.startIndex, offsetBy: range.location + range.length)
        let textFieldString = textField.text!.replacingCharacters(in: newRange, with: string)
        let textString = textFieldString.components(separatedBy: ",")
        
        if textFieldString.characters.count > 2 {
            let lastString = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 1))
            let lastTwoStrings = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 2))
            let lastThreeStrings = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 3))
            
            if lastTwoStrings == ",," {
                return false
            }
            if lastString == "," && lastThreeStrings.components(separatedBy: ",").count == 3 {
                let requiredString = textFieldString.substring(to: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 2)) + "0" + lastTwoStrings
                textField.text = requiredString
                return false
            }
            
        } else {
            if textFieldString.characters.count == 2 {
                let lastString = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 1))
                if lastString == "," {
                    textField.text = "0" + textFieldString
                    return false
                }
                
            }
            if textFieldString == "," {
                return false
            }
        }
        
        
        
        for text in textString {
            if text == "0" {
                return true
            }
            if text == "00" {
                return false
            }
            if Int(text) > 35 {
                return false
            }
        }
        return true
    }
}


//extension HomeViewController : UITableViewDelegate {
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.row == consentIndex {
//            let subForms = formList[consentIndex].subForms
//            for subFrom in subForms! {
//                subFrom.isSelected = false
//            }
//            let form = self.formList[consentIndex]
//            form.isSelected = !form.isSelected
//            var indexPaths : [IndexPath] = [IndexPath]()
//            for (idx, _) in form.subForms.enumerated() {
//                let indexPath = IndexPath(row: consentIndex + 1 + idx, section: 0)
//                indexPaths.append(indexPath)
//            }
//            if form.isSelected == true {
//                tableView.beginUpdates()
//                tableView.insertRows(at: indexPaths, with: .bottom)
//                tableView.endUpdates()
//                let delayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
//                DispatchQueue.main.asyncAfter(deadline: delayTime) {
//                    if form.isSelected == true {
//                        tableView.scrollToRow(at: indexPaths.last!, at: .top, animated: true)
//                    }
//                }
//            } else {
//                tableView.beginUpdates()
//                tableView.deleteRows(at: indexPaths, with: .bottom)
//                tableView.endUpdates()
//            }
//            tableView.reloadRows(at: [indexPath], with: .none)
//            return
//        }
//       // let formsCount = formList[5].isSelected == true ? formList.count + formList[5].subForms.count  : formList.count
//        var form : Forms!
//        if (indexPath.row <= consentIndex) {
//            form = formList[indexPath.row]
//        } else if formList[consentIndex].isSelected && indexPath.row <= consentIndex + formList[consentIndex].subForms.count {
//            form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
//        } else {
//            form = formList.last
//        }
//        
//        if form.isToothNumberRequired == true && !form.isSelected {
//            textFieldToothNumbers.tag = indexPath.row
//            self.showPopup()
//
//        } else {
//            form.isSelected = !form.isSelected
//        }
//        
////        if indexPath.row == 0 && form.isSelected{
////            PopupTextField.sharedInstance.showWithPlaceHolder("PATIENT NUMBER", keyboardType: UIKeyboardType.NumbersAndPunctuation, textFormat: TextFormat.Default, completion: { (textField, isEdited) in
////                if isEdited{
////                    self.patNumber = textField.text!
////                }else{
////                    self.patNumber = "N/A"
////                }
////            })
////        }
//        tableView.reloadData()
//    }
//}

//extension HomeViewController : UITableViewDataSource {
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//       if formList.count > 0 {
//           let subForms = formList[consentIndex].subForms
//            return formList[consentIndex].isSelected == true ? formList.count + subForms!.count  : formList.count
//       } else {
//            return formList.count
//        }
//        
//    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if (indexPath.row == 19){
//            return 90
//        } else if (indexPath.row == 20){
//            return 70
//        } else{
//            return 44 //a default size if the cell index path is anything other than the 1st or second row.
//        }
//
//    }
//
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if (indexPath.row <= consentIndex) {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMainForm", for: indexPath) as! FormsTableViewCell
//            let form = formList[indexPath.row]
//            cell.labelFormName.text = form.formTitle
//            cell.imageViewCheckMark.isHidden = !form.isSelected
//            cell.backgroundColor = UIColor.clear
//            cell.contentView.backgroundColor = UIColor.clear
//            return cell
//        } else if formList[consentIndex].isSelected && indexPath.row <= consentIndex + formList[consentIndex].subForms.count {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSubForm", for: indexPath) as! FormsTableViewCell
//            let form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
//            cell.labelFormName.text = form.formTitle
//            cell.imageViewCheckMark.isHidden = !form.isSelected
//            cell.backgroundColor = UIColor.clear
//            cell.contentView.backgroundColor = UIColor.clear
//            return cell
//        } else {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMainForm", for: indexPath) as! FormsTableViewCell
//            let form = formList[consentIndex + 1]
//            cell.labelFormName.text = form.formTitle
//            cell.imageViewCheckMark.isHidden = !form.isSelected
//            cell.backgroundColor = UIColor.clear
//            cell.contentView.backgroundColor = UIColor.clear
//            return cell
//        }
//        
//    }
//}
 
 
 extension HomeViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //var form : Forms!
        let form = formList[indexPath.section].subForms[indexPath.row]
        if form.isToothNumberRequired == true && !form.isSelected {
            textFieldToothNumbers.tag = indexPath.row
            self.showPopup()
            
        } else {
            form.isSelected = !form.isSelected
        }
        tableView.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSubForm") as! FormsTableViewCell
        let form = formList[indexPath.section].subForms[indexPath.row]
        let height = form.formTitle.heightWithConstrainedWidth(480, font: cell.labelFormName.font) + 20
        return height
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let form = formList[section]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMainForm") as! FormsTableViewCell
        let headerView = cell.contentView.subviews[0] as! PDTableHeaderView
        headerView.labelFormName.text = form.formTitle
        headerView.labelFormName.textColor = UIColor.white
        headerView.imageViewCheckMark.isHidden = !form.isSelected
        headerView.tag = section
        if headerView.gestureRecognizers == nil {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction))
            headerView.addGestureRecognizer(tapGesture)
        }
        
        return cell.contentView
    }
    
    func tapGestureAction(_ sender : UITapGestureRecognizer) {
        let headerView = sender.view as! PDTableHeaderView
        let form = formList[headerView.tag]
        form.isSelected = !form.isSelected
        headerView.imageViewCheckMark.isHidden = !form.isSelected
        var indexPaths : [IndexPath] = [IndexPath]()
        for (idx, formObj) in form.subForms.enumerated() {
            if form.isSelected == false {
                formObj.isSelected = false
            }
            let indexPath = IndexPath(row: idx, section: headerView.tag)
            indexPaths.append(indexPath)
        }
        if form.isSelected == true && indexPaths.count > 0 {
            self.tableViewForms.insertRows(at: indexPaths, with: .automatic)
            let delayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                if form.isSelected == true {
                    self.tableViewForms.scrollToRow(at: indexPaths.last!, at: .bottom, animated: true)
                }
            }
        } else {
            self.tableViewForms.deleteRows(at: indexPaths, with: .automatic)
        }
    }
 }
 
 extension HomeViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return formList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let form = formList[section]
        return form.isSelected == true ? form.subForms.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSubForm", for: indexPath) as! FormsTableViewCell
        let form = formList[indexPath.section].subForms[indexPath.row]
        cell.labelFormName.text = form.formTitle
        cell.imageViewCheckMark.isHidden = !form.isSelected
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        return cell
    }
    
    
 }



