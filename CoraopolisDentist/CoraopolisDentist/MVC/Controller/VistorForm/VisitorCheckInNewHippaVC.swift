//
//  ConsentForPrivacyPracticeVC.swift
//  DentalHealthAndBeauty
//
//  Created by SRS on 02/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class VisitorCheckInNewHippaVC: PDViewController {

    
    @IBOutlet weak var VisitorPrintName: UILabel!
    
    @IBOutlet weak var VisitorDate: UILabel!
    
    @IBOutlet weak var VisitorSign: SignatureView!
    
    @IBOutlet weak var VisitorName: UILabel!
    
    @IBOutlet weak var dateLabel: DateLabel!
    
    @IBOutlet weak var doneBtn: PDButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        VisitorName.text = patient.VisitorFullName
        VisitorDate.text = patient.dateToday
        VisitorPrintName.text = patient.VisitorFullName
        dateLabel.todayDate = patient.dateToday
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func nextBtnPressed(_ sender: AnyObject) {
      
        if !VisitorSign.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !dateLabel.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            
            self.doneBtn.isUserInteractionEnabled = false
            ServiceManager.ChekInFormStatus(VisitorName.text!, patientPurpose: patient.VisitingPurpose, signatureImage: VisitorSign.signatureImage(), completion: { (success, error) in
                if success {
                    
                    self.patient.selectedForms.removeFirst()
                    if self.patient.selectedForms.count > 0 {
                        if self.patient.selectedForms.count == 1 && self.patient.selectedForms.first!.formTitle == kFeedbackForm {
                            let patientInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kFeedBackInfoViewController") as! FeedBackInfoViewController
                            patientInfoVC.patient = self.patient
                            self.navigationController?.pushViewController(patientInfoVC, animated: true)
                        }else {
                            
                            let patientInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientInfoVC") as! PatientInfoViewController
                            patientInfoVC.patient = self.patient
                            self.navigationController?.pushViewController(patientInfoVC, animated: true)
                        }
                    } else {
                        NotificationCenter.default.post(name: Notification.Name(rawValue: kFormsCompletedNotification), object: nil)
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                    
                } else {
                    if error == nil {
                        let alert = Extention.alert("PLEASE CHECK YOUR INTERNET CONNECTION AND TRY AGAIN")
                        self.present(alert, animated: true, completion: nil)
                        self.doneBtn.isUserInteractionEnabled = true
                    } else {
                        let alert = Extention.alert(error!.localizedDescription.uppercased())
                        self.present(alert, animated: true, completion: nil)
                        self.doneBtn.isUserInteractionEnabled = true
                    }
                }

            })
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
