//
//  ReservationFeeFormViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/19/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ReservationFeeFormViewController: PDViewController {

    var patientSign : UIImage!
    @IBOutlet var imageviewPatientSign : UIImageView!
    @IBOutlet var labelDate : UILabel!
    @IBOutlet var labelPatientName : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        imageviewPatientSign.image = patientSign
        labelDate.text = patient.dateToday
        labelPatientName.text = patient.fullName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
