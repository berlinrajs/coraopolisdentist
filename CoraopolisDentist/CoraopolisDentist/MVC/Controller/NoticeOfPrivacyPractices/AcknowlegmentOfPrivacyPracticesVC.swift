//
//  AcknowlegmentOfPrivacyPracticesVC.swift
//  CoraopolisDentist
//
//  Created by Manjusha Chembra on 4/18/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import Foundation

class AcknowlegmentOfPrivacyPracticesVC: PDViewController {
    
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDetails : UILabel!
    var patientSign : UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
        labelDetails.text = labelDetails.text?.replacingOccurrences(of: "kPatientName", with: patient.fullName)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned() && !self.patient.signRefused{
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped && !self.patient.signRefused{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kNoticeOfPrivacyPracticesFormVC") as! NoticeOfPrivacyPracticesFormVC
            formVC.patient = self.patient
            formVC.patientSignature = patientSign
            formVC.patientSignature1 = signaturePatient.signatureImage()
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
    
}
