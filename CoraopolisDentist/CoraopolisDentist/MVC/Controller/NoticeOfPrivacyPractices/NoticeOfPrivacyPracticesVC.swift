//
//  NoticeOfPrivacyPracticesVC.swift
//  CoraopolisDentist
//
//  Created by Manjusha Chembra on 4/18/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import Foundation

class NoticeOfPrivacyPracticesVC: PDViewController {
    
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            
            
            HipaaSignatureRefusalPopup.popUpView().showInViewController(viewController: self, completion: { (popUpView, refused, textFieldOther, refusalTag) in
                popUpView.close()
                self.patient.signRefused = refused
                self.patient.signRefusalTag = refused == false ? 0 : refusalTag
                self.patient.signRefusalOther = refused == false ? "" : refusalTag == 4 ? textFieldOther.text! : ""
                
                let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kAcknowlegmentOfPrivacyPracticesVC") as! AcknowlegmentOfPrivacyPracticesVC
                formVC.patient = self.patient
                formVC.patientSign = self.signaturePatient.signatureImage()
                self.navigationController?.pushViewController(formVC, animated: true)
                
                
            }, error: { (message) in
                self.showAlert(message)
            })
        }
        
    }
    
}
