//
//  NoticeOfPrivacyPracticesFormVC.swift
//  CoraopolisDentist
//
//  Created by Manjusha Chembra on 4/18/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import Foundation

class NoticeOfPrivacyPracticesFormVC: PDViewController {
    
    var patientSignature : UIImage!
    var patientSignature1 : UIImage!
  
    @IBOutlet var imageviewPatientSign : UIImageView!
    @IBOutlet var labelDate : FormLabel!
    @IBOutlet var labelPatientName : UILabel!
    @IBOutlet var imageviewPatientSign1 : UIImageView!
    @IBOutlet var labelDate1 : FormLabel!
    @IBOutlet var labelPatientName1 : FormLabel!
    @IBOutlet weak var radioRefusal: RadioButton!
    @IBOutlet var labelDetails : UILabel!
    @IBOutlet var labelRefusalOtherReason : FormLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageviewPatientSign.image = patientSignature
        labelDate.text = patient.dateToday
        labelPatientName.text = patient.fullName
        
        imageviewPatientSign1.image = patientSignature1
        labelDate1.text = patient.dateToday
        labelPatientName1.text = patient.fullName
        labelDetails.text = labelDetails.text?.replacingOccurrences(of: "kPatientName", with: patient.fullName)
        if self.patient.signRefused
        {
           radioRefusal.setSelectedWithTag(patient.signRefusalTag)
            labelRefusalOtherReason.text = patient.signRefusalOther
        }

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
}
