//
//  HIPAAFirstVC.swift
//  CoraopolisDentist
//
//  Created by Manjusha Chembra on 4/20/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import Foundation


class HIPAAFirstVC: PDViewController {
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDetails : UILabel!
    @IBOutlet weak var labelDate : DateLabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
        labelDetails.text = labelDetails.text?.replacingOccurrences(of: "kPatientName", with: patient.fullName)
               // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned()  {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{

            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kHIPAAVC") as! HIPAAVC
            formVC.patient = self.patient
            formVC.patientSignature = signaturePatient.signatureImage()
            self.navigationController?.pushViewController(formVC, animated: true)
        }
        
    }
    
}
