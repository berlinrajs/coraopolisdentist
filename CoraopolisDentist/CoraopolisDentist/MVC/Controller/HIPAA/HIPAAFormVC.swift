//
//  HIPAAFormVC.swift
//  CoraopolisDentist
//
//  Created by Manjusha Chembra on 4/20/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import Foundation

class HIPAAFormVC: PDViewController {
    
    var patientSign : UIImage!
    
    
    @IBOutlet var imageviewPatientSign1 : UIImageView!
    @IBOutlet var labelDate1 : FormLabel!

    @IBOutlet var labelName1 : FormLabel!
    @IBOutlet var labelRelationship1 : FormLabel!
    @IBOutlet var labelName2 : FormLabel!
    @IBOutlet var labelRelationship2 : FormLabel!
    @IBOutlet var labelName3 : FormLabel!
    @IBOutlet var labelRelationship3 : FormLabel!
    
    @IBOutlet var labelPatientName : FormLabel!
    @IBOutlet var labelPatientName1 : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate1.text = patient.dateToday
        labelPatientName.text = patient.fullName
        labelPatientName1.text = patient.fullName
        
        labelName1.text = patient.name1
        labelRelationship1.text = patient.relationship1
        labelName2.text = patient.name2
        labelRelationship2.text = patient.relationship2
        labelName3.text = patient.name3
        labelRelationship3.text = patient.relationship3
        imageviewPatientSign1.image = patientSign
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
