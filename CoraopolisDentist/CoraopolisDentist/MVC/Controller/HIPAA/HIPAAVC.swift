//
//  HIPAAVC.swift
//  CoraopolisDentist
//
//  Created by Manjusha Chembra on 4/20/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import Foundation

import Foundation

class HIPAAVC: PDViewController {


    
    @IBOutlet weak var textFieldName1: PDTextField!
    @IBOutlet weak var textFieldRelationship1: PDTextField!
    @IBOutlet weak var textFieldName2: PDTextField!
    @IBOutlet weak var textFieldRelationship2: PDTextField!
    @IBOutlet weak var textFieldName3: PDTextField!
    @IBOutlet weak var textFieldRelationship3: PDTextField!
    
    var patientSignature : UIImage!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.endEditing(true)
        loadValue()
        // Do any additional setup after loading the view.
    }
    
    
    func loadValue()  {

        textFieldName1.text = patient.name1
        textFieldRelationship1.text = patient.relationship1
        textFieldName2.text = patient.name2
        textFieldRelationship2.text = patient.relationship2
        textFieldName3.text = patient.name3
        textFieldRelationship3.text = patient.relationship3
        
    }
    
    func saveValue()  {

        patient.name1 = textFieldName1.text!
        patient.relationship1 = textFieldRelationship1.text!
        patient.name2 = textFieldName2.text!
        patient.relationship2 = textFieldRelationship2.text!
        patient.name3 = textFieldName3.text!
        patient.relationship3 = textFieldRelationship3.text!

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
            self.view.endEditing(true)
            saveValue()
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kHIPAAFormVC") as! HIPAAFormVC
            formVC.patient = self.patient
            formVC.patientSign = patientSignature
            
            self.navigationController?.pushViewController(formVC, animated: true)
       
    }
    
}
