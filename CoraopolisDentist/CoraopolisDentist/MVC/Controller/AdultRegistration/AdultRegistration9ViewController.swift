//
//  AdultRegistration9ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/24/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AdultRegistration9ViewController: PDViewController {
    
    @IBOutlet weak var radioHospital : RadioButton!
    @IBOutlet weak var textfieldName : UITextField!
    @IBOutlet weak var textfieldState : UITextField!
    @IBOutlet weak var textfieldCity : UITextField!
    @IBOutlet weak var textfieldPhone : UITextField!
    @IBOutlet weak var radioTobacco : RadioButton!
    @IBOutlet weak var radioAlcohol : RadioButton!
    @IBOutlet weak var radioPregnant : RadioButton!
    @IBOutlet weak var radioNursing : RadioButton!
    @IBOutlet weak var viewHospital : UIView!
    @IBOutlet weak var viewWoman : UIView!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loadValue()  {
        StateListView.addStateListForTextField(textfieldState)
        radioHospital.setSelectedWithTag(patient.adult.radioHospitalizedTag)
        if patient.adult.radioHospitalizedTag == 1{
            viewHospital.isUserInteractionEnabled = true
            viewHospital.alpha = 1.0
            textfieldName.setSavedText(patient.adult.hospitalName)
            textfieldCity.setSavedText(patient.adult.hospitalCity)
            textfieldState.setSavedText(patient.adult.hospitalState)
            textfieldPhone.setSavedText(patient.adult.hospitalPhone)
        }else{
            viewHospital.isUserInteractionEnabled = false
            viewHospital.alpha = 0.5
            textfieldName.text = ""
            textfieldCity.text = ""
            textfieldState.text = "PA"
            textfieldPhone.text = ""
        }
        radioTobacco.setSelectedWithTag(patient.adult.radioTobaccoTag)
        radioAlcohol.setSelectedWithTag(patient.adult.radioAlcoholTag)
        
        if patient.adult.radioGenderTag == 1{
            viewWoman.isUserInteractionEnabled = false
            viewWoman.alpha = 0.5
        }else{
            viewWoman.isUserInteractionEnabled = true
            viewWoman.alpha = 1.0
            radioPregnant.setSelectedWithTag(patient.adult.radioPregnantTag)
            radioNursing.setSelectedWithTag(patient.adult.radioNursingTag)

        }
        
    }
    
    func saveValue()  {
        patient.adult.radioHospitalizedTag = radioHospital.selected.tag
        patient.adult.hospitalName = textfieldName.getText()
        patient.adult.hospitalState = textfieldState.getText()
        patient.adult.hospitalCity = textfieldCity.getText()
        patient.adult.hospitalPhone = textfieldPhone.getText()
        patient.adult.radioTobaccoTag = radioTobacco.selected == nil ? 0 : radioTobacco.selected.tag
        patient.adult.radioNursingTag = radioNursing.selected == nil ? 0 : radioNursing.selected.tag
        patient.adult.radioPregnantTag = radioPregnant.selected == nil ? 0 : radioPregnant.selected.tag
        patient.adult.radioAlcoholTag = radioAlcohol.selected == nil ? 0 : radioAlcohol.selected.tag
        
        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldPhone.isEmpty && !textfieldPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID PHONE NUMBER")
        }else if (patient.adult.radioGenderTag == 2 && (radioPregnant.selected == nil || radioNursing.selected == nil)) || radioTobacco.selected == nil || radioAlcohol.selected == nil{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else{
        saveValue()
        let adult = adultStoryboard.instantiateViewController(withIdentifier: "Adult10VC") as! AdultRegistration10ViewController
        adult.patient = self.patient
        self.navigationController?.pushViewController(adult, animated: true)
        }
    }
    
    @IBAction func onHospitalButtonPressed (withSender sender : RadioButton){
        if sender.tag == 1{
            PopupTextField.sharedInstance.showWithPlaceHolder("FOR WHAT?", keyboardType: UIKeyboardType.default, textFormat: TextFormat.default, completion: { (textField, isEdited) in
                if isEdited{
                    self.viewHospital.isUserInteractionEnabled = true
                    self.viewHospital.alpha = 1.0
                    self.patient.adult.hospitalizedReason = textField.text!

                }else{
                    self.viewHospital.isUserInteractionEnabled = false
                    self.viewHospital.alpha = 0.5
                    self.textfieldName.text = ""
                    self.textfieldCity.text = ""
                    self.textfieldState.text = "PA"
                    self.textfieldPhone.text = ""
                    self.patient.adult.hospitalizedReason = "N/A"
                    sender.isSelected = false



                }
            })
        }else{
            viewHospital.isUserInteractionEnabled = false
            viewHospital.alpha = 0.5
            textfieldName.text = ""
            textfieldCity.text = ""
            textfieldState.text = "PA"
            textfieldPhone.text = ""
            self.patient.adult.hospitalizedReason = "N/A"

        }
    }
    
    @IBAction func onPregnantButtonPressed (withSender sender : RadioButton){
        if sender.tag == 2{
            YesOrNoAlert.sharedInstance.showWithTitle("Are you taking birth control pills?", button1Title: "YES", button2Title: "NO", completion: { (buttonIndex) in
                self.patient.adult.radioBirthControlTag = buttonIndex
            })
        }else{
            self.patient.adult.radioBirthControlTag = 0
        }
        
    }

    
}

extension AdultRegistration9ViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldPhone {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

