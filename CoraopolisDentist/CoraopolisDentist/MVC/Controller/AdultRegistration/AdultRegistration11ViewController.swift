//
//  AdultRegistration11ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/24/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AdultRegistration11ViewController: PDViewController {

    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var buttonVerified: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if buttonVerified.isSelected == false {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.present(alert, animated: true, completion: nil)
        } else {
            let new1VC = self.patientStoryBoard.instantiateViewController(withIdentifier: "PatientInformation1VC") as! PatientInformation1ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
            
        }
    }



}

extension AdultRegistration11ViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.adult.allergyQuestions.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PatientInfoCell
        cell.delegate = self
        cell.tag = indexPath.row
        cell.configCell(self.patient.adult.allergyQuestions[indexPath.row])
        
        return cell
    }
}

extension AdultRegistration11ViewController : PatientInfoCellDelegate{
    
    func radioButtonTappedForCell(_ cell: PatientInfoCell) {
            PopupTextField.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", keyboardType: UIKeyboardType.default, textFormat: TextFormat.default) { (textField, isEdited) in
                if isEdited{
                    self.patient.adult.allergyQuestions[cell.tag].answer = textField.text
                }else{
                    cell.radioButtonYes.isSelected = false
                    self.patient.adult.allergyQuestions[cell.tag].selectedOption = false
                }
                
            }
    }
}

