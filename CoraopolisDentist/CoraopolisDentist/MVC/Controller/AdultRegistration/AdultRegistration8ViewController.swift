//
//  AdultRegistration8ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/24/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AdultRegistration8ViewController: PDViewController {

    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var buttonVerified: UIButton!
    @IBOutlet var buttonNext: UIButton!
    @IBOutlet var buttonBack1: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var labelHeading : UILabel!
    var selectedIndex: Int = 0

    @IBOutlet weak var viewContainer : UIView!
    @IBOutlet weak var textviewDetails : UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.activityIndicator.stopAnimating()
        viewContainer.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBackButton1Pressed (withSender sender: AnyObject) {
        self.patient.adult.dentalHistoryDetails = textviewDetails.text! == "PLEASE DESCRIBE" ? "N/A" : textviewDetails.text!
        if selectedIndex == 0 {
            self.navigationController?.popViewController(animated: true)
        } else {
            buttonBack1.isUserInteractionEnabled = false
            buttonNext.isUserInteractionEnabled = false
            
            self.activityIndicator.startAnimating()
            let delayTime = DispatchTime.now() + Double(Int64(0.4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                self.activityIndicator.stopAnimating()
                self.selectedIndex = self.selectedIndex - 1
                self.labelHeading.text = self.selectedIndex == 0 ? "" : "Have you ever had any of the following: *"
                self.tableViewQuestions.reloadData()
                self.viewContainer.isHidden = self.selectedIndex == 0
                self.buttonBack1.isUserInteractionEnabled = true
                self.buttonNext.isUserInteractionEnabled = true
            }
            
        }
    }
    
    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if buttonVerified.isSelected == false {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.present(alert, animated: true, completion: nil)
        } else {
            if selectedIndex == 1 {
                self.patient.adult.dentalHistoryDetails = textviewDetails.text! == "PLEASE DESCRIBE" ? "N/A" : textviewDetails.text!
                let adult = adultStoryboard.instantiateViewController(withIdentifier: "Adult9VC") as! AdultRegistration9ViewController
                adult.patient = self.patient
                self.navigationController?.pushViewController(adult, animated: true)
                
            } else {
                buttonBack1.isUserInteractionEnabled = false
                buttonNext.isUserInteractionEnabled = false
                self.buttonVerified.isSelected = false
                self.activityIndicator.startAnimating()
                let delayTime = DispatchTime.now() + Double(Int64(0.4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    self.activityIndicator.stopAnimating()
                    self.selectedIndex = self.selectedIndex + 1
                    self.labelHeading.text = self.selectedIndex == 0 ? "" : "Have you ever had any of the following: *"
                    self.tableViewQuestions.reloadData()
                    self.viewContainer.isHidden = self.selectedIndex == 0
                    self.textviewDetails.text = self.patient.adult.dentalHistoryDetails == "N/A" ? "PLEASE DESCRIBE" : self.patient.adult.dentalHistoryDetails
                    self.textviewDetails.textColor = self.textviewDetails.text == "PLEASE DESCRIBE" ? UIColor.lightGray : UIColor.black
                    self.buttonBack1.isUserInteractionEnabled = true
                    self.buttonNext.isUserInteractionEnabled = true
                }
            }
        }
    }


}

extension AdultRegistration8ViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.adult.dentalQuestions[selectedIndex].count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return selectedIndex == 0 ? 44 : 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PatientInfoCell
        cell.delegate = self
        cell.tag = indexPath.row
        cell.configCell(self.patient.adult.dentalQuestions[selectedIndex][indexPath.row])
        
        return cell
    }
}

extension AdultRegistration8ViewController : PatientInfoCellDelegate{
    
    func radioButtonTappedForCell(_ cell: PatientInfoCell) {
        if selectedIndex == 0{
            PopupTextField.sharedInstance.showWithPlaceHolder(cell.tag == 13 ? "HOW LONG AGO?" : cell.tag == 12 ? "PLEASE LIST THE NAMES" : "PLEASE DESCRIBE" , keyboardType: UIKeyboardType.default, textFormat: TextFormat.default) { (textField, isEdited) in
            if isEdited{
                self.patient.adult.dentalQuestions[self.selectedIndex][cell.tag].answer = textField.text
            }else{
                cell.radioButtonYes.isSelected = false
                self.patient.adult.dentalQuestions[self.selectedIndex][cell.tag].selectedOption = false
            }

        }
        
        }else{
            PopupTextView.sharedInstance.showWithPlaceHolder("PLEASE DESCRIBE", completion: { (textView, isEdited) in
                if isEdited{
                    self.patient.adult.dentalQuestions[self.selectedIndex][cell.tag].answer = textView.text
                }else{
                    cell.radioButtonYes.isSelected = false
                    self.patient.adult.dentalQuestions[self.selectedIndex][cell.tag].selectedOption = false
                }

            })
        }
}
}

extension AdultRegistration8ViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "PLEASE DESCRIBE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty  {
            textView.text = "PLEASE DESCRIBE"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
