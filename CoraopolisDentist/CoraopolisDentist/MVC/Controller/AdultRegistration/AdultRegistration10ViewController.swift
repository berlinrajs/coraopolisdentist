//
//  AdultRegistration10ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/24/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AdultRegistration10ViewController: PDViewController {
    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var buttonVerified: UIButton!
    @IBOutlet var buttonNext: UIButton!
    @IBOutlet var buttonBack1: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    var selectedIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        self.activityIndicator.stopAnimating()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBackButton1Pressed (withSender sender: AnyObject) {
        if selectedIndex == 0 {
            self.navigationController?.popViewController(animated: true)
        } else {
            buttonBack1.isUserInteractionEnabled = false
            buttonNext.isUserInteractionEnabled = false
            
            self.activityIndicator.startAnimating()
            let delayTime = DispatchTime.now() + Double(Int64(0.4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                self.activityIndicator.stopAnimating()
                self.selectedIndex = self.selectedIndex - 1
                self.tableViewQuestions.reloadData()
                self.buttonBack1.isUserInteractionEnabled = true
                self.buttonNext.isUserInteractionEnabled = true
            }
            
        }
    }
    
    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if buttonVerified.isSelected == false {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.present(alert, animated: true, completion: nil)
        } else {
            if selectedIndex == 3 {
                let adult = adultStoryboard.instantiateViewController(withIdentifier: "Adult11VC") as! AdultRegistration11ViewController
                adult.patient = self.patient
                self.navigationController?.pushViewController(adult, animated: true)
                
            } else {
                buttonBack1.isUserInteractionEnabled = false
                buttonNext.isUserInteractionEnabled = false
                self.buttonVerified.isSelected = false
                self.activityIndicator.startAnimating()
                let delayTime = DispatchTime.now() + Double(Int64(0.4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    self.activityIndicator.stopAnimating()
                    self.selectedIndex = self.selectedIndex + 1
                    self.tableViewQuestions.reloadData()
                    self.buttonBack1.isUserInteractionEnabled = true
                    self.buttonNext.isUserInteractionEnabled = true
                }
            }
        }
    }
    

}
extension AdultRegistration10ViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.adult.medicalQuestions[selectedIndex].count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PatientInfoCell
        cell.delegate = self
        cell.tag = indexPath.row
        cell.configCell(self.patient.adult.medicalQuestions[selectedIndex][indexPath.row])
        
        return cell
    }
}

extension AdultRegistration10ViewController : PatientInfoCellDelegate{
    
    func radioButtonTappedForCell(_ cell: PatientInfoCell) {
        if selectedIndex == 3{
            PopupTextField.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", keyboardType: UIKeyboardType.default, textFormat: TextFormat.default) { (textField, isEdited) in
                if isEdited{
                    self.patient.adult.medicalQuestions[self.selectedIndex][cell.tag].answer = textField.text
                }else{
                    cell.radioButtonYes.isSelected = false
                    self.patient.adult.medicalQuestions[self.selectedIndex][cell.tag].selectedOption = false
                }
                
            }
            
        }else{
            HepatitisAlert.sharedInstance.showPopUp(self.view, completion: { (arrayTags) in
                if arrayTags.count > 0{
                    self.patient.adult.medicalQuestions[self.selectedIndex][cell.tag].answer = (arrayTags as NSArray).componentsJoined(by: ",")
                }else{
                    cell.radioButtonYes.isSelected = false
                    self.patient.adult.medicalQuestions[self.selectedIndex][cell.tag].selectedOption = false
                }

                }, showAlert: { (alertMessage) in
                    
            })
        }
    }
}

