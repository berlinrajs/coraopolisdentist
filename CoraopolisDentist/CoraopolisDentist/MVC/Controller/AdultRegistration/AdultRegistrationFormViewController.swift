//
//  AdultRegistrationFormViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/25/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AdultRegistrationFormViewController: PDViewController {

    ///SMILE ANALYSIS
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelPatientNumber : UILabel!
    @IBOutlet weak var radioSmile : RadioButton!
    @IBOutlet weak var radioTeeth : RadioButton!
    @IBOutlet var arraySmileButtons : [UIButton]!
    @IBOutlet weak var labelSmileOther : UILabel!
    @IBOutlet var arrayTeethButtons : [UIButton]!
    @IBOutlet weak var labelTeethOther : UILabel!
    @IBOutlet var arrayLifestyleButtons : [UIButton]!
    @IBOutlet weak var labelLifestyleOther : UILabel!
    @IBOutlet var arrayMakeoverButtons : [UIButton]!
    @IBOutlet weak var labelMakeoverOther : UILabel!
    @IBOutlet var arrayFamilyButtons : [UIButton]!
    @IBOutlet weak var labelFamilyOthers : UILabel!
    @IBOutlet var arrayAppointmentButtons : [UIButton]!
    @IBOutlet weak var labelAppointmentOther : UILabel!
    @IBOutlet  var labelUpcomingEvents : [UILabel]!
    @IBOutlet var arrayMusicButtons : [UIButton]!
    @IBOutlet weak var labelMusicOthers : UILabel!
    @IBOutlet  var labelHobbies : [UILabel]!
    @IBOutlet  var labelchildren : [UILabel]!
    @IBOutlet var labelKnowMore : [UILabel]!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!
    
    
    //DENTAL INSURANCE
    @IBOutlet weak var labelPatientName1 : UILabel!
    
    @IBOutlet weak var labelPrimaryCompanyName : UILabel!
    @IBOutlet weak var labelPrimaryCompanyPhone : UILabel!
    @IBOutlet weak var labelPrimaryAddress : UILabel!
    @IBOutlet weak var labelPrimaryGroupNumber : UILabel!
    @IBOutlet weak var labelPrimaryInsuredId : UILabel!
    @IBOutlet weak var labelPrimaryInsuredName : UILabel!
    @IBOutlet weak var labelPrimaryRelationship : UILabel!
    @IBOutlet weak var labelPrimaryDateOfBirth : UILabel!
    @IBOutlet weak var labelPrimarySocialSecurityNumber : UILabel!
    @IBOutlet weak var labelPrimaryEmployerName : UILabel!
    @IBOutlet weak var radioPrimaryPractice : RadioButton!
    
    @IBOutlet weak var labelSecondaryCompanyName : UILabel!
    @IBOutlet weak var labelSecondaryCompanyPhone : UILabel!
    @IBOutlet weak var labelSecondaryAddress : UILabel!
    @IBOutlet weak var labelSecondaryGroupNumber : UILabel!
    @IBOutlet weak var labelSecondaryInsuredId : UILabel!
    @IBOutlet weak var labelSecondaryInsuredName : UILabel!
    @IBOutlet weak var labelSecondaryRelationship : UILabel!
    @IBOutlet weak var labelSecondaryDateOfBirth : UILabel!
    @IBOutlet weak var labelSecondarySocialSecurityNumber : UILabel!
    @IBOutlet weak var labelSecondaryEmployerName : UILabel!
    @IBOutlet weak var radioSecondaryPractice : RadioButton!
    
    @IBOutlet weak var labelResponsibleName : UILabel!
    @IBOutlet weak var labelResponsibleRelationship : UILabel!
    @IBOutlet weak var labelResponsibleSocialSecurityNumber : UILabel!
    @IBOutlet weak var labelResponsiblePhone : UILabel!
    @IBOutlet weak var labelDriverLicense : UILabel!
    @IBOutlet weak var labelResponsibleDateOfBirth : UILabel!
    @IBOutlet weak var labelResponsibleAddress : UILabel!
    @IBOutlet weak var labelResponsibleEmployer : UILabel!
    @IBOutlet weak var labelResponsibleWorkNumber : UILabel!
    @IBOutlet weak var radioPaymentMethod : RadioButton!
    @IBOutlet weak var labelCardNumber : UILabel!
    @IBOutlet weak var labelExpDate : UILabel!
    @IBOutlet weak var labelGuardianDetails : UILabel!
    @IBOutlet weak var radioResponsiblePractice : RadioButton!
    
    @IBOutlet weak var imageviewSignaturePatient : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    
    @IBOutlet weak var labelEmergencyName : UILabel!
    @IBOutlet weak var labelEmergencyRelationship : UILabel!
    @IBOutlet weak var labelEmergencyCity : UILabel!
    @IBOutlet weak var labelEmergencyState : UILabel!
    @IBOutlet weak var labelEmergencyCellPhone : UILabel!
    @IBOutlet weak var labelEmergencyHomePhone : UILabel!
    @IBOutlet weak var labelEmergencyWorkPhone : UILabel!
    
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var imageviewOfficeInitial : UIImageView!
    
    
    
    ////ADULT REGISTRATION 1
    @IBOutlet weak var labelDate3 : UILabel!
    @IBOutlet weak var labelPatientNumber1 : UILabel!
    @IBOutlet weak var labelFirstName : UILabel!
    @IBOutlet weak var labelLastName : UILabel!
    @IBOutlet weak var labelMiddleInitial : UILabel!
    @IBOutlet weak var labelNickName : UILabel!
    @IBOutlet weak var radioGender : RadioButton!
    @IBOutlet weak var labelAddress : UILabel!
    @IBOutlet weak var labelCity : UILabel!
    @IBOutlet weak var labelState : UILabel!
    @IBOutlet weak var labelZip : UILabel!
    @IBOutlet weak var labelDateOfBirth : UILabel!
    @IBOutlet weak var labelSocialSecurityNumber : UILabel!
    @IBOutlet weak var labelHomeNumber : UILabel!
    @IBOutlet weak var labelWorkNumber : UILabel!
    @IBOutlet weak var labelCellNumber : UILabel!
    @IBOutlet weak var radioContactMethod : RadioButton!
    @IBOutlet weak var labelTimeToCall : UILabel!
    @IBOutlet weak var labelFax : UILabel!
    @IBOutlet weak var labelEmail : UILabel!
    @IBOutlet weak var labelDriverLicense1 : UILabel!
    @IBOutlet weak var labelEmployerName1 : UILabel!
    @IBOutlet weak var labelOccupation1 : UILabel!
    @IBOutlet weak var labelSpouseEmployer : UILabel!
    @IBOutlet weak var labelSpouseName : UILabel!
    @IBOutlet weak var labelReference : UILabel!
    @IBOutlet weak var labelSchoolName : UILabel!
    @IBOutlet weak var labelSchoolPhone : UILabel!
    @IBOutlet weak var labelGrade : UILabel!
    @IBOutlet weak var labelReasonTodayVisit : UILabel!
    @IBOutlet weak var labelPain : UILabel!
    @IBOutlet weak var labelDentalProblems : UILabel!
    @IBOutlet weak var labelDentalTreatment : UILabel!
    @IBOutlet weak var labelAnxiety : UILabel!
    @IBOutlet weak var labelLastDentalExam : UILabel!
    @IBOutlet weak var labelDentalCleaning : UILabel!
    @IBOutlet weak var labelDentalXrays : UILabel!
    @IBOutlet weak var labelDentalProcedures : UILabel!
    @IBOutlet weak var labelDentistName : UILabel!
    @IBOutlet weak var labelDentistCity : UILabel!
    @IBOutlet weak var labelDentistState : UILabel!
    @IBOutlet weak var labelDentistPhone : UILabel!
    @IBOutlet weak var labelChangingDentist : UILabel!
    @IBOutlet weak var labelDentalExaminations : UILabel!
    @IBOutlet weak var labelBrush : UILabel!
    @IBOutlet weak var labelFloss : UILabel!
    @IBOutlet weak var radioBristle : RadioButton!
    @IBOutlet weak var labelDentalAids : UILabel!
    @IBOutlet      var arrayDentalHistoryButtons : [RadioButton]!
    
    
    ///ADULT REGISTRATION 2
    @IBOutlet      var arrayDentalQuestions2 : [RadioButton]!
    @IBOutlet      var labelPreviousQuestion : [UILabel]!
    @IBOutlet      var dentalTreatmentsToKnow : [UILabel]!
    @IBOutlet      var arrayMedicalQuestions1 : [RadioButton]!
    @IBOutlet      var arrayMedicalQuestions2 : [RadioButton]!
    @IBOutlet      var arrayMedicalQuestions3 : [RadioButton]!
    @IBOutlet      var arrayMedicalQuestions4 : [RadioButton]!
    @IBOutlet      var arrayMedicalQuestions5 : [RadioButton]!
    @IBOutlet weak var labelHospitalized : UILabel!
    @IBOutlet weak var radioHospitalized : RadioButton!
    @IBOutlet weak var labelHospitalName : UILabel!
    @IBOutlet weak var hospitalCity : UILabel!
    @IBOutlet weak var hospitalState : UILabel!
    @IBOutlet weak var hospitalPhone : UILabel!
    @IBOutlet weak var labelMedications : UILabel!
    @IBOutlet weak var labelHeartProblems : UILabel!
    @IBOutlet weak var labelFenPhem : UILabel!
    @IBOutlet weak var labelMedicalCondition : UILabel!
    @IBOutlet weak var labelAllergyOther : UILabel!
    @IBOutlet      var arrayLabelHepatitis : [UILabel]!
    @IBOutlet weak var radioTobacco : RadioButton!
    @IBOutlet weak var radioAlcohol : RadioButton!
    @IBOutlet weak var radioPregnant : RadioButton!
    @IBOutlet weak var radioNursing : RadioButton!
    @IBOutlet weak var radioBirthControl : RadioButton!
    @IBOutlet weak var imageviewPatient : UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        smileAnalysis()
        dentalInsurance()
        AdultRegistration1()
        adultRegistration2()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    func smileAnalysis (){
        labelDate.text = patient.dateToday
        labelPatientNumber.text = patient.patientNumber
        radioSmile.setSelectedWithTag(patient.radioSmileTag)
        radioTeeth.setSelectedWithTag(patient.radioTeethTag)
        for btn in arraySmileButtons{
            btn.isSelected = patient.arraySmileTags.contains(btn.tag)
        }
        labelSmileOther.text = patient.smileOther
        for btn in arrayTeethButtons{
            btn.isSelected = patient.arrayTeethTags.contains(btn.tag)
        }
        labelTeethOther.text = patient.teethOther
        for btn in arrayLifestyleButtons{
            btn.isSelected = patient.arrayLifestyleTag.contains(btn.tag)
        }
        labelLifestyleOther.text = patient.lifestyleOther
        for btn in arrayMakeoverButtons{
            btn.isSelected = patient.arrayMakeOverTag.contains(btn.tag)
        }
        labelMakeoverOther.text = patient.makeOverOther
        for btn in arrayFamilyButtons{
            btn.isSelected = patient.arrayFamilyTag.contains(btn.tag)
        }
        labelFamilyOthers.text = patient.familyOther
        for btn in arrayAppointmentButtons{
            btn.isSelected = patient.arrayAppointmentTags.contains(btn.tag)
        }
        labelAppointmentOther.text = patient.appointmentOther
        patient.upcomingEvents.setTextForArrayOfLabels(labelUpcomingEvents)
        for btn in arrayMusicButtons{
            btn.isSelected = patient.arrayMusicTags.contains(btn.tag)
        }
        labelMusicOthers.text = patient.musicOther
        patient.hobbies.setTextForArrayOfLabels(labelHobbies)
        patient.childrens.setTextForArrayOfLabels(labelchildren)
        patient.knowMore.setTextForArrayOfLabels(labelKnowMore)
        labelPatientName.text = patient.fullName
        signaturePatient.image = patient.smileAnalysisSignature

    }
    
    func dentalInsurance() {
        labelPatientName1.text =  patient.fullName
        
        labelPrimaryCompanyName.text = patient.patientInformation.primaryCompanyName
        labelPrimaryCompanyPhone.text = patient.patientInformation.primaryCompanyNumber
        labelPrimaryAddress.text = getPrimaryAddress() == "PA" ? "N/A" : getPrimaryAddress()//patient.patientInformation.primaryAddress + ", " + patient.patientInformation.primaryCity + ", " + patient.patientInformation.primaryState + ", " + patient.patientInformation.primaryZipcode
        labelPrimaryGroupNumber.text = patient.patientInformation.primaryGroupNumber
        labelPrimaryInsuredId.text = patient.patientInformation.primaryInsuredId
        labelPrimaryInsuredName.text = patient.patientInformation.primaryName
        labelPrimaryRelationship.text = patient.patientInformation.primaryRelationship
        labelPrimaryDateOfBirth.text = patient.patientInformation.primaryDateOfBirth
        labelPrimarySocialSecurityNumber.text = patient.patientInformation.primarySocialSecurityNumber.socialSecurityNumber
        labelPrimaryEmployerName.text = patient.patientInformation.primaryEmployerName
        radioPrimaryPractice.setSelectedWithTag(patient.patientInformation.primaryPracticeTag)
        
        
        labelSecondaryCompanyName.text = patient.patientInformation.secondaryCompanyName
        labelSecondaryCompanyPhone.text = patient.patientInformation.secondaryCompanyNumber
        labelSecondaryAddress.text = getSecondaryAddress() == "PA" ? "N/A" : getSecondaryAddress()//patient.patientInformation.secondaryAddress + ", " + patient.patientInformation.secondaryCity + ", " + patient.patientInformation.secondaryState + ", " + patient.patientInformation.secondaryZipcode
        labelSecondaryGroupNumber.text =  patient.patientInformation.secondaryGroupNumber
        labelSecondaryInsuredId.text = patient.patientInformation.secondaryInsuredId
        labelSecondaryInsuredName.text = patient.patientInformation.secondaryName
        labelSecondaryRelationship.text = patient.patientInformation.secondaryRelationship
        labelSecondaryDateOfBirth.text = patient.patientInformation.secondaryDateOfBirth
        labelSecondarySocialSecurityNumber.text = patient.patientInformation.secondarySocialSecurityNumber.socialSecurityNumber
        labelSecondaryEmployerName.text = patient.patientInformation.secondaryEmployerName
        radioSecondaryPractice.setSelectedWithTag(patient.patientInformation.secondaryPracticeTag)
        
        
        labelResponsibleName.text = patient.patientInformation.responsibleName
        labelResponsibleRelationship.text = patient.patientInformation.responsibleRelationship
        labelResponsibleSocialSecurityNumber.text = patient.patientInformation.responsibleSocialSecurityNumber.socialSecurityNumber
        labelResponsiblePhone.text = patient.patientInformation.responsiblePhoneNUmber
        labelDriverLicense.text = patient.patientInformation.responsibleDriverLicense
        labelResponsibleDateOfBirth.text = patient.patientInformation.responsibleDateOfBirth
        labelResponsibleAddress.text = patient.patientInformation.responsibleAddress + ", " + patient.patientInformation.responsibleCity + ", " + patient.patientInformation.responsibleState + ", " +
            patient.patientInformation.responsibleZipcode
        labelResponsibleEmployer.text = patient.patientInformation.responsibleEmployerName
        labelResponsibleWorkNumber.text = patient.patientInformation.responsibleWOrkPhone
        radioPaymentMethod.setSelectedWithTag(patient.patientInformation.responsiblePaymentType)
        labelCardNumber.text = patient.patientInformation.responsibleCardNumber
        labelExpDate.text = patient.patientInformation.responsibleExpDate
        labelGuardianDetails.text = getGuardianDetails()//patient.patientInformation.legalGuardianName + ", " + patient.patientInformation.legalGuardianRelationship
        radioResponsiblePractice.setSelectedWithTag(patient.patientInformation.legalGuardianPractice)
        
        
        imageviewSignaturePatient.image = patient.patientInformation.signature
        labelDate1.text = patient.dateToday
        
        
        labelEmergencyName.text = patient.patientInformation.emergencyName
        labelEmergencyRelationship.text = patient.patientInformation.emergencyRelationship
        labelEmergencyCity.text = patient.patientInformation.emergencyCity
        labelEmergencyState.text = patient.patientInformation.emergencyState
        labelEmergencyCellPhone.text = patient.patientInformation.emergencyCellNumber
        labelEmergencyHomePhone.text = patient.patientInformation.emergencyHomeNumber
        labelEmergencyWorkPhone.text = patient.patientInformation.emergencyWorkNumber
        
        
        labelDate2.text = patient.patientInformation.signatureOfficial == nil ? "" : patient.dateToday
        imageviewOfficeInitial.image = patient.patientInformation.signatureOfficial

    }
    
    func getGuardianDetails() -> String {
        var array : [String] = [String]()
        if patient.patientInformation.legalGuardianName != "N/A"{
            array.append(" \(patient.patientInformation.legalGuardianName)")
        }
        if patient.patientInformation.legalGuardianRelationship != "N/A"{
            array.append(" \(patient.patientInformation.legalGuardianRelationship)")
        }

        return array.count == 0 ? "N/A" : (array as NSArray).componentsJoined(by: ",")

    }
    func getPrimaryAddress() -> String {
        var array : [String] = [String]()
        if patient.patientInformation.primaryAddress != "N/A"{
            array.append(" \(patient.patientInformation.primaryAddress)")
        }
        if patient.patientInformation.primaryCity != "N/A"{
            array.append(" \(patient.patientInformation.primaryCity)")
        }
        if patient.patientInformation.primaryState != "N/A"{
            array.append(" \(patient.patientInformation.primaryState)")
        }
        if patient.patientInformation.primaryZipcode != "N/A"{
            array.append(" \(patient.patientInformation.primaryZipcode)")
        }
        return (array as NSArray).componentsJoined(by: ",")

    }
    
    
    func getSecondaryAddress() -> String {
        var array : [String] = [String]()
        if patient.patientInformation.secondaryAddress != "N/A"{
            array.append(" \(patient.patientInformation.secondaryAddress)")
        }
        if patient.patientInformation.secondaryCity != "N/A"{
            array.append(" \(patient.patientInformation.secondaryCity)")
        }
        if patient.patientInformation.secondaryState != "N/A"{
            array.append(" \(patient.patientInformation.secondaryState)")
        }
        if patient.patientInformation.secondaryZipcode != "N/A"{
            array.append(" \(patient.patientInformation.secondaryZipcode)")
        }
        return (array as NSArray).componentsJoined(by: ",")
        
    }

    func AdultRegistration1()  {
        labelDate3.text = patient.dateToday
        labelPatientNumber1.text = patient.patientNumber
        labelFirstName.text = patient.firstName
        labelLastName.text = patient.lastName
        labelMiddleInitial.text = patient.middleInitial
        labelNickName.text = patient.adult.nickName
        radioGender.setSelectedWithTag(patient.adult.radioGenderTag)
        labelAddress.text = patient.adult.address
        labelCity.text = patient.adult.city
        labelState.text = patient.adult.state
        labelZip.text = patient.adult.zipcode
        labelDateOfBirth.text = patient.dateOfBirth
        labelSocialSecurityNumber.text = patient.adult.socialSecurityNumber.socialSecurityNumber
        labelHomeNumber.text = patient.adult.homeNumber
        labelWorkNumber.text = patient.adult.workNumber
        labelCellNumber.text = patient.adult.cellNumber
        radioContactMethod.setSelectedWithTag(patient.adult.radioContactTag)
        labelTimeToCall.text = patient.adult.timeToCall
        labelFax.text = patient.adult.faxNumber
        labelEmail.text = patient.adult.email
        labelDriverLicense1.text = patient.adult.driverLicense
        labelEmployerName1.text = patient.adult.employerName
        labelOccupation1.text = patient.adult.occupation
        labelSpouseEmployer.text = patient.adult.spouseEmployer
        labelSpouseName.text = patient.adult.spouseName
        labelReference.text = patient.adult.reference
        labelSchoolName.text = patient.adult.schoolName
        labelSchoolPhone.text = patient.adult.schoolPhone
        labelGrade.text = patient.adult.grade
        labelReasonTodayVisit.text = patient.adult.reasonTodayVisit
        labelPain.text = patient.adult.dentalQuestions[0][0].answer
        labelDentalProblems.text = patient.adult.dentalQuestions[0][1].answer
        labelDentalTreatment.text = patient.adult.dentalQuestions[0][2].answer
        labelAnxiety.text = patient.adult.radioAnxietyTag == 0 ? "N/A" : "\(patient.adult.radioAnxietyTag)"
        labelLastDentalExam.text = patient.adult.lastDentalExam
        labelDentalCleaning.text = patient.adult.lastDentalCleaning
        labelDentalXrays.text = patient.adult.lastDentalXray
        labelDentalProcedures.text = patient.adult.dentalProcedure
        labelDentistName.text = patient.adult.dentistName
        labelDentistCity.text = patient.adult.dentistCity
        labelDentistState.text = patient.adult.dentistState
        labelDentistPhone.text = patient.adult.dentistPhone
        labelChangingDentist.text = patient.adult.dentistReasonToChange
        labelDentalExaminations.text = patient.adult.dentalExamination
        labelBrush.text = patient.adult.brush
        labelFloss.text = patient.adult.floss
        radioBristle.setSelectedWithTag(patient.adult.bristleTag)
        labelDentalAids.text = patient.adult.dentalAids
        for btn in arrayDentalHistoryButtons{
            btn.isSelected = patient.adult.dentalQuestions[0][btn.tag].selectedOption!
        }
    }
    
    
    func adultRegistration2(){
        for btn in arrayDentalQuestions2{
            btn.isSelected = patient.adult.dentalQuestions[1][btn.tag].selectedOption!
        }
        
        patient.adult.dentalHistoryDetails.setTextForArrayOfLabels(labelPreviousQuestion)
        patient.adult.dentalQuestions[1][8].answer?.setTextForArrayOfLabels(dentalTreatmentsToKnow)
        for btn in arrayMedicalQuestions1{
            btn.isSelected = patient.adult.medicalQuestions[0][btn.tag].selectedOption!
        }
        for btn in arrayMedicalQuestions2{
            btn.isSelected = patient.adult.medicalQuestions[1][btn.tag].selectedOption!
        }
        for btn in arrayMedicalQuestions3{
            btn.isSelected = patient.adult.medicalQuestions[2][btn.tag].selectedOption!
        }
        for btn in arrayMedicalQuestions4{
            btn.isSelected = patient.adult.medicalQuestions[3][btn.tag].selectedOption!
        }
        for btn in arrayMedicalQuestions5{
            btn.isSelected = patient.adult.allergyQuestions[btn.tag].selectedOption!
        }

        labelHospitalized.text = patient.adult.hospitalizedReason
        radioHospitalized.setSelectedWithTag(patient.adult.radioHospitalizedTag)
        labelHospitalName.text = patient.adult.hospitalName
        hospitalCity.text = patient.adult.hospitalCity
        hospitalState.text = patient.adult.hospitalState
        hospitalPhone.text = patient.adult.hospitalPhone
        labelMedications.text = patient.adult.dentalQuestions[0][12].answer
        labelHeartProblems.text = patient.adult.dentalQuestions[1][0].answer
        labelFenPhem.text = patient.adult.dentalQuestions[0][13].answer
        labelMedicalCondition.text = patient.adult.medicalQuestions[3][13].answer
        labelAllergyOther.text = patient.adult.allergyQuestions[11].answer
        if patient.adult.medicalQuestions[2][2].answer != nil{
        let arrayTags = patient.adult.medicalQuestions[2][2].answer!.components(separatedBy: ",")
        for lbl in arrayLabelHepatitis{
            if arrayTags.contains("\(lbl.tag)"){
                lbl.layer.borderColor = UIColor.black.cgColor
                lbl.layer.borderWidth = 1.0
                lbl.layer.cornerRadius = lbl.frame.size.width/2
                lbl.clipsToBounds = true
                
            }
        }
        }
        
        radioTobacco.setSelectedWithTag(patient.adult.radioTobaccoTag)
        radioAlcohol.setSelectedWithTag(patient.adult.radioAlcoholTag)
        radioPregnant.setSelectedWithTag(patient.adult.radioPregnantTag)
        radioNursing.setSelectedWithTag(patient.adult.radioNursingTag)
        radioBirthControl.setSelectedWithTag(patient.adult.radioBirthControlTag)
        imageviewPatient.image = patient.patientInformation.signature

    }
    
    
    override func buttonActionSubmit(withSender sender: UIButton) {
        #if AUTO
            if !Reachability.isConnectedToNetwork() {
                let alertController = UIAlertController(title: "Coraopolis Dentist", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
                let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url as URL)
                    }
                }
                let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    
                }
                alertController.addAction(alertOkAction)
                alertController.addAction(alertCancelAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            BRProgressHUD.show()
            ServiceManager.sendAdultDetails(patient: patient, completion: { (success, error) in
                BRProgressHUD.hide()
                if success {
                    super.buttonActionSubmit(withSender: sender)
                } else {
                    let alert = Extention.alert(error!.localizedDescription.uppercased())
                    self.present(alert, animated: true, completion: nil)
                }
            })
        #else
            super.buttonActionSubmit(withSender: sender)
        #endif
    }
}
