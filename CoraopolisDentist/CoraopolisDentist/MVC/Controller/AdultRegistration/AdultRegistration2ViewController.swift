//
//  AdultRegistration2ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AdultRegistration2ViewController: PDViewController {
    @IBOutlet weak var radioContact : RadioButton!
    @IBOutlet weak var textfieldTimeToCall : UITextField!
    @IBOutlet weak var textfieldHomeNumber : PDTextField!
    @IBOutlet weak var textfieldWorkNumber : UITextField!
    @IBOutlet weak var textfieldCellNumber : UITextField!
    @IBOutlet weak var textfieldFaxNumber : UITextField!
    @IBOutlet weak var textfieldLicense : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        loadValue()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        DateInputView.addTimePickerForTextField(textfieldTimeToCall)
        textfieldTimeToCall.setSavedText(patient.adult.timeToCall)
        textfieldFaxNumber.setSavedText(patient.adult.faxNumber)
        textfieldLicense.setSavedText(patient.adult.driverLicense)
        checkContactMandatoryFields(patient.adult.radioContactTag)
        
        func load() {
            textfieldHomeNumber.setSavedText(patient.adult.homeNumber)
            textfieldWorkNumber.setSavedText(patient.adult.workNumber)
            textfieldCellNumber.setSavedText(patient.adult.cellNumber)
            radioContact.setSelectedWithTag(patient.adult.radioContactTag)
        }
        
        #if AUTO
            if let patientDetails = patient.patientDetails {
                textfieldHomeNumber.text = patient.adult.homeNumber == "N/A" ? patientDetails.homePhone : patient.adult.homeNumber
                textfieldWorkNumber.text = patient.adult.workNumber == "N/A" ? patientDetails.workPhone : patient.adult.workNumber
                textfieldCellNumber.text = patient.adult.cellNumber == "N/A" ? patientDetails.cellPhone : patient.adult.cellNumber
                radioContact.setSelectedWithTag(patient.adult.radioContactTag == 0 ? !textfieldHomeNumber.isEmpty ? 1 : !textfieldWorkNumber.isEmpty ? 2 : 3 : patient.adult.radioContactTag)

            } else {
                load()
            }
        #else
            load()
        #endif
    }
    
    func saveValue()  {
        patient.adult.timeToCall = textfieldTimeToCall.getText()
        patient.adult.homeNumber = textfieldHomeNumber.getText()
        patient.adult.workNumber = textfieldWorkNumber.getText()
        patient.adult.cellNumber = textfieldCellNumber.getText()
        patient.adult.faxNumber = textfieldFaxNumber.getText()
        patient.adult.driverLicense = textfieldLicense.getText()
        patient.adult.radioContactTag = radioContact.selected == nil ? 0 : radioContact.selected.tag
    }
    
    @IBAction func onContactMethodButtonPressed (withSender sender : RadioButton){
        checkContactMandatoryFields(sender.tag)
    }
    
    func checkContactMandatoryFields(_ tag : Int)  {
        if tag == 1{
            textfieldHomeNumber.placeholder = "HOME PHONE NUMBER *"
            textfieldWorkNumber.placeholder = "WORK PHONE NUMBER"
            textfieldCellNumber.placeholder = "CELL PHONE NUMBER"
        }else if tag == 2{
            textfieldHomeNumber.placeholder = "HOME PHONE NUMBER"
            textfieldWorkNumber.placeholder = "WORK PHONE NUMBER *"
            textfieldCellNumber.placeholder = "CELL PHONE NUMBER"
        }else if tag == 3{
            textfieldHomeNumber.placeholder = "HOME PHONE NUMBER"
            textfieldWorkNumber.placeholder = "WORK PHONE NUMBER"
            textfieldCellNumber.placeholder = "CELL PHONE NUMBER *"

        }else{
            textfieldHomeNumber.placeholder = "HOME PHONE NUMBER"
            textfieldWorkNumber.placeholder = "WORK PHONE NUMBER"
            textfieldCellNumber.placeholder = "CELL PHONE NUMBER"
            
        }

    }
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        
        if radioContact.selected == nil {
            self.showAlert("PLEASE SELECT THE PRIMARY CONTACT NUMBER")
        }else if (radioContact.selected.tag == 1 && textfieldHomeNumber.isEmpty) || (radioContact.selected.tag == 2 && textfieldWorkNumber.isEmpty) || (radioContact.selected.tag == 3 && textfieldCellNumber.isEmpty){
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if !textfieldHomeNumber.isEmpty && !textfieldHomeNumber.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID HOME PHONE NUMBER")
        }else if !textfieldWorkNumber.isEmpty && !textfieldWorkNumber.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID WORK PHONE NUMBER")
        }else if !textfieldCellNumber.isEmpty && !textfieldCellNumber.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID CELL PHONE NUMBER")
        }else if !textfieldFaxNumber.isEmpty && !textfieldFaxNumber.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID FAX NUMBER")
        }else{
            saveValue()
            let adult = adultStoryboard.instantiateViewController(withIdentifier: "Adult3VC") as! AdultRegistration3ViewController
            adult.patient = self.patient
            self.navigationController?.pushViewController(adult, animated: true)
        }
        
    }

}

extension AdultRegistration2ViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldHomeNumber || textField == textfieldWorkNumber || textField == textfieldCellNumber || textField == textfieldFaxNumber{
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

