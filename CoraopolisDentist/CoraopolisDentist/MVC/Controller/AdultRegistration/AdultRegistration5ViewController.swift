//
//  AdultRegistration5ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AdultRegistration5ViewController: PDViewController {
    
    @IBOutlet weak var radioAnxiety : RadioButton!
    @IBOutlet weak var textfieldReason : UITextField!
    @IBOutlet weak var textfieldDentalExam : UITextField!
    @IBOutlet weak var textfieldDentalCleaning : UITextField!
    @IBOutlet weak var textfieldXrays : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        DateInputView.addDatePickerForTextField(textfieldDentalExam)
        DateInputView.addDatePickerForTextField(textfieldXrays)
        DateInputView.addDatePickerForTextField(textfieldDentalCleaning)
        radioAnxiety.setSelectedWithTag(patient.adult.radioAnxietyTag)
        textfieldReason.setSavedText(patient.adult.reasonTodayVisit)
        textfieldDentalExam.setSavedText(patient.adult.lastDentalExam)
        textfieldDentalCleaning.setSavedText(patient.adult.lastDentalCleaning)
        textfieldXrays.setSavedText(patient.adult.lastDentalXray)
    }
    
    func saveValue()  {
        patient.adult.reasonTodayVisit = textfieldReason.getText()
        patient.adult.lastDentalExam = textfieldDentalExam.getText()
        patient.adult.lastDentalCleaning = textfieldDentalCleaning.getText()
        patient.adult.lastDentalXray = textfieldXrays.getText()
        patient.adult.radioAnxietyTag = radioAnxiety.selected == nil ? 0 : radioAnxiety.selected.tag
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        saveValue()
        let adult = adultStoryboard.instantiateViewController(withIdentifier: "Adult6VC") as! AdultRegistration6ViewController
        adult.patient = self.patient
        self.navigationController?.pushViewController(adult, animated: true)
    }
    
    
}

extension AdultRegistration5ViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

