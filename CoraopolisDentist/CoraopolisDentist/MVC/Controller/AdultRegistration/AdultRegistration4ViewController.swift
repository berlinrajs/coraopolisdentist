//
//  AdultRegistration4ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AdultRegistration4ViewController: PDViewController {
    @IBOutlet weak var textfieldReference : UITextField!
    @IBOutlet weak var textfieldSchoolName : UITextField!
    @IBOutlet weak var textfieldSchoolPhone : UITextField!
    @IBOutlet weak var textfieldGrade : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loadValue()  {
        textfieldReference.setSavedText(patient.adult.reference)
        textfieldSchoolName.setSavedText(patient.adult.schoolName)
        textfieldSchoolPhone.setSavedText(patient.adult.schoolPhone)
        textfieldGrade.setSavedText(patient.adult.grade)
    }
    
    func saveValue()  {
        patient.adult.reference = textfieldReference.getText()
        patient.adult.schoolName = textfieldSchoolName.getText()
        patient.adult.schoolPhone = textfieldSchoolPhone.getText()
        patient.adult.grade = textfieldGrade.getText()
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldSchoolPhone.isEmpty && !textfieldSchoolPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID PHONE NUMBER")
        }else{
        saveValue()
        let adult = adultStoryboard.instantiateViewController(withIdentifier: "Adult5VC") as! AdultRegistration5ViewController
        adult.patient = self.patient
        self.navigationController?.pushViewController(adult, animated: true)
        }
    }

}

extension AdultRegistration4ViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldSchoolPhone {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

