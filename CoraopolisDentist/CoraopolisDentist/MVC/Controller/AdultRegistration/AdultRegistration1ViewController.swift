//
//  AdultRegistration1ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/21/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AdultRegistration1ViewController: PDViewController {
    
    @IBOutlet weak var textfieldNickname : UITextField!
    @IBOutlet weak var textfieldAddress : UITextField!
    @IBOutlet weak var textfieldCity : UITextField!
    @IBOutlet weak var textfieldState : UITextField!
    @IBOutlet weak var textfieldZipcode : UITextField!
    @IBOutlet weak var textfieldSsn : UITextField!
    @IBOutlet weak var textfieldEmail : UITextField!
    @IBOutlet weak var dropdownGender : BRDropDown!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        StateListView.addStateListForTextField(textfieldState)
        dropdownGender.items = ["MALE","FEMALE"]
        dropdownGender.placeholder = "-- SELECT GENDER * --"
        loadValue()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        
        textfieldNickname.setSavedText(patient.adult.nickName)
        
        func load() {
            textfieldAddress.setSavedText(patient.adult.address)
            textfieldCity.setSavedText(patient.adult.city)
            textfieldState.setSavedText(patient.adult.state)
            textfieldZipcode.setSavedText(patient.adult.zipcode)
            textfieldSsn.setSavedText(patient.adult.socialSecurityNumber)
            textfieldEmail.setSavedText(patient.adult.email)
            dropdownGender.selectedIndex = patient.adult.radioGenderTag
        }
        
        #if AUTO
            if let patientDetails = patient.patientDetails {
                textfieldAddress.text = patient.adult.address == "N/A" ? patientDetails.address : patient.adult.address
                textfieldCity.text = patient.adult.city == "N/A" ? patientDetails.city : patient.adult.city
                textfieldState.text = patient.adult.state == "N/A" ? patientDetails.state : patient.adult.state
                textfieldZipcode.text = patient.adult.zipcode == "N/A" ? patientDetails.zipCode : patient.adult.zipcode
                textfieldSsn.text = patient.adult.socialSecurityNumber == "N/A" ? patientDetails.socialSecurityNumber : patient.adult.socialSecurityNumber
                textfieldEmail.text = patient.adult.email == "N/A" ? patientDetails.email : patient.adult.email
                if let gender = patientDetails.gender, patient.adult.radioGenderTag == 0 {
                    dropdownGender.selectedIndex = gender.index
                } else {
                    dropdownGender.selectedIndex = patient.adult.radioGenderTag
                }
            } else {
                load()
            }
        #else
            load()
        #endif
        
        
        
        
    }
    
    func saveValue()  {
        patient.adult.nickName = textfieldNickname.getText()
        patient.adult.address = textfieldAddress.getText()
        patient.adult.city = textfieldCity.getText()
        patient.adult.state = textfieldState.getText()
        patient.adult.zipcode = textfieldZipcode.getText()
        patient.adult.socialSecurityNumber = textfieldSsn.getText()
        patient.adult.email = textfieldEmail.getText()
        patient.adult.radioGenderTag = dropdownGender.selectedIndex
        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textfieldAddress.isEmpty || textfieldCity.isEmpty || textfieldState.isEmpty || textfieldZipcode.isEmpty || dropdownGender.selectedOption == nil{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if !textfieldZipcode.text!.isZipCode{
            self.showAlert("PLEASE ENTER THE VALID ZIPCODE")
        }else if !textfieldSsn.isEmpty && textfieldSsn.text!.characters.count != 9{
            self.showAlert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
        }else if !textfieldEmail.isEmpty && !textfieldEmail.text!.isValidEmail{
            self.showAlert("PLEASE ENTER THE VALID EMAIL")
        }else{
            saveValue()
            let adult = adultStoryboard.instantiateViewController(withIdentifier: "Adult2VC") as! AdultRegistration2ViewController
            adult.patient = self.patient
            self.navigationController?.pushViewController(adult, animated: true)
            
        }
        
    }
    
    
    
}

extension AdultRegistration1ViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldZipcode {
            return textField.formatZipCode(range, string: string)
        }else if textField == textfieldSsn{
            return textField.formatNumbers(range, string: string, count: 9)
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

