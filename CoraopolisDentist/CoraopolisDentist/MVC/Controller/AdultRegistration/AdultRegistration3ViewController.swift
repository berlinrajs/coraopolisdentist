//
//  AdultRegistration3ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AdultRegistration3ViewController: PDViewController {
    
    @IBOutlet weak var textfieldEmployerName : UITextField!
    @IBOutlet weak var textfieldOccupation : UITextField!
    @IBOutlet weak var textfieldSpouseName : UITextField!
    @IBOutlet weak var textfieldSpouseEmployer : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        textfieldEmployerName.setSavedText(patient.adult.employerName)
        textfieldOccupation.setSavedText(patient.adult.occupation)
        textfieldSpouseName.setSavedText(patient.adult.spouseName)
        textfieldSpouseEmployer.setSavedText(patient.adult.spouseEmployer)
    }
    
    func saveValue()  {
        patient.adult.employerName = textfieldEmployerName.getText()
        patient.adult.occupation = textfieldOccupation.getText()
        patient.adult.spouseName = textfieldSpouseName.getText()
        patient.adult.spouseEmployer = textfieldSpouseEmployer.getText()
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        saveValue()
        let adult = adultStoryboard.instantiateViewController(withIdentifier: "Adult4VC") as! AdultRegistration4ViewController
        adult.patient = self.patient
        self.navigationController?.pushViewController(adult, animated: true)
        
    }
    
    
    
}

extension AdultRegistration3ViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
