//
//  SmileAnalysisFormViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 8/30/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SmileAnalysisFormViewController: PDViewController {
    
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelPatientNumber : UILabel!
    @IBOutlet weak var radioSmile : RadioButton!
    @IBOutlet weak var radioTeeth : RadioButton!
    @IBOutlet var arraySmileButtons : [UIButton]!
    @IBOutlet weak var labelSmileOther : UILabel!
    @IBOutlet var arrayTeethButtons : [UIButton]!
    @IBOutlet weak var labelTeethOther : UILabel!
    @IBOutlet var arrayLifestyleButtons : [UIButton]!
    @IBOutlet weak var labelLifestyleOther : UILabel!
    @IBOutlet var arrayMakeoverButtons : [UIButton]!
    @IBOutlet weak var labelMakeoverOther : UILabel!
    @IBOutlet var arrayFamilyButtons : [UIButton]!
    @IBOutlet weak var labelFamilyOthers : UILabel!
    @IBOutlet var arrayAppointmentButtons : [UIButton]!
    @IBOutlet weak var labelAppointmentOther : UILabel!
    @IBOutlet  var labelUpcomingEvents : [UILabel]!
    @IBOutlet var arrayMusicButtons : [UIButton]!
    @IBOutlet weak var labelMusicOthers : UILabel!
    @IBOutlet  var labelHobbies : [UILabel]!
    @IBOutlet  var labelchildren : [UILabel]!
    @IBOutlet var labelKnowMore : [UILabel]!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.text = patient.dateToday
        labelPatientNumber.text = patient.patientNumber
        radioSmile.setSelectedWithTag(patient.radioSmileTag)
        radioTeeth.setSelectedWithTag(patient.radioTeethTag)
        for btn in arraySmileButtons{
            btn.isSelected = patient.arraySmileTags.contains(btn.tag)
        }
        labelSmileOther.text = patient.smileOther
        for btn in arrayTeethButtons{
            btn.isSelected = patient.arrayTeethTags.contains(btn.tag)
        }
        labelTeethOther.text = patient.teethOther
        for btn in arrayLifestyleButtons{
            btn.isSelected = patient.arrayLifestyleTag.contains(btn.tag)
        }
        labelLifestyleOther.text = patient.lifestyleOther
        for btn in arrayMakeoverButtons{
            btn.isSelected = patient.arrayMakeOverTag.contains(btn.tag)
        }
        labelMakeoverOther.text = patient.makeOverOther
        for btn in arrayFamilyButtons{
            btn.isSelected = patient.arrayFamilyTag.contains(btn.tag)
        }
        labelFamilyOthers.text = patient.familyOther
        for btn in arrayAppointmentButtons{
            btn.isSelected = patient.arrayAppointmentTags.contains(btn.tag)
        }
        labelAppointmentOther.text = patient.appointmentOther
        patient.upcomingEvents.setTextForArrayOfLabels(labelUpcomingEvents)
        for btn in arrayMusicButtons{
            btn.isSelected = patient.arrayMusicTags.contains(btn.tag)
        }
        labelMusicOthers.text = patient.musicOther
        patient.hobbies.setTextForArrayOfLabels(labelHobbies)
        patient.childrens.setTextForArrayOfLabels(labelchildren)
        patient.knowMore.setTextForArrayOfLabels(labelKnowMore)
        labelPatientName.text = patient.fullName
        signaturePatient.image = patient.smileAnalysisSignature
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
