//
//  SmileAnalysis3ViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 8/29/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SmileAnalysis3ViewController: PDViewController {
    
    @IBOutlet weak var textviewHobbies : UITextView!
    @IBOutlet weak var textviewKnowmore : UITextView!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate : DateLabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
        // Do any additional setup after loading the view.
        loadValue()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        textviewHobbies.text = patient.hobbies == "N/A" ? "PLEASE SPECIFY" : patient.hobbies
        textviewHobbies.textColor = patient.hobbies == "N/A" ? UIColor.lightGray : UIColor.black
        textviewKnowmore.text = patient.knowMore == "N/A" ? "PLEASE SPECIFY" : patient.knowMore
        textviewKnowmore.textColor = patient.knowMore == "N/A" ? UIColor.lightGray : UIColor.black

    }
    
    func saveValue()  {
        patient.hobbies = textviewHobbies.text! == "PLEASE SPECIFY" ? "N/A" : textviewHobbies.text!
        patient.knowMore = textviewKnowmore.text! == "PLEASE SPECIFY" ? "N/A" : textviewKnowmore.text!

    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
            
        }else if !labelDate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        }else{
            patient.smileAnalysisSignature = signaturePatient.signatureImage()
            saveValue()
            let adult = adultStoryboard.instantiateViewController(withIdentifier: "AdultFormVC") as! AdultRegistrationFormViewController
            adult.patient = self.patient
            self.navigationController?.pushViewController(adult, animated: true)

        }
        
    }
    
}


extension SmileAnalysis3ViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "PLEASE SPECIFY" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "PLEASE SPECIFY"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
