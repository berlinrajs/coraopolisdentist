//
//  AdultRegistration6ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AdultRegistration6ViewController: PDViewController {

    @IBOutlet weak var textfieldProcedure : UITextField!
    @IBOutlet weak var textfieldDentistName : UITextField!
    @IBOutlet weak var textfieldDentistCity : UITextField!
    @IBOutlet weak var textfieldDentistState : UITextField!
    @IBOutlet weak var textfieldDentistPhone : UITextField!
    @IBOutlet weak var textfieldReasonToChange : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        StateListView.addStateListForTextField(textfieldDentistState)
        textfieldProcedure.setSavedText(patient.adult.dentalProcedure)
        textfieldDentistName.setSavedText(patient.adult.dentistName)
        textfieldDentistCity.setSavedText(patient.adult.dentistCity)
        textfieldDentistState.setSavedText(patient.adult.dentistState)
        textfieldDentistPhone.setSavedText(patient.adult.dentistPhone)
        textfieldReasonToChange.setSavedText(patient.adult.dentistReasonToChange)

    }
    
    func saveValue()  {
        patient.adult.dentalProcedure = textfieldProcedure.getText()
        patient.adult.dentistName = textfieldDentistName.getText()
        patient.adult.dentistCity = textfieldDentistCity.getText()
        patient.adult.dentistState = textfieldDentistState.getText()
        patient.adult.dentistPhone = textfieldDentistPhone.getText()
        patient.adult.dentistReasonToChange = textfieldReasonToChange.getText()
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldDentistPhone.isEmpty && !textfieldDentistPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID PHONE NUMBER")
        }else{
        saveValue()
        let adult = adultStoryboard.instantiateViewController(withIdentifier: "Adult7VC") as! AdultRegistration7ViewController
        adult.patient = self.patient
        self.navigationController?.pushViewController(adult, animated: true)
        }
    }
}

extension AdultRegistration6ViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldDentistPhone {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
