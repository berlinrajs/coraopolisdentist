//
//  AdultRegistration7ViewController.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AdultRegistration7ViewController: PDViewController {

    @IBOutlet weak var dropDownBrush : BRDropDown!
    @IBOutlet weak var dropDownFloss : BRDropDown!
    @IBOutlet weak var dropDownBristle : BRDropDown!
    @IBOutlet weak var textfieldExamination : UITextField!
    @IBOutlet weak var textfieldAids : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        dropDownBrush.items = ["ONCE A DAY","TWICE A DAY","THRICE A DAY","I DON'T BRUSH"]
        dropDownFloss.items = ["ONCE A DAY","TWICE A WEEK","OCCASSIONALLY","I DON'T FLOSS"]
        dropDownBristle.items = ["HARD","MEDIUM","SOFT"]
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        textfieldExamination.setSavedText(patient.adult.dentalExamination)
        textfieldAids.setSavedText(patient.adult.dentalAids)
        dropDownBrush.selectedIndex = patient.adult.brushTag
        dropDownBristle.selectedIndex = patient.adult.bristleTag
        dropDownFloss.selectedIndex = patient.adult.flossTag
        
    }
    
    func saveValue()  {
        patient.adult.dentalExamination = textfieldExamination.getText()
        patient.adult.brush = dropDownBrush.selectedOption == nil ? "N/A" : dropDownBrush.selectedOption!
        patient.adult.brushTag = dropDownBrush.selectedIndex
        patient.adult.floss = dropDownFloss.selectedOption == nil ? "N/A" : dropDownFloss.selectedOption!
        patient.adult.flossTag = dropDownFloss.selectedIndex
        patient.adult.bristleTag = dropDownBristle.selectedIndex
        patient.adult.dentalAids = textfieldAids.getText()

        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
            saveValue()
            let adult = adultStoryboard.instantiateViewController(withIdentifier: "Adult8VC") as! AdultRegistration8ViewController
            adult.patient = self.patient
            self.navigationController?.pushViewController(adult, animated: true)
    }


}

extension AdultRegistration7ViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
