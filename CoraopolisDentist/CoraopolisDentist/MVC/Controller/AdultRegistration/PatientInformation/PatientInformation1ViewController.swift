//
//  PatientInformation1ViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 8/31/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientInformation1ViewController: PDViewController {

    @IBOutlet weak var radioInsurance : RadioButton!
    @IBOutlet weak var radioSelf : RadioButton!
    @IBOutlet weak var textfieldInsuranceCompanyName : UITextField!
    @IBOutlet weak var textfieldInsuranceCompanyPhone : UITextField!
    @IBOutlet weak var textfieldAddress : UITextField!
    @IBOutlet weak var textfieldCity : UITextField!
    @IBOutlet weak var textfieldState : UITextField!
    @IBOutlet weak var textfieldZipcode : UITextField!
    @IBOutlet weak var textfieldGroupNumber : UITextField!
    @IBOutlet weak var textfieldInsuredId : UITextField!
    @IBOutlet weak var heightInsurance : NSLayoutConstraint!
    @IBOutlet weak var heightSelf : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        StateListView.addStateListForTextField(textfieldState)
        heightSelf.constant = patient.adult.address != "" ? 38 : 0
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        radioInsurance.setSelectedWithTag(patient.patientInformation.radioPrimaryInsuranceTag)
        radioSelf.setSelectedWithTag(patient.patientInformation.radioPrimarySelfTag)
        if radioInsurance.selected.tag == 1{
            heightInsurance.constant = 551
            textfieldInsuranceCompanyName.setSavedText(patient.patientInformation.primaryCompanyName)
            textfieldInsuranceCompanyPhone.setSavedText(patient.patientInformation.primaryCompanyNumber)
            textfieldGroupNumber.setSavedText(patient.patientInformation.primaryGroupNumber)
            textfieldInsuredId.setSavedText(patient.patientInformation.primaryInsuredId)
            textfieldAddress.setSavedText(patient.patientInformation.primaryAddress)
            textfieldCity.setSavedText(patient.patientInformation.primaryCity)
            textfieldState.setSavedText(patient.patientInformation.primaryState)
            textfieldZipcode.setSavedText(patient.patientInformation.primaryZipcode)

        }else{
            heightInsurance.constant = 0
        
        }
    }
    
    func saveValue()  {
        patient.patientInformation.primaryCompanyName = textfieldInsuranceCompanyName.isEmpty ? "N/A" : textfieldInsuranceCompanyName.text!
        patient.patientInformation.primaryCompanyNumber = textfieldInsuranceCompanyPhone.isEmpty ? "N/A" : textfieldInsuranceCompanyPhone.text!
        patient.patientInformation.primaryGroupNumber  = textfieldGroupNumber.isEmpty ? "N/A" : textfieldGroupNumber.text!
        patient.patientInformation.primaryInsuredId = textfieldInsuredId.isEmpty ? "N/A" : textfieldInsuredId.text!
        patient.patientInformation.primaryAddress = textfieldAddress.isEmpty ? "N/A" : textfieldAddress.text!
        patient.patientInformation.primaryCity = textfieldCity.isEmpty ? "N/A" : textfieldCity.text!
        patient.patientInformation.primaryState = textfieldState.isEmpty ? "N/A" : textfieldState.text!
        patient.patientInformation.primaryZipcode = textfieldZipcode.isEmpty ? "N/A" : textfieldZipcode.text!
        patient.patientInformation.radioPrimaryInsuranceTag = radioInsurance.selected.tag
        patient.patientInformation.radioPrimarySelfTag = radioSelf.selected.tag

    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    

    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if radioInsurance.selected.tag == 1{
//        if textfieldInsuranceCompanyName.isEmpty || textfieldAddress.isEmpty || textfieldCity.isEmpty || textfieldState.isEmpty || textfieldZipcode.isEmpty || textfieldGroupNumber.isEmpty || textfieldInsuredId.isEmpty{
//            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
//            self.presentViewController(alert, animated: true, completion: nil)
//
//        }else 
            if !textfieldInsuranceCompanyPhone.isEmpty && !textfieldInsuranceCompanyPhone.text!.isPhoneNumber{
            let alert = Extention.alert("PLEASE ENTER THE VALID INSURANCE COMPANY PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)

        }else if !textfieldZipcode.isEmpty && !textfieldZipcode.text!.isZipCode{
            let alert = Extention.alert("PLEASE ENTER THE VALID ZIPCODE")
            self.present(alert, animated: true, completion: nil)

        }else{
                saveValue()
            let new1VC = patientStoryBoard.instantiateViewController(withIdentifier: "PatientInformation2VC") as! PatientInformation2ViewController
            new1VC.patient = self.patient
            new1VC.isSelfSelected = radioSelf.selected.tag == 1
            self.navigationController?.pushViewController(new1VC, animated: true)
            }
            }else{

                let new1VC = patientStoryBoard.instantiateViewController(withIdentifier: "PatientInformation5VC") as! PatientInformation5ViewController
                new1VC.patient = self.patient
                self.navigationController?.pushViewController(new1VC, animated: true)
            }
    }
    
    @IBAction func radioInsuranceButtonPressed (withSender sender : RadioButton){
        if sender.tag == 1{
        self.view.layoutIfNeeded()
            heightInsurance.constant = 551
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }else{
            self.view.layoutIfNeeded()
            heightInsurance.constant = 0
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
            self.view.endEditing(true)
        }

        
    }
    
    @IBAction func radioSelfPressed (withSender sender : RadioButton){
        if sender.tag == 1{
            textfieldAddress.text = patient.adult.address
            textfieldCity.text = patient.adult.city
            textfieldState.text = patient.adult.state
            textfieldZipcode.text = patient.adult.zipcode
        }else{
            textfieldAddress.text = ""
            textfieldCity.text = ""
            textfieldState.text = "PA"
            textfieldZipcode.text = ""

        }
        
    }
    


}

extension PatientInformation1ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldInsuranceCompanyPhone{
            return textField.formatPhoneNumber(range, string: string)
        } else if textField == textfieldZipcode {
            return textField.formatZipCode(range, string: string)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

