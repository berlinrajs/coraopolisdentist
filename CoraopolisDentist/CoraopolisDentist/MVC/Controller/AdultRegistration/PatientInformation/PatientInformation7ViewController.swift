//
//  PatientInformation7ViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 9/1/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientInformation7ViewController: PDViewController {
    
    @IBOutlet weak var textfieldName : UITextField!
    @IBOutlet weak var textfieldRelationship : UITextField!
    @IBOutlet weak var radioPractice : RadioButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        loadValue()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        textfieldName.setSavedText(patient.patientInformation.legalGuardianName)
        textfieldRelationship.setSavedText(patient.patientInformation.legalGuardianRelationship)
        radioPractice.setSelectedWithTag(patient.patientInformation.legalGuardianPractice)

    }
    
    func saveValue()  {
        patient.patientInformation.legalGuardianName = textfieldName.getText()
        patient.patientInformation.legalGuardianRelationship = textfieldRelationship.getText()
        patient.patientInformation.legalGuardianPractice = radioPractice.selected == nil ? 0 : radioPractice.selected.tag

    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }


    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textfieldName.isEmpty || textfieldRelationship.isEmpty || radioPractice.selected == nil{
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        }else{
            saveValue()
            let new1VC = patientStoryBoard.instantiateViewController(withIdentifier: "PatientInformation9VC") as! PatientInformation9ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)


        }
    }

}

extension PatientInformation7ViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
