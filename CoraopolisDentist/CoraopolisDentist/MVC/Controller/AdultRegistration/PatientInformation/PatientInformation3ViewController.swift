//
//  PatientInformation3ViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 8/31/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientInformation3ViewController: PDViewController {

    @IBOutlet weak var radioSelf : RadioButton!
    @IBOutlet weak var textfieldInsuranceCompanyName : UITextField!
    @IBOutlet weak var textfieldInsuranceCompanyPhone : UITextField!
    @IBOutlet weak var textfieldAddress : UITextField!
    @IBOutlet weak var textfieldCity : UITextField!
    @IBOutlet weak var textfieldState : UITextField!
    @IBOutlet weak var textfieldZipcode : UITextField!
    @IBOutlet weak var textfieldGroupNumber : UITextField!
    @IBOutlet weak var textfieldInsuredId : UITextField!
    @IBOutlet weak var heightSelf : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        StateListView.addStateListForTextField(textfieldState)
        heightSelf.constant = patient.adult.address != "" ? 38 : 0
        loadValue()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
            textfieldInsuranceCompanyName.setSavedText(patient.patientInformation.secondaryCompanyName)
            textfieldInsuranceCompanyPhone.setSavedText(patient.patientInformation.secondaryCompanyNumber)
            textfieldGroupNumber.setSavedText(patient.patientInformation.secondaryGroupNumber)
            textfieldInsuredId.setSavedText(patient.patientInformation.secondaryInsuredId)
            textfieldAddress.setSavedText(patient.patientInformation.secondaryAddress)
            textfieldCity.setSavedText(patient.patientInformation.secondaryCity)
            textfieldState.setSavedText(patient.patientInformation.secondaryState)
            textfieldZipcode.setSavedText(patient.patientInformation.secondaryZipcode)
            radioSelf.setSelectedWithTag(patient.patientInformation.radioSecondarySelfTag)

    }
    
    func saveValue()  {
        patient.patientInformation.secondaryCompanyName = textfieldInsuranceCompanyName.isEmpty ? "N/A" : textfieldInsuranceCompanyName.text!
        patient.patientInformation.secondaryCompanyNumber =  textfieldInsuranceCompanyPhone.isEmpty ? "N/A" : textfieldInsuranceCompanyPhone.text!
        patient.patientInformation.secondaryGroupNumber  = textfieldGroupNumber
            .isEmpty ? "N/A" : textfieldGroupNumber.text!
        patient.patientInformation.secondaryInsuredId = textfieldInsuredId.isEmpty ? "N/A" : textfieldInsuredId.text!
        patient.patientInformation.secondaryAddress = textfieldAddress.isEmpty ? "N/A" : textfieldAddress.text!
        patient.patientInformation.secondaryCity = textfieldCity.isEmpty ? "N/A" : textfieldCity.text!
        patient.patientInformation.secondaryState = textfieldState.isEmpty ? "N/A" : textfieldState.text!
        patient.patientInformation.secondaryZipcode = textfieldZipcode.isEmpty ? "N/A" : textfieldZipcode.text!
        patient.patientInformation.radioSecondarySelfTag = radioSelf.selected.tag

    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    

    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
//        if textfieldInsuranceCompanyName.isEmpty || textfieldAddress.isEmpty || textfieldCity.isEmpty || textfieldState.isEmpty || textfieldZipcode.isEmpty || textfieldGroupNumber.isEmpty || textfieldInsuredId.isEmpty{
//            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
//            self.presentViewController(alert, animated: true, completion: nil)
//            
//        }else 
        if !textfieldInsuranceCompanyPhone.isEmpty && !textfieldInsuranceCompanyPhone.text!.isPhoneNumber{
            let alert = Extention.alert("PLEASE ENTER THE VALID INSURANCE COMPANY PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)
            
        }else if !textfieldZipcode.isEmpty && !textfieldZipcode.text!.isZipCode{
            let alert = Extention.alert("PLEASE ENTER THE VALID ZIPCODE")
            self.present(alert, animated: true, completion: nil)
            
        }else{
            saveValue()
            let new1VC = patientStoryBoard.instantiateViewController(withIdentifier: "PatientInformation4VC") as! PatientInformation4ViewController
            new1VC.patient = self.patient
            new1VC.isSelfSelected = radioSelf.selected.tag == 1
            self.navigationController?.pushViewController(new1VC, animated: true)
            
        }
        
    }
    
    
    @IBAction func radioSelfPressed (withSender sender : RadioButton){
        if sender.tag == 1{
            textfieldAddress.text = patient.adult.address
            textfieldCity.text = patient.adult.city
            textfieldState.text = patient.adult.state
            textfieldZipcode.text = patient.adult.zipcode
        }else{
            textfieldAddress.text = ""
            textfieldCity.text = ""
            textfieldState.text = "PA"
            textfieldZipcode.text = ""
            
        }
        
    }
}

extension PatientInformation3ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldInsuranceCompanyPhone{
            return textField.formatPhoneNumber(range, string: string)
        } else if textField == textfieldZipcode {
            return textField.formatZipCode(range, string: string)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

