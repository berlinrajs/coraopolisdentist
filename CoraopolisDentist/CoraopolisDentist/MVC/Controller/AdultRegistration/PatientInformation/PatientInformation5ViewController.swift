//
//  PatientInformation5ViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 8/31/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientInformation5ViewController: PDViewController {

    @IBOutlet weak var radioSelf : RadioButton!
    @IBOutlet weak var textfieldName : UITextField!
    @IBOutlet weak var textfieldRelationship : UITextField!
    @IBOutlet weak var textfieldAddress : UITextField!
    @IBOutlet weak var textfieldCity : UITextField!
    @IBOutlet weak var textfieldState : UITextField!
    @IBOutlet weak var textfieldZipcode : UITextField!
    @IBOutlet weak var textfieldPhoneNumber : UITextField!
    @IBOutlet weak var textfieldDateOfBirth : UITextField!
    @IBOutlet weak var heightSelf : NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()

        StateListView.addStateListForTextField(textfieldState)
        heightSelf.constant = patient.adult.address != "" ? 38 : 0
        DateInputView.addDatePickerForDOBTextField(textfieldDateOfBirth)
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        radioSelf.setSelectedWithTag(patient.patientInformation.responsibleSelfTag)
        textfieldName.setSavedText(patient.patientInformation.responsibleName)
        textfieldRelationship.setSavedText(patient.patientInformation.responsibleRelationship)
        textfieldPhoneNumber.setSavedText(patient.patientInformation.responsiblePhoneNUmber)
        textfieldDateOfBirth.setSavedText(patient.patientInformation.responsibleDateOfBirth)
        textfieldAddress.setSavedText(patient.patientInformation.responsibleAddress)
        textfieldCity.setSavedText(patient.patientInformation.responsibleCity)
        textfieldState.setSavedText(patient.patientInformation.responsibleState)
        textfieldZipcode.setSavedText(patient.patientInformation.responsibleZipcode)
        

    }
    
    func saveValue()  {
        patient.patientInformation.responsibleName = textfieldName.getText()
        patient.patientInformation.responsibleRelationship = textfieldRelationship.getText()
        patient.patientInformation.responsiblePhoneNUmber  = textfieldPhoneNumber.getText()
        patient.patientInformation.responsibleDateOfBirth = textfieldDateOfBirth.getText()
        patient.patientInformation.responsibleAddress = textfieldAddress.getText()
        patient.patientInformation.responsibleCity = textfieldCity.getText()
        patient.patientInformation.responsibleState = textfieldState.isEmpty ? "PA" : textfieldState.text!
        patient.patientInformation.responsibleZipcode = textfieldZipcode.getText()
        patient.patientInformation.responsibleSelfTag = radioSelf.selected.tag
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }

    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textfieldName.isEmpty || textfieldRelationship.isEmpty || textfieldDateOfBirth.isEmpty || textfieldAddress.isEmpty || textfieldCity.isEmpty || textfieldState.isEmpty || textfieldZipcode.isEmpty{
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
            
        }else if !textfieldPhoneNumber.isEmpty && !textfieldPhoneNumber.text!.isPhoneNumber{
            let alert = Extention.alert("PLEASE ENTER THE VALID PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)
            
        }else if !textfieldZipcode.text!.isZipCode{
            let alert = Extention.alert("PLEASE ENTER THE VALID ZIPCODE")
            self.present(alert, animated: true, completion: nil)
            
        }else{
            saveValue()
            let new1VC = patientStoryBoard.instantiateViewController(withIdentifier: "PatientInformation6VC") as! PatientInformation6ViewController
            new1VC.patient = self.patient
            
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
        
    }

    @IBAction func radioSelfPressed (withSender sender : RadioButton){
        if sender.tag == 1{
            textfieldName.text = patient.fullName
            textfieldRelationship.text = "PATIENT"
            textfieldDateOfBirth.text = patient.dateOfBirth
            textfieldPhoneNumber.text = patient.adult.cellNumber == "N/A" ? "" : patient.adult.cellNumber
            textfieldAddress.text = patient.adult.address
            textfieldCity.text = patient.adult.city
            textfieldState.text = patient.adult.state
            textfieldZipcode.text = patient.adult.zipcode
        }else{
            textfieldAddress.text = ""
            textfieldCity.text = ""
            textfieldState.text = "PA"
            textfieldZipcode.text = ""
            textfieldName.text = ""
            textfieldRelationship.text = ""
            textfieldDateOfBirth.text = ""
            textfieldPhoneNumber.text = ""
        }
        
    }
    
    
    
}

extension PatientInformation5ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldPhoneNumber{
            return textField.formatPhoneNumber(range, string: string)
        } else if textField == textfieldZipcode {
            return textField.formatZipCode(range, string: string)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

