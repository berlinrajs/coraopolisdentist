//
//  PatientInformation6ViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 9/1/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientInformation6ViewController: PDViewController {
    
    @IBOutlet weak var textfieldLicense : UITextField!
    @IBOutlet weak var textfieldEmployerName : UITextField!
    @IBOutlet weak var textfieldWorkPhone : UITextField!
    @IBOutlet weak var textfieldCardNUmber : UITextField!
    @IBOutlet weak var textfieldExpiry : UITextField!
    @IBOutlet weak var textfieldSSN : UITextField!
    @IBOutlet weak var dropDownPayment : BRDropDown!
    @IBOutlet weak var dropDownCard : BRDropDown!
    @IBOutlet weak var viewCard : UIView!
    var datePicker : DatePickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dropDownPayment.items = ["Cash","Credit Card"]
        dropDownPayment.delegate = self
        dropDownCard.items = ["Visa","MC","AMEX"]
        DateInputView.addExpiryDatePickerForTextField(textfieldExpiry)
        
        datePicker = DatePickerView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        let gregorian = Calendar(identifier: Calendar.Identifier.gregorian)
        
        let year = (gregorian as NSCalendar).component(.year, from: Date())
        datePicker.minYear = year
        datePicker.maxYear = year + 5
        datePicker.selectToday()
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 44))
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePressed))
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil), barbuttonDone]
        
        textfieldExpiry.inputView = datePicker
        textfieldExpiry.inputAccessoryView = toolbar
        
        loadValue()
        // Do any additional setup after loading the view.
    }
    
    func donePressed() {
        textfieldExpiry.resignFirstResponder()
        textfieldExpiry.text = datePicker.date
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        if patient.patientInformation.responsibleSelfTag == 1 && patient.patientInformation.responsibleDriverLicense == "N/A"{
            textfieldLicense.setSavedText(patient.adult.driverLicense)
            textfieldEmployerName.setSavedText(patient.adult.employerName)
            textfieldWorkPhone.setSavedText(patient.adult.workNumber)
            textfieldSSN.setSavedText(patient.adult.socialSecurityNumber)
            
        }else{
            textfieldLicense.setSavedText(patient.patientInformation.responsibleDriverLicense)
            textfieldEmployerName.setSavedText(patient.patientInformation.responsibleEmployerName)
            textfieldWorkPhone.setSavedText(patient.patientInformation.responsibleWOrkPhone)
            textfieldSSN.setSavedText(patient.patientInformation.responsibleSocialSecurityNumber)
            
            
        }
        textfieldCardNUmber.setSavedText(patient.patientInformation.responsibleCardNumber)
        textfieldExpiry.setSavedText(patient.patientInformation.responsibleExpDate)
        dropDownPayment.selectedIndex = patient.patientInformation.responsiblePaymentType
        dropDownCard.selectedIndex = patient.patientInformation.responsibleCardType
        if dropDownPayment.selectedIndex == 2{
            viewCard.isUserInteractionEnabled = true
            viewCard.alpha = 1.0
        }else{
            viewCard.isUserInteractionEnabled = false
            viewCard.alpha = 0.5
            dropDownCard.selectedIndex = 0
            textfieldExpiry.text = ""
            textfieldCardNUmber.text = ""
        }
        
        
    }
    
    func saveValue()  {
        patient.patientInformation.responsibleDriverLicense = textfieldLicense.isEmpty ? "N/A" : textfieldLicense.text!
        patient.patientInformation.responsibleEmployerName = textfieldEmployerName.isEmpty ? "N/A" : textfieldEmployerName.text!
        patient.patientInformation.responsibleWOrkPhone = textfieldWorkPhone.isEmpty ? "N/A" : textfieldWorkPhone.text!
        patient.patientInformation.responsiblePaymentType = dropDownPayment.selectedIndex
        patient.patientInformation.responsibleCardType =  dropDownPayment.selectedIndex == 2 ?dropDownCard.selectedIndex : 0
        patient.patientInformation.responsibleCardNumber = dropDownPayment.selectedIndex == 2 ? textfieldCardNUmber.text! : "N/A"
        patient.patientInformation.responsibleExpDate = dropDownPayment.selectedIndex == 2 ? textfieldExpiry.text! : "N/A"
        patient.patientInformation.responsibleSocialSecurityNumber = textfieldSSN.isEmpty ? "N/A" : textfieldSSN.text!
        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValue()
        super.buttonActionBack(withSender: sender)
    }
    
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        
        if !textfieldSSN.isEmpty && textfieldSSN.text!.characters.count != 9{
            let alert = Extention.alert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
            self.present(alert, animated: true, completion: nil)
        }else if !textfieldWorkPhone.isEmpty && !textfieldWorkPhone.text!.isPhoneNumber{
            let alert = Extention.alert("PLEASE ENTER THE VALID WORK PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)
        }else if !textfieldCardNUmber.isEmpty{
            if dropDownCard.selectedOption == "AMEX" && textfieldCardNUmber.text!.characters.count != 15{
                let alert = Extention.alert("PLEASE ENTER THE VALID CARD NUMBER")
                self.present(alert, animated: true, completion: nil)
            }else if textfieldCardNUmber.text!.characters.count != 16{
                let alert = Extention.alert("PLEASE ENTER THE VALID CARD NUMBER")
                self.present(alert, animated: true, completion: nil)
            }else{
                goToNextPage()
            }
            //        }else if dropDownPayment.selectedOption == nil{
            //            let alert = Extention.alert("PLEASE SELECT THE PAYMENT TYPE")
            //            self.presentViewController(alert, animated: true, completion: nil)
            //        }else if dropDownPayment.selectedOption == "Credit Card" {
            //            if dropDownCard.selectedOption == nil{
            //                let alert = Extention.alert("PLEASE SELECT THE CARD TYPE")
            //                self.presentViewController(alert, animated: true, completion: nil)
            //
            //            }else if dropDownCard.selectedOption == "AMEX" && textfieldCardNUmber.text!.characters.count != 15{
            //                let alert = Extention.alert("PLEASE ENTER THE VALID CARD NUMBER")
            //                self.presentViewController(alert, animated: true, completion: nil)
            //
            //            }else if dropDownCard.selectedOption == "AMEX" && textfieldExpiry.isEmpty{
            //                let alert = Extention.alert("PLEASE ENTER THE VALID EXP DATE")
            //                self.presentViewController(alert, animated: true, completion: nil)
            //
            //            }else if dropDownCard.selectedOption != "AMEX"{
            //                if textfieldCardNUmber.text!.characters.count != 16{
            //                    let alert = Extention.alert("PLEASE ENTER THE VALID CARD NUMBER")
            //                    self.presentViewController(alert, animated: true, completion: nil)
            //                }else if textfieldExpiry.isEmpty{
            //                    let alert = Extention.alert("PLEASE ENTER THE VALID EXP DATE")
            //                    self.presentViewController(alert, animated: true, completion: nil)
            //                }else{
            //                    goToNextPage()
            //                }
            //            }else{
            //                goToNextPage()
            //            }
            
        }else{
            goToNextPage()
        }
        
        
    }
    
    func goToNextPage()  {
        saveValue()
        if patient.is18YearsOld == true{
            let new1VC = patientStoryBoard.instantiateViewController(withIdentifier: "PatientInformation9VC") as! PatientInformation9ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
            
        }else{
            let new1VC = patientStoryBoard.instantiateViewController(withIdentifier: "PatientInformation7VC") as! PatientInformation7ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
    }
    
}

extension PatientInformation6ViewController : BRDropDownDelegate{
    
    func dropDown(_ dropDown: BRDropDown, selectedAtIndex index: Int, selectedOption option: String?) {
        if option! == "Credit Card"{
            viewCard.isUserInteractionEnabled = true
            viewCard.alpha = 1.0
        }else{
            viewCard.isUserInteractionEnabled = false
            viewCard.alpha = 0.5
            dropDownCard.selectedIndex = 0
            textfieldExpiry.text = ""
            textfieldCardNUmber.text = ""
        }
        
        
    }
    //    func didFinishSelectingValues(selectedOption: String) {
    //        if selectedOption == "Credit Card"{
    //            viewCard.userInteractionEnabled = true
    //            viewCard.alpha = 1.0
    //        }else{
    //            viewCard.userInteractionEnabled = false
    //            viewCard.alpha = 0.5
    //            textfieldExpiry.text = ""
    //            textfieldCardNUmber.text = ""
    //        }
    //
    //    }
}

extension PatientInformation6ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldWorkPhone{
            return textField.formatPhoneNumber(range, string: string)
        } else if textField == textfieldCardNUmber {
            return textField.formatNumbers(range, string: string, count: 16)
            //        } else if textField == textfieldExpiry {
            //            return textField.formatNumbers(range, string: string, count: 4)
        }else if textField == textfieldSSN{
            return textField.formatNumbers(range, string: string, count: 9)
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
