//
//  PDViewController.swift
//   Coraopolisdentist
//
//  Created by Leojin Bose on 2/22/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class PDViewController: UIViewController {
    
    var patient : PDPatient!
    
    @IBOutlet weak var pdfView: UIScrollView?
    
    
    @IBOutlet weak var buttonBack: UIButton?
    @IBOutlet weak var buttonSubmit: UIButton?
    var isFromPatientInfo: Bool = false
    
    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    let consentStoryboard = UIStoryboard(name: "Consent", bundle: Bundle.main)
    let adultStoryboard = UIStoryboard(name: "AdultRegistration", bundle: Bundle.main)
    let patientStoryBoard = UIStoryboard(name: "PatientInformation", bundle: Bundle.main)
    let childStoryBoard = UIStoryboard(name: "ChildRegistration", bundle: Bundle.main)
    let consentStoryboardNew = UIStoryboard(name: "ConsentNew", bundle: Bundle.main)


    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = UIRectEdge()
        self.buttonBack?.isHidden = self.buttonSubmit == nil && isFromPreviousForm
        buttonSubmit?.backgroundColor = UIColor.green
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var isFromPreviousForm: Bool {
        get {
            #if AUTO
                if navigationController?.viewControllers.count > 3 && navigationController?.viewControllers[2].isKind(of: VerificationViewController.self) == true {
                    navigationController?.viewControllers.remove(at: 2)
                    return false
                }
            #endif
            return navigationController?.viewControllers.count > 3 ? true : false
        }
    }
    
    @IBAction func buttonActionBack(withSender sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func showAlert(_ message: String) {
        let alert = Extention.alert(message)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert1(_ message: String, completion: @escaping (_ completed: Bool) -> Void) {
        let alertController = UIAlertController(title: "Coraopolis Dentist", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.destructive) { (action) -> Void in
            completion(true)
        }
        alertController.addAction(alertOkAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func buttonActionSubmit(withSender sender : UIButton) {
        if !Reachability.isConnectedToNetwork() {
            let alertController = UIAlertController(title: "Coraopolis Dentist", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
            let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.destructive) { (action) -> Void in
                let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.shared.openURL(url)
                }
            }
            let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) -> Void in
                
            }
            alertController.addAction(alertOkAction)
            alertController.addAction(alertCancelAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        let pdfManager = PDFManager()
        pdfManager.authorizeDrive(self) { (success) -> Void in
            if success {
                self.buttonSubmit?.isHidden = true
                self.buttonBack?.isHidden = true
                if let _ = self.pdfView {
                    if self.pdfView!.isKind(of: UIScrollView.self) {
                        pdfManager.createPDFForScrollView(self.pdfView!, fileName: self.patient.selectedForms.first!.formTitle!.fileName, patient: self.patient, completionBlock: { (finished) -> Void in
                            if finished {
                                self.patient.selectedForms.removeFirst()
                                self.gotoNextForm(false)
                            } else {
                                self.buttonSubmit?.isHidden = false
                                self.buttonBack?.isHidden = false
                            }
                        })
                    }
                } else {
                    pdfManager.createPDFForView(self.view, fileName: self.patient.selectedForms.first!.formTitle!.fileName, patient: self.patient, completionBlock: { (finished) -> Void in
                        if finished {
                            self.patient.selectedForms.removeFirst()
                            self.gotoNextForm(false)
                        } else {
                            self.buttonSubmit?.isHidden = false
                            self.buttonBack?.isHidden = false
                        }
                    })
                }
            } else {
                self.buttonSubmit?.isHidden = false
                self.buttonBack?.isHidden = false
            }
        }
    }
    
    func gotoNextForm(_ isFromPatientInfoValue: Bool) {
        if isFromPreviousForm {
            if self.navigationController?.viewControllers.count > 2 {
                self.navigationController?.viewControllers.removeSubrange(2...(self.navigationController?.viewControllers.count)! - 2)
            }
        }
        
        let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
        if formNames.contains(kAdultSignIn) || formNames.contains(kNewAdultSignIn){
            let adult = adultStoryboard.instantiateViewController(withIdentifier: "Adult1VC") as! AdultRegistration1ViewController
            adult.patient = self.patient
            self.navigationController?.pushViewController(adult, animated: true)
        }else if formNames.contains(kChildSignIn) || formNames.contains(kNewChildSignIn) {
            let child = childStoryBoard.instantiateViewController(withIdentifier: "Child1VC") as! ChildRegistration1ViewController
            child.patient = self.patient
            self.navigationController?.pushViewController(child, animated: true)
        }else if formNames.contains(kInsuranceCard) {
            let cardCapture = mainStoryboard.instantiateViewController(withIdentifier: "kCardImageCaptureVC") as! CardImageCaptureVC
            cardCapture.patient = self.patient
            cardCapture.isFromPatientInfo = isFromPatientInfoValue
            self.navigationController?.pushViewController(cardCapture, animated: true)
        } else if formNames.contains(kDrivingLicense) {
            let cardCapture = mainStoryboard.instantiateViewController(withIdentifier: "kCardImageCaptureVC") as! CardImageCaptureVC
            cardCapture.patient = self.patient
            cardCapture.isDrivingLicense = true
            cardCapture.isFromPatientInfo = isFromPatientInfoValue
            self.navigationController?.pushViewController(cardCapture, animated: true)
        }else if formNames.contains(kComposite){
            let composite = consentStoryboard.instantiateViewController(withIdentifier: "Composite1VC") as! Composite1ViewController
            composite.patient = self.patient
            self.navigationController?.pushViewController(composite, animated: true)
        }else if formNames.contains(kOfficePolicy){
            let office = consentStoryboard.instantiateViewController(withIdentifier: "Office1VC") as! AppointmentPolicy1ViewController
            office.patient = self.patient
            self.navigationController?.pushViewController(office, animated: true)
        }else if formNames.contains(kReservation){
            let reservation = consentStoryboard.instantiateViewController(withIdentifier: "Reservation1VC") as! ReservationFee1ViewController
            reservation.patient = self.patient
            self.navigationController?.pushViewController(reservation, animated: true)
        }else if formNames.contains(kFinancial){
            let finance = consentStoryboard.instantiateViewController(withIdentifier: "Financial1VC") as! Financial1ViewController
            finance.patient = self.patient
            self.navigationController?.pushViewController(finance, animated: true)
        }else if formNames.contains(kNoticeOfPrivacyPractices) {
            let noticeOfPrivacyPracticesVC = consentStoryboardNew.instantiateViewController(withIdentifier: "kNoticeOfPrivacyPracticesVC") as! NoticeOfPrivacyPracticesVC
            noticeOfPrivacyPracticesVC.patient = self.patient
            self.navigationController?.pushViewController(noticeOfPrivacyPracticesVC, animated: true)
        }else if formNames.contains(kPatientUpdate) {
            let patientUpdateVCVC = consentStoryboardNew.instantiateViewController(withIdentifier: "kPatientUpdateVC") as! PatientUpdateVC
            patientUpdateVCVC.patient = self.patient
            self.navigationController?.pushViewController(patientUpdateVCVC, animated: true)
        }else if formNames.contains(kPaymentPlan) {
            let paymentPlanVC = consentStoryboardNew.instantiateViewController(withIdentifier: "kPaymentPlanVC") as! PaymentPlanVC
            paymentPlanVC.patient = self.patient
            self.navigationController?.pushViewController(paymentPlanVC, animated: true)
        } else if formNames.contains(kDiscountPlan) {
            let discountPlanVC = consentStoryboardNew.instantiateViewController(withIdentifier: "kDiscountPlanVC") as! DiscountPlanVC
            discountPlanVC.patient = self.patient
            self.navigationController?.pushViewController(discountPlanVC, animated: true)
        }else if formNames.contains(kHippa) {
            let hippaaFormVC = consentStoryboardNew.instantiateViewController(withIdentifier: "kHIPAAFirstVC") as! HIPAAFirstVC
            hippaaFormVC.patient = self.patient
            self.navigationController?.pushViewController(hippaaFormVC, animated: true)
        }else if formNames.contains(kPeriodontalCharting){
            let dental = consentStoryboardNew.instantiateViewController(withIdentifier: "PeriodontalChartingVC") as! PeriodontalChartingViewController
            dental.patient = self.patient
            self.navigationController?.pushViewController(dental, animated: true)
        }else if formNames.contains(kRootPlanningRefusal){
            let dental = consentStoryboardNew.instantiateViewController(withIdentifier: "RootPlanningRefusalVC") as! RootPlanningRefusalViewController
            dental.patient = self.patient
            self.navigationController?.pushViewController(dental, animated: true)
        }else if formNames.contains(kTeethScaling){
            let dental = consentStoryboardNew.instantiateViewController(withIdentifier: "TeethScalingVC") as! TeethScalingViewController
            dental.patient = self.patient
            self.navigationController?.pushViewController(dental, animated: true)
        }else if formNames.contains(kRootPlanningAuthorization){
            let dental = consentStoryboardNew.instantiateViewController(withIdentifier: "RootPlanningAuthorizationVC") as! RootPlanningAuthorizationViewController
            dental.patient = self.patient
            self.navigationController?.pushViewController(dental, animated: true)
        }else if formNames.contains(kPeriodontalSpecialist){
            let dental = consentStoryboardNew.instantiateViewController(withIdentifier: "PeriodontalSpecialistVC") as! PeriodontalSpecialistViewController
            dental.patient = self.patient
            self.navigationController?.pushViewController(dental, animated: true)
        }else if formNames.contains(kPeriodontalDisease) {
            let notificationOfPeriodontalVC = consentStoryboardNew.instantiateViewController(withIdentifier: "kNotificationOfPeriodontalVC") as! NotificationOfPeriodontalVC
            notificationOfPeriodontalVC.patient = self.patient
            self.navigationController?.pushViewController(notificationOfPeriodontalVC, animated: true)
        }
        else if formNames.contains(kPeriodontalManagement) {
            let periodontalManagementVC = consentStoryboardNew.instantiateViewController(withIdentifier: "kPeriodontalManagementVC") as! PeriodontalManagementVC
            periodontalManagementVC.patient = self.patient
            self.navigationController?.pushViewController(periodontalManagementVC, animated: true)
        }
        else if formNames.contains(kSocialMedia){
            let social = consentStoryboard.instantiateViewController(withIdentifier: "SocialMedia1VC") as! SocialMedia1ViewController
            social.patient = self.patient
            self.navigationController?.pushViewController(social, animated: true)
        }else if formNames.contains(kXrays){
            let xrays = consentStoryboard.instantiateViewController(withIdentifier: "Xray1VC") as! Xray1ViewController
            xrays.patient = self.patient
            self.navigationController?.pushViewController(xrays, animated: true)
        }else if formNames.contains(kWaxConsent){
            let wax = consentStoryboard.instantiateViewController(withIdentifier: "Wax1VC") as! Wax1ViewController
            wax.patient = self.patient
            self.navigationController?.pushViewController(wax, animated: true)
        }else if formNames.contains(kOralSurgery){
            let oral = consentStoryboard.instantiateViewController(withIdentifier: "Oral1VC") as! OralSurgery1ViewController
            oral.patient = self.patient
            self.navigationController?.pushViewController(oral, animated: true)
        }else if formNames.contains(kPhotography){
            let photo = consentStoryboard.instantiateViewController(withIdentifier: "Photography1VC") as! Photography1ViewController
            photo.patient = self.patient
            self.navigationController?.pushViewController(photo, animated: true)
        }else if formNames.contains(kDentalRecords){
            let dental = consentStoryboard.instantiateViewController(withIdentifier: "DentalRecords1VC") as! DentalRecords1ViewController
            dental.patient = self.patient
            self.navigationController?.pushViewController(dental, animated: true)
        }else if formNames.contains(kPreviousRecords){
            let dental = consentStoryboard.instantiateViewController(withIdentifier: "PreviousRecords1VC") as! PreviousRecords1ViewController
            dental.patient = self.patient
            self.navigationController?.pushViewController(dental, animated: true)
        }else if formNames.contains(kInsuranceCoverage){
            let dental = consentStoryboardNew.instantiateViewController(withIdentifier: "InsuranceCoverageVC") as! InsuranceCoverageViewController
            dental.patient = self.patient
            self.navigationController?.pushViewController(dental, animated: true)
        }else if formNames.contains(kDeliveredAppliance){
            let dental = consentStoryboardNew.instantiateViewController(withIdentifier: "DeliveredApplianceVC") as! DeliveredApplianceViewController
            dental.patient = self.patient
            self.navigationController?.pushViewController(dental, animated: true)
        }
        else if formNames.contains(kFeedbackForm) {
            let feedBackVC = mainStoryboard.instantiateViewController(withIdentifier: "kFeedBackInfoViewController") as! FeedBackInfoViewController
            feedBackVC.patient = self.patient
            self.navigationController?.pushViewController(feedBackVC, animated: true)
        }
        
        else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: kFormsCompletedNotification), object: nil)
            self.navigationController?.popToRootViewController(animated: true)
        }
        
    }
}

