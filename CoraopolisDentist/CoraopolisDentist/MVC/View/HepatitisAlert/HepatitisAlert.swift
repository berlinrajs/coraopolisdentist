//
//  HepatitisAlert.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/24/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class HepatitisAlert: UIView {
    
    var arrayButtonTags : [Int] = [Int]()
    
    var completion:(([Int])->Void)?
    var errorAlert : ((String) -> Void)?
    
    static var sharedInstance : HepatitisAlert {
        let instance :  HepatitisAlert = Bundle.main.loadNibNamed("HepatitisAlert", owner: nil, options: nil)!.first as! HepatitisAlert
        return instance
        
    }
    
    func showPopUp(_ inview : UIView, completion : @escaping (_ arrayTags : [Int]) -> Void , showAlert : @escaping (_ alertMessage : String)-> Void)  {
        self.completion = completion
        self.errorAlert = showAlert
        arrayButtonTags.removeAll()
        self.frame = screenSize
        
        inview.addSubview(self)
        inview.bringSubview(toFront: self)
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func hepatitisButtonAction (withSender sender : UIButton){
        sender.isSelected = !sender.isSelected
        if arrayButtonTags.contains(sender.tag){
            arrayButtonTags.remove(at: arrayButtonTags.index(of: sender.tag)!)
        }else{
            arrayButtonTags.append(sender.tag)
        }
        
    }
    
    
    @IBAction func okButtonPressed (withSender sender : UIButton){
            self.completion!(arrayButtonTags)
            self.removeFromSuperview()
        }
    }

