//
//  BottleAlert.swift
//  CoraopolisDentist
//
//  Created by Bala Murugan on 10/31/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class BottleAlert: UIView {

    @IBOutlet weak var textfieldContents : UITextField!
    @IBOutlet weak var textfieldAge : UITextField!
    
    var completion:((String,String)->Void)?
    var errorAlert : ((String) -> Void)?
    
    static var sharedInstance : BottleAlert {
        let instance :  BottleAlert = Bundle.main.loadNibNamed("BottleAlert", owner: nil, options: nil)!.first as! BottleAlert
        return instance
        
    }
    
    func showPopUp(_ inview : UIView, completion :  @escaping (_ contents : String , _ age : String) -> Void , showAlert : @escaping (_ alertMessage : String)-> Void)  {
        self.completion = completion
        self.errorAlert = showAlert
        self.frame = screenSize
        
        inview.addSubview(self)
        inview.bringSubview(toFront: self)
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    
    @IBAction func okButtonPressed (withSender sender : UIButton){
        self.completion!(textfieldContents.isEmpty ? "N/A" : textfieldContents.text!, textfieldAge.isEmpty ? "N/A" : textfieldAge.text!)
        //self.completion!(arrayButtonTags)
        self.removeFromSuperview()
    }

}

extension BottleAlert : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldAge {
            return textField.formatNumbers(range, string: string, count: 2)
        }
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
