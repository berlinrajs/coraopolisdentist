//
//  HippaSignatureRefusal.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/12/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HipaaSignatureRefusalPopup: UIView {

    @IBOutlet weak var radioRefusal: RadioButton!
    @IBOutlet weak var textFieldRefusalOther: PDTextField!
    @IBOutlet weak var textFieldEfforts: PDTextField!
    
    class func popUpView() -> HipaaSignatureRefusalPopup {
        return Bundle.main.loadNibNamed("HipaaSignatureRefusalPopup", owner: nil, options: nil)!.first as! HipaaSignatureRefusalPopup
    }
    
    var completion:((HipaaSignatureRefusalPopup, Bool, UITextField, Int)->Void)?
    var errorBlock:((String) -> Void)?
    
    func showInViewController(viewController: UIViewController?, completion : @escaping (_ popUpView: HipaaSignatureRefusalPopup, _ refused: Bool, _ textFieldOther : UITextField, _ refusalTag: Int) -> Void, error: @escaping (_ message: String) -> Void) {
        
        textFieldRefusalOther.textColor = UIColor.lightGray
        textFieldRefusalOther.isEnabled = false
        
        self.completion = completion
        self.errorBlock = error
        self.frame = CGRect(x:0, y:0, width:screenSize.width, height:screenSize.height)
        
        if viewController != nil {
            viewController!.view.addSubview(self)
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        }
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionOK() {
        if radioRefusal.selected != nil && radioRefusal.selected.tag == 4 && textFieldRefusalOther.isEmpty {
            self.errorBlock?("PLEASE ENTER THE REASON")
        } else {
            self.completion?(self, radioRefusal.selected != nil, self.textFieldRefusalOther, radioRefusal.selected == nil ? 0 : radioRefusal.selected.tag)
        }
    }
    
    @IBAction func buttonRefusalAction() {
        if radioRefusal.selected.tag == 4 {
            textFieldRefusalOther.isEnabled = true
            textFieldRefusalOther.textColor = UIColor.black
        } else {
            textFieldRefusalOther.text = ""
            textFieldRefusalOther.isEnabled = false
            textFieldRefusalOther.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func buttonSkipAction () {
        completion?(self, false, self.textFieldRefusalOther, 0)
    }
    
    func close() {
        self.removeFromSuperview()
    }
}
