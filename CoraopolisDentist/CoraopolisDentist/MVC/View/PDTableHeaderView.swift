//
//  PDTableHeaderView.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/5/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDTableHeaderView: UIView {

    @IBOutlet weak var labelFormName: PDLabel!
    @IBOutlet weak var imageViewCheckMark: UIImageView!
    @IBOutlet weak var imageViewBackground: UIImageView!

}
